﻿/*; (function ($) {
    $.fn.fixMe = function () {
        return this.each(function () {
            var $this = $(this),
               $t_fixed;
            function init() {
                $this.wrap('<div class="container" />');
                $t_fixed = $this.clone();
                $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
                resizeFixed();
            }
            function resizeFixed() {
                $t_fixed.find("th").each(function (index) {
                    $(this).css("width", $this.find("th").eq(index).outerWidth() + "px");
                });
            }
            function scrollFixed() {
                var offset = $(this).scrollTop(),
                tableOffsetTop = $this.offset().top,
                tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
                if (offset < tableOffsetTop || offset > tableOffsetBottom)
                    $t_fixed.hide();
                else if (offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
                    $t_fixed.show();
            }
            $(window).resize(resizeFixed);
            $(window).scroll(scrollFixed);
            init();
        });
    };
})(jQuery);

$(document).ready(function () {
    $("table").fixMe();
    $(".up").click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 2000);
    });
});*/
;(function($) {
    $.fn.fixMe = function() {
        return this.each(function(index) {
            var $this = $(this),
                $body = $this.find("tr"),
                $t_fixed,
                css = {
                    position: "fixed",
                    display: "none",
                    "overflow-x": "scroll",
                    left: "auto",
                    top: '105px',
                    'z-index': 10,
                },
                class_top_bar = 'scroll-top-'+index,
                last_scroll = 0,
                alturaSobrante = 0;


            var isSafari = function(){
                if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
                   return true;
                }

                return false;
            };
            function init() {
                $this.wrap('<div ><div class="container-table-scroll"  style="overflow-x: scroll;"></div></div>');
                $t_fixed = $this.clone();
                alturaSobrante = $this.find(".table-scroll-tr-remove td").first().outerHeight();
                $t_fixed.find(".table-scroll-tr-remove").remove();
                $t_fixed.find("tbody").remove().end().insertBefore($this);
                $t_fixed.wrap('<div></div>');
                $t_fixed.parent().css(css);
                $t_fixed.parent().parent().parent().prepend('<div class="'+class_top_bar+'" style="height: 20px;overflow-x: scroll;overflow-y: hidden;"><div style="height: 20px;"></div></div>');

                $t_fixed.parent().scroll(function() {
                    last_scroll = $(this).scrollLeft();
                    $t_fixed.parent().parent().parent().find('.container-table-scroll').scrollLeft(last_scroll);
                    $t_fixed.parent().parent().parent().find('.'+class_top_bar).scrollLeft(last_scroll);

                    if($this.data('fixed-column-left') == 1){
                        $body.each(function(){
                            var $td = $(this).find('td:first-child, th:first-child');
                            if(!$td.data("no-fixed-column-left")){
                                $td.css({
                                        'position': 'relative',
                                        'left': (last_scroll-1)+'px',
                                        'z-index': 9
                                });
                            }
                        });
                        $t_fixed.find("thead tr").each(function(i,v){

                                var $td = $(this).find('td:first-child, th:first-child');

                                $td.css({
                                        'position': 'relative',
                                        'left': (last_scroll-1)+'px',
                                        'z-index': 9
                                });
                        });
                    }
                });
                $t_fixed.parent().parent().parent().find('.container-table-scroll').scroll(function() {
                    last_scroll = $(this).scrollLeft();
                    $t_fixed.parent().parent().parent().find('.'+class_top_bar).scrollLeft(last_scroll);
                    $t_fixed.parent().scrollLeft(last_scroll);
                    if($this.data('fixed-column-left') == 1){
                        $body.each(function(){
                            var $td = $(this).find('td:first-child, th:first-child');
                            if(!$td.data("no-fixed-column-left")){
                                $td.css({
                                        'position': 'relative',
                                        'left': (last_scroll-1)+'px',
                                        'z-index': 9
                                });
                            }
                        });
                    }
                    $t_fixed.find("thead tr").each(function(i,v){
                            var $td = $(this).find('td:first-child, th:first-child');
                            $td.css({
                                    'position': 'relative',
                                    'left': (last_scroll-1)+'px',
                                    'z-index': 9
                            });
                    });
                });
                $t_fixed.parent().parent().parent().find('.'+class_top_bar).scroll(function(){
                    last_scroll = $(this).scrollLeft();
                    $t_fixed.parent().parent().parent().find('.container-table-scroll').scrollLeft(last_scroll);
                    if($this.data('fixed-column-left') == 1){
                        $body.each(function(){
                            var $td = $(this).find('td:first-child, th:first-child');
                            if(!$td.data("no-fixed-column-left")){
                                $td.css({
                                        'position': 'relative',
                                        'left': (last_scroll-1)+'px',
                                        'z-index': 9
                                });
                            }
                        });
                    }
                    $t_fixed.find("thead tr").each(function(i,v){
                            var $td = $(this).find('td:first-child, th:first-child');
                            $td.css({
                                    'position': 'relative',
                                    'left': (last_scroll-1)+'px',
                                    'z-index': 9
                            });

                    });
                });

                if($this.data('fixed-column-left') == 1){
                        initLefColumn();
                }
                resizeFixed();
                scrollFixed();
            }
            function initLefColumn(){
                $this.find("tr").each(function(i,v){
                        $(v).find('td, th').first().css({
                                'position': 'relative',
                                'left': 0,
                                'z-index': 9
                        });
                });
                $t_fixed.find("thead tr").each(function(i,v){
                        $(v).find('td, th').first().css({
                                'position': 'relative',
                                'left': 0,
                                'z-index': 9
                        });
                });
            }
            function resizeFixed() {
                $t_fixed.find("th").each(function(index) {

                    $(this).css({
                            "min-width"  :  $this.find("th").eq(index).outerWidth() + "px",
                            "max-width" :  $this.find("th").eq(index).outerWidth() + "px"
                    });
                    $(this).css({
                            "min-width": $this.find("td").eq(index).outerWidth() + "px",
                            "max-width": $this.find("td").eq(index).outerWidth() + "px"
                    });
                });
                $t_fixed.find("td").each(function(index) {

                    $(this).css({
                            "min-width"  :  $this.find("th").eq(index).outerWidth() + "px",
                            "max-width" :  $this.find("th").eq(index).outerWidth() + "px"
                    });
                    $(this).css({
                            "min-width": $this.find("td").eq(index).outerWidth() + "px",
                            "max-width": $this.find("td").eq(index).outerWidth() + "px"
                    });
                });
                var width = $t_fixed.parent().parent().outerWidth(),
                    left = $t_fixed.parent().parent().offset().left;
                $t_fixed.parent().css({
                    width: width + 'px',
                    // left: left + 'px'
                });
                $t_fixed.parent().parent().parent().find('.'+class_top_bar).css({
                    width: width + 'px',
                });
                $t_fixed.parent().parent().parent().find('.'+class_top_bar).children().first().css({
                    width: $t_fixed.parent().next().width() + 'px',
                });

                if ( !isSafari() && getDiferencia($t_fixed.parent().outerWidth(), $t_fixed.parent().parent().parent().find('.'+class_top_bar).children().outerWidth()) <= 10 ){

                    $t_fixed.parent().css('overflow-x', 'hidden' );
                    $t_fixed.parent().parent().parent().find('.'+class_top_bar).css('overflow-x', 'hidden' );
                }else{
                    $t_fixed.parent().css('overflow-x', 'scroll' );
                    $t_fixed.parent().parent().parent().find('.'+class_top_bar).css('overflow-x', 'scroll' );
                }

                if ( !isSafari() && getDiferencia($t_fixed.parent().outerWidth() , $t_fixed.parent().next().outerWidth())  <= 10 ){
                    $t_fixed.parent().parent().css('overflow-x', 'hidden' );
                }else{
                    $t_fixed.parent().parent().css('overflow-x', 'scroll' );
                }

            }
            function scrollFixed() {
                var offset = $(this).scrollTop(),
                    tableOffsetTop = $this.offset().top + alturaSobrante,
                    tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();

                if (offset < tableOffsetTop || offset > tableOffsetBottom){
                    $t_fixed.parent().hide();

                }else if (offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden")){
                    $t_fixed.parent().show();
                    resizeFixed();
                }

                $t_fixed.parent().parent().parent().find('.'+class_top_bar).scrollLeft(last_scroll);
                $t_fixed.parent().scrollLeft(last_scroll);

            }
            $(window).resize(resizeFixed);
            $(window).scroll(scrollFixed);

            init();


        });
    };
})(jQuery);

function getDiferencia(a, b){
    return Math.sqrt(Math.pow( a-b, 2 ));
}

var fixMeIndex = 0;
function tryFixMe(){
    fixMeIndex++;
    if(fixMeIndex == 1){
        return false;
    }
    $.each($('.table-scrollable'), function(index, value){
            if(!$(value).parent().hasClass('container-table-scroll')){
                $(value).fixMe();
            }
    });
    $("#ajaxProgress").hide();
}

$( window ).load(function(){
    $('.table-scrollable').fixMe();
});

function scrollAlternativo(){

	$(".table-scrollable").parent().scroll(function(){
			last_scroll = $(this).scrollLeft();
			$(".table-scrollable tr td:first-child").each(function(){
				$(this).css({
						'position': 'relative',
						'left': (last_scroll-1)+'px',
                        'z-index': 9
				});
			});
	});
	$(".scroll-top-0").scroll(function(){
			last_scroll = $(this).scrollLeft();
			$(".table-scrollable tr td:first-child").each(function(){
				$(this).css({
						'position': 'relative',
						'left': (last_scroll-1)+'px',
                        'z-index': 9
				});
			});
	});

	$(".table-scrollable tr td:first-child").each(function(){
		$(this).css({
				'position': 'relative',
				'left': $(".scroll-top-0").scrollLeft()+'px',
                'z-index': 9
		});
	});

}