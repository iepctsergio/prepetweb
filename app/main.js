/***
Metronic AngularJS App Main Script
***/

/* Metronic App */
var PrepetApp = angular.module("prepetApp", [
    "ui.router", 
    "ui.bootstrap", 
    "oc.lazyLoad",  
    "ngSanitize",
    "ngTable"
]); 

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
PrepetApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/
/**
`$controller` will no longer look for controllers on `window`.
The old behavior of looking on `window` for controllers was originally intended
for use in examples, demos, and toy apps. We found that allowing global controller
functions encouraged poor practices, so we resolved to disable this behavior by
default.

To migrate, register your controllers with modules rather than exposing them
as globals:

Before:

```javascript
function MyController() {
  // ...
}
```

After:

```javascript
angular.module('myApp', []).controller('MyController', [function() {
  // ...
}]);

Although it's not recommended, you can re-enable the old behavior like this:
0
```javascript
angular.module('myModule').config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);
**/

//AngularJS v1.3.x workaround for old style controller declarition in HTML
PrepetApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
PrepetApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: 'assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout3',
        TruncateDecimal: function (value, precision) {
            if (value > 0) {
                var step = Math.pow(10, precision);
                var tmp = Math.trunc(step * value);
                return parseFloat(tmp / step);
            }
            else
                return value;
        }
    };
    
    $rootScope.settings = settings;
    return settings;
}]);

/* Setup App Main Controller */
PrepetApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        var $jq = jQuery.noConflict();
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 

    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
PrepetApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
PrepetApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar($state); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
PrepetApp.controller('QuickSidebarController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
        setTimeout(function () {
            QuickSidebar.init(); // init quick sidebar        
        }, 2000)
    });
}]);

/* Setup Layout Part - Sidebar */
PrepetApp.controller('PageHeadController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {        
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Theme Panel */
PrepetApp.controller('ThemePanelController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
PrepetApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
PrepetApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider
               .when('/', '/gubernatura-por-entidad')
               .otherwise('/');

    
    $stateProvider

        .state('gob_entidad', {
            url: "/gubernatura-por-entidad",
            cache: false,
             templateUrl: "app/componentes/views/gob_entidad.html",
             data: { pageTitle: 'Gubernatura - Entidad' },
             controller: "gobEntidadController",
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load({
                         name: 'PrepetApp',
                         insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                         files: [
                            'app/componentes/controllers/gob_ent_contoller.js',
                         ],
                         cache: false
                     });
                 }]
             }
         })

        .state('gob_detalle_entidad', {
            url: "/gubernatura-detalle-por-entidad",
            cache: false,
            templateUrl: "app/componentes/views/gob_detalle_entidad.html",
            data: { pageTitle: 'Gubernatura - Detalle de la Entidad' },
            controller: "gobDetalleEntidadController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        //insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/gob_detalle_entidad_controller.js',
                        ],
                        cache: false
                    });
                }]
            }
        })

        .state('gob_distrito', {
            url: "/gubernatura-por-distritos",
            cache: false,
             templateUrl: "app/componentes/views/gob_distritos.html",
             data: { pageTitle: 'Gubernatura - Distritos' },
             controller: "gobDistritoController",
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load({
                         name: 'PrepetApp',
                         //insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                         files: [
                            /* '../assets/global/plugins/datatables/datatables.min.css',
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            '../assets/global/plugins/datatables/datatables.all.min.js',
                            '../assets/pages/scripts/table-datatables-managed.min.js',*/

                             /*'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                             'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',*/
                             'app/componentes/controllers/gob_distrito_controller.js',
                         ],
                         cache: false
                     });
                 }]
             }
         })

        .state('seccion_distrito', {
            url: "/gubernatura-desglose-por-secciones/:pIdDistrito",
            cache: false,
            templateUrl: "app/componentes/views/seccion_distrito.html",
            data: { pageTitle: 'Gubernatura' },
            controller: "seccionDistritoController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        //insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                           /*'../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',*/

                            'app/componentes/controllers/seccion_distrito.js',
                        ],
                        cache: false
                    });
                }]
            }
        })

        .state('seccion_casilla', {
            url: "/detalle-por-casilla/:pIdSeccion/:modulo/:pIdDistrito",
            templateUrl: "app/componentes/views/seccion_casilla.html",
            data: { pageTitle: "" },
            controller: "seccionCasillaController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/seccion_casilla.js',
                        ], cache: false
                    });
                }]
            }
        })

         /*seccion de diputados*/
        .state('dip_entidad', {
            url: "/diputaciones-locales-por-entidad",
            cache: false,
            templateUrl: "app/componentes/views/dip_entidad.html",
            data: { pageTitle: 'Diputaci\u00F3n - Entidad' },
            controller: "dipEntController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                              //'assets/global/plugins/jquery-mapael/js/jquery-3-3-1.min.js',
                              //'assets/global/plugins/jquery-mapael/js/jquery.mousewheel.min.js',
                              //'assets/global/plugins/jquery-mapael/js/raphael.min.js',
                              //'assets/global/plugins/jquery-mapael/js/jquery.mapael.js',
                              //'assets/global/plugins/jquery-mapael/js/maps/distritos_tabasco.js',
                              'app/componentes/controllers/dip_ent_controller.js'
                        ], cache: false
                    });
                }]
            }
        })

        .state('dip_detalle_entidad', {
            url: "/diputaciones-locales-detalle-por-entidad",
            cache: false,   
            templateUrl: "app/componentes/views/dip_detalle_entidad.html",
            data: { pageTitle: 'Diputaci\u00F3n - Detalle del Distrito' },
            controller: "dipDetalleEntidadController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/dip_detalle_entidad_controller.js',
                        ], cache: false
                    });
                }]
            }
        })

        .state('dip_detalle_distrito', {
            url: "/diputaciones-locales-detalle-por-distritos/:pIdDistrito",
            cache: false,
            templateUrl: "app/componentes/views/dip_detalle_distrito.html",
            data: { pageTitle: 'Diputaci\u00F3n - Detalle-Distrito' },
            controller: "dipDetalleDistritoController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/dip_detalle_distrito_controller.js',
                        ], cache: false
                    });
                }]
            }
        })

        .state('dip_seccion_distrito', {
            url: "/diputaciones-locales-secciones-por-distritos/:pIdDistrito",
            cache: false,
            templateUrl: "app/componentes/views/dip_seccion_distrito.html",
            data: { pageTitle: 'Diputaciones - Detalle del Distrito' },
            controller: "dipSeccionDistritoController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/dip_seccion_distrito_controller.js',
                        ], cache: false
                    });
                }]
            }
        })
    

        /*seccion de ayuntamientos*/
        .state('ayuntamiento_entidad', {
            url: "/presidencias-municipales-por-entidad",
            cache: false,
            templateUrl: "app/componentes/views/ayuntamiento_entidad.html",
            data: { pageTitle: 'Ayuntamientos - Entidad' },
            controller: "ayuntamientoEntController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/ayuntamiento-entidad-controller.js',
                        ], cache: false
                    });
                }]
            }
        })

        .state('ayuntamiento_municipio', {
            url: "/presidencias-municipales",
            cache: false,
            templateUrl: "app/componentes/views/ayuntamiento_municipios.html",
            data: { pageTitle: 'Ayuntamientos - Detalle de la Entidad' },
            controller: "ayuntamientoMunicipioController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/ayuntamiento-municipio-controller.js',
                        ], cache: false
                    });
                }]
            }
        })

        .state('ayuntamiento_detalle', {
            url: "/presidencias-municipales-detalle/:pIDMunicipio",
            cache: false,
            templateUrl: "app/componentes/views/ayuntamiento_detalle_municipio.html",
            data: { pageTitle: 'Ayuntamientos - Detalle por Municipios' },
            controller: "ayuntamientoDetalleController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'PrepetApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'app/componentes/controllers/ayuntamiento-detalle-controller.js',
                        ], cache: false
                    });
                }]
            }
        })

        .state('ayuntamiento_desglose_seccion', {
            url: "/presidencias-municipales-desglose-por-seccion/:pIdMunicipio",
            cache: false,
        templateUrl: "app/componentes/views/ayuntamiento_seccion.html",
        data: { pageTitle: 'Ayuntamiento - Detalle por Municipio' },
        controller: "ayuntamientoSeccionController",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'PrepetApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        'app/componentes/controllers/ayuntamiento-seccion-controller.js',
                    ],cache: false
                });
            }]
        }
    })

    .state('ayuda_prepet', {
        url: "/ayuda/:id?av?acta",
        cache: false,
        templateUrl: "app/componentes/views/ayuda.html",
        data: { pageTitle: 'Ayuda' },
        controller: "ayudaController",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'PrepetApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        'app/componentes/controllers/ayuda-controller.js',
                    ], cache: false
                });
            }]
        }
    })


    .state('bd_Descarga', {
        url: "/DescargaBD",
        cache: false,
        templateUrl: "app/componentes/views/descargaBD.html",
        data: { pageTitle: 'Ayuda' },
        controller: "bdController",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'PrepetApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        'app/componentes/controllers/descargaBDController.js',
                    ], cache: false
                });
            }]
        }
    })

    .state('detalleCasilla', {
        url: "/detallePorCasilla",
        cache: false,
        templateUrl: "app/componentes/views/detalle-Por-Casilla.html",
        data: { pageTitle: 'DetallePorCasilla' },
        controller: "detallePorCasilla",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'PrepetApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        'app/componentes/controllers/detalle-por-casilla-controller.js',
                    ], cache: false
                });
            }]
        }
    })

    //$locationProvider.html5Mode(true);
}]);

/* Init global settings and run the app */
PrepetApp.run(["$rootScope", "settings", "$state", "$stateParams", function ($rootScope, settings, $state, $stateParams) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
    $rootScope.$stateParams = $stateParams;
   
   
    $rootScope.$on('$routeChangeStart', function (event, next, current) {       
    });

    
    //var miscelanea = 60000;   
    //iniciaInterval = function () {
    //    if (miscelanea > 0) {
    //        tiempo = $interval(function () {
    //            $rootScope.$emit('Refresh', { mensaje: "Hola Mundo" });
    //            iniciaInterval();
    //        }, miscelanea, true);
    //        $rootScope.$on('$destroy', function () { $interval.cancel(tiempo); });
    //    }
    //}
    //iniciaInterval();

    
}]);


