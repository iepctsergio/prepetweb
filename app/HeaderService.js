﻿PrepetApp.factory('HeaderService', ['$http', '$q', function ($http, $q) {
    var request_gob = {
        method: 'get',
        url: 'app/json/Estructura/estructuraGOB.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };
    var request_dip = {
        method: 'get',
        url: 'app/json/Estructura/estructuraDIP.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_mun = {
        method: 'get',
        url: 'app/json/Estructura/estructuraPRES_MUN_REG.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    return {
        getGobEntidad: getGobEntidad,
        getDiputacion:getDiputacion,
        getMunicipios: getMunicipios,
        getCasilla:getCasilla 

    }

    function getGobEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_gob)
            .success(function (jsonData) {
                defered.resolve(jsonData.gobEntidad);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }    

    function getDiputacion() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http(request_dip)
           .success(function (jsonData) {
               defered.resolve(jsonData.Diputacion);
           })
           .error(function (error) {
               defered.reject(err)
           })
        return promise;
    }

    function getMunicipios() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http(request_mun)
           .success(function (jsonData) {
               defered.resolve(jsonData.Municipios);
           })
           .error(function (error) {
               defered.reject(err)
           })
        return promise;
    }

    function getCasilla(modulo,seccion) {
        var defered = $q.defer();
        var promise = defered.promise;
        
        switch (modulo) {
            case '1S':
            case '1D':
                $http(request_gob)
                 .success(function (jsonData) {
                     var secciones = jsonData.gobEntidad.SeccionPartidos;
                     defered.resolve(secciones);
                 })
                .error(function (error) {
                    defered.reject(err)
                })
                return promise;
                break;
            case '2D':
            case '2S':
                $http(request_dip)
                  .success(function (jsonData) {                      
                      
                      var secciones = jsonData.Diputacion.Secciones;
                      if (secciones.length > 0) {
                          for (var i = 0; i < secciones.length; i++) {
                              if ("" + secciones[i].id == seccion) {
                                  defered.resolve(secciones[i].Partidos);
                                  break;
                              }
                          }
                      }
                      else {
                          var Partidos = [];
                          defered.resolve(Partidos);
                      }
                  })
                 .error(function (error) {
                     defered.reject(err)
                 })
                return promise;
                break;
            case 'DIP':
                $http(request_dip)
                 .success(function (jsonData) {
                     defered.resolve(jsonData.Diputacion.Partidos);
                 })
                .error(function (error) {
                    defered.reject(err)
                })
                return promise;
                break;
            case '3D':
            case '3S':
                $http(request_mun)
                 .success(function (jsonData) {
                     var secciones = jsonData.Municipios.Secciones;
                     if (secciones.length > 0) {
                         for (var i = 0; i < secciones.length; i++) {
                             if ("" + secciones[i].id == seccion) {
                                 defered.resolve(secciones[i].Partidos);
                                 break;
                             }
                         }
                     }
                     else {
                         var Partidos = [];
                         defered.resolve(Partidos);
                     }
                 })
                .error(function (error) {
                    defered.reject(err)
                })
                return promise;
                break;
           
        }

    }

}]);