﻿angular.module('PrepetApp', [])
    .controller('gobEntidadController', function ($rootScope, $scope, $timeout, $filter, $templateCache, $state, $interval, DataService, HeaderService) {
        var vm = this;
        var result = {};
        var resultHeader = {};
        var miscelanea = 60000;
        var tiempo;
        var num_decimal=4
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
        $('html, body').animate({ scrollTop: 0 }, 500);
        Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_gob_entidad'), $state);       
        $rootScope.settings.layout.pageBodySolid = true;
        $rootScope.settings.layout.pageSidebarClosed = true;

       
    });
        

   /* var destroyHandler = $rootScope.$on("Refresh", function (event, name) {
        alert(name.mensaje);
        $scope.$on('$destroy', destroyHandler);
    });*/

   



    BusyLoading = function () {
        var option = { animate: true };
        App.startPageLoading(option);
    }

    vm.loadPage = function ()
    {     

        BusyLoading();

        vm.titulo = "Gubernatura ";
        vm.ambitoEstadistica = "- Entidad";
        vm.descripcion = "El total de votos calculado y porcentaje que se muestran, se refieren a los votos asentados en las Actas PREP hasta el momento. \nPor presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%.\nEl total de votos mostrado a nivel Entidad representa la suma del voto emitido en territorio estatal."
        vm.VotosCandidatura = "Votos por Candidatura";
        vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
        vm.radioSelected = 1;
        vm.votacionMaxima = -1;

        vm.Chart = {};
        vm.Chart.p1 = 0;
        vm.Chart.p2;
        vm.Chart.p3;
        vm.Chart.p4;
        vm.Chart.p5;
        vm.maxValueChart = 0;
        vm.sumaTotal = 0;
       
        //Header
        HeaderService.
        getGobEntidad()
            .then(function (data) {
                
                resultHeader = data;
                vm.candidatos = data.candidatos;
                vm.length_cart = vm.candidatos.length;
                vm.tabla_grafica = data.tablaVotosCandidato;
                getDataSource();
            })
            .catch(function (err)
            {
                App.stopPageLoading();
        })
       

        

        //Refrescar la pagina: Hay que implementarlo en todo el sistema
        
       
       
    }

    iniciaInterval = function () {
        if (miscelanea > 0) {            
            tiempo = $interval(function () {
                getDataSource();
            }, miscelanea, true);
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
        }
    }

    getDataSource = function () {
        DataService
      .getGubernaturaEntidad()
      .then(function (data) {

          App.stopPageLoading();
          result = data;
          miscelanea = data.informacionGral.timer_update;
          vm.actasCapturadas = data.informacionGral.actasCapturadas;
          vm.actasEsperadas = data.informacionGral.actasEsperadas;
          vm.actasRecibidas = data.informacionGral.actasRecibidas;
          vm.contabilizadas = data.informacionGral.contabilizadas;
          vm.actasCasillaUrbana = data.informacionGral.actasCasillaUrbana;
          vm.actasCasillaNoUrbana = data.informacionGral.actasCasillaNoUrbana;
          vm.actasListaNominal = data.informacionGral.actasListaNominal;
          vm.casillaBasica = data.informacionGral.casillaBasica;
          vm.casillaContigua = data.informacionGral.casillaContigua;
          vm.casillaExtraordinaria = data.informacionGral.casillaExtraordinaria;
          vm.casillaEspecial = data.informacionGral.casillaEspecial;
          vm.TotalActasListaNominal = data.informacionGral.TotalActasListaNominal;
          vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
          vm.porcentajecontabilizadas =data.informacionGral.porcentajecontabilizadas;
          vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
          vm.barraporcentaje = $rootScope.settings.TruncateDecimal(((vm.actasListaNominal / vm.TotalActasListaNominal) * 100), num_decimal);         
          vm.participacion = data.informacionGral.Participacion;
          vm.horaCorte = data.informacionGral.horaCorte;
          vm.fechaCorte = data.informacionGral.fechaCorte;
          vm.ultimo_corte = data.informacionGral.ultimo_corte;
         
          if (data.votosCandidatura.candidatos.length > 0)
          {
              //vm.candidatos = data.votosCandidatura.candidatos;            
              angular.forEach(vm.candidatos, function (value) {
                  angular.forEach(data.votosCandidatura.candidatos, function (item) {
                      if (value.Clave === item.logoPartido) {
                          value.porcentaje = item.porcentaje;
                          value.totalVotos = item.totalVotos;
                          value.max = item.max;
                          value.orden = item.orden;
                          if (item.partidos.length > 1)
                              value.partidos = item.partidos;
                          else
                              value.partidos[0].votos = item.partidos[0].votos;
                      }
                  })
              })

              //vm.TotalVotos = data.votosCandidatura.totalVotos;
              //vm.length_cart = vm.candidatos.length;
          }        
         
          
              //vm.tabla_grafica = data.votosCandidatura.tablaVotosCandidato;
          if (vm.radioSelected == 1) {
              if (data.votosCandidatura.tablaVotosCandidato.length > 0)
                  LlenaTablaGrafica(data.votosCandidatura.tablaVotosCandidato);
          }
          else if (result.votosCandidaturaIndependiente.tablaVotosCandidato.length > 0) 
          {
                  LlenaTablaGrafica(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                  vm.calcularMaximos();
          }         
         

          $timeout(function () {
              var $jq = jQuery.noConflict();
              var chart = $("#graficaEstadistica").highcharts();             
              chart.series[1].data[0].update(parseFloat(vm.porcentajecapturadas));
              chart.series[2].data[0].update(parseFloat(vm.porcentajecontabilizadas));
          }, 100);

          vm.sumaTotal = vm.Suma(vm.candidatos, "totalVotos");//vm.number_format(,2);

          if (vm.ultimo_corte) {
              $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
          }
          else
          iniciaInterval();
      })
      .catch(function (err) {
          App.stopPageLoading();
      })
    }

    

    LlenaTablaGrafica = function (data) {
        var sum = 0; var p=0;
        angular.forEach(vm.tabla_grafica, function (value) {
            angular.forEach(data, function (item) {
                if (value.Clave === item.logoPartido) {
                    value.porcentaje = item.porcentaje;
                    value.totalVotos = item.totalVotos;
                    sum += value.totalVotos;
                    value.max = item.max;
                    p+=value.porcentaje;
                }
            })
        })
        vm.TotalVotos = sum;
        vm.PorcentajeTotal = "100.0000%";
    }

    vm.calcularMaximos= function(){
        var max = Math.max.apply(Math, vm.tabla_grafica.map(function (item) { return item.totalVotos; }));
        vm.valorMaximo = max;
        if (max > 0) {
            vm.Chart.p2 = vm.porcentajeGrafica(max, 25);
            vm.Chart.p3 = vm.porcentajeGrafica(max, 50);
            vm.Chart.p4 = vm.porcentajeGrafica(max, 75);
            vm.Chart.p5 = vm.porcentajeGrafica(max, 100);
        } else {
            vm.Chart.p2 = 25;
            vm.Chart.p3 = 50;
            vm.Chart.p4 = 75;
            vm.Chart.p5 = 100;           
        }
    }

    vm.Suma = function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;
        var sum = 0;
        angular.forEach(data, function (value) {
            sum = sum + parseInt(value[key], 10);
            /*obtener votacion maxima*/
            if (vm.votacionMaxima < parseInt(value[key], 10)) {
                vm.votacionMaxima = parseInt(value[key], 10);                
            }
        });
        return sum;
    }

    vm.alturaBar = function (valormaximo, total) {
        if (angular.isUndefined(valormaximo))
            return 200;
        valormaximo = valormaximo.toString();
        total = total.toString();
        var totalInteger = parseInt(total.replace(',', ''));
       
        var tot = parseInt(valormaximo.replace(',', ''));
        if (tot <= 0) {
            return 200;//0
        } else {
            return 200 - ((totalInteger * 200) / vm._porcentajeGrafica(valormaximo, 100));
        }
   }  

    vm.porcentajeGrafica = function (total, por)
    {
        var result=vm._porcentajeGrafica(total, por);
        return vm.number_format(result,0);
    };

    vm._porcentajeGrafica = function (total, por) {
        
        var tot = parseInt(total.toString().replace(/,/g, ""));
        var next = parseInt(tot.toString()[0]) + 1;

        var nextInt = next * Math.pow(10, tot.toString().length - 1);

        if ((nextInt - tot) <= nextInt * 0.1) {
            nextInt = nextInt + (nextInt * 0.1);
        }

        return nextInt * (por / 100);
    }

    vm.number_format = function (amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    $scope.$watch("ctrl.radioSelected", function (val) {
        if (val)
        {            
            if (val == 1)
            {
               
                if (result.votosCandidatura) {
                    if (result.votosCandidatura.tablaVotosCandidato.length > 0) {
                        vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                        LlenaTablaGrafica(result.votosCandidatura.tablaVotosCandidato);                       
                    }
                    else {
                        vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                    }
                }
            }
            else
            {
                if (result.votosCandidaturaIndependiente.tablaVotosCandidato.length > 0) {
                    vm.tabla_grafica = resultHeader.Partidos;// result.votosCandidaturaIndependiente.tablaVotosCandidato;
                    LlenaTablaGrafica(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                    if (result.votosCandidaturaIndependiente.tablaVotosCandidato.length > 0)
                        vm.calcularMaximos();
                }
                else {
                    vm.tabla_grafica = resultHeader.Partidos;
                }
            }
        }
    })

    vm.reloadRoute = function ()
    {
        BusyLoading();
        $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
        getDataSource();
    }
    vm.exportarDatos = function () {
        window.location.href = $rootScope.settings.basedatos;
    }

  /*  var c = 0;
    $scope.message = "This DIV is refreshed " + c + " time.";
    var timer = $interval(function () {
        $scope.message = "This DIV is refreshed " + c + " time.";
        c++;
        if (c === 20) {
            c = 0;
            BusyLoading();
            getDataSource();
            
        }
    }, 1000);*/
});
