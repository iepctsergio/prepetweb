﻿angular.module('PrepetApp', [])
    .controller('dipDetalleDistritoController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService, HeaderService, $interval) {
        var vm = this;
        var result = {};
        vm.modulo = "2D";//Diputacion distrito    >diputacionsecciondistrito
        var encabezado = {};
        vm.distritoSeleccionado = undefined;

        var resultHeader = {};
        var miscelanea = 60000;
        var tiempo;
        

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_diputacion_distrito'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;
        });

        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }
        //****variable del encabzado**************    
        vm.radioSelected = 1;
        vm.VotosCandidatura = "Votos por Candidatura";
        vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
        //vm.detalleCasilla = "Detalle Casilla";
        //vm.radioSelectedCasilla = true;
        vm.tipoAmbito = "2S";
        vm.mostrarNombreCandidato = true;
         
        //****variable del encabzado**************
        vm.titulo = 'Diputaciones ';
        vm.ambitoEstadistica = '- Detalle del Distrito';
        vm.tituloTabla = 'Distribución de votos por candidatura a nivel Distrito';
        vm.tituloVotacionPartidoTabla = 'Distribución de votos por Partido Político y Candidatura Independiente a nivel Distrito';
        vm.descripcion = "El total de votos calculado y porcentaje que se muestran, se refieren a los votos asentados en las Actas PREP hasta el momento. Por presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%.";
        vm.tipoSectorEleccion = 'el Distrito';
        vm.titulo_detalle_votos = "Detalle de votos por Distritos";
        vm.titulo_actas = "Actas del Distritos";
        vm.item_acta = {};
        vm.inicio = true;
        vm.detallevotos_primera_columna = "Distritos";
        /************************************************************************/
        vm.loadPage = function () {

            vm.Distritos = [];
            vm.DistritosPartidos = [];
            vm.tabla_grafica = [];
            vm.objVotosCandidatura = [];
            vm.objVotosPartido = [];
            vm.ActasDistritos = [];
            vm.votacionMaxima = -1;

            BusyLoading();

            HeaderService.
       getDiputacion()
           .then(function (data) {
               BusyLoading();
               encabezado = data;
               vm.Distritos = angular.copy(data.Distritos);
               vm.DistritosPartidos = angular.copy(data.Distritos);
               
              getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })
           
                   
        }//Fin de load page
        /****************************/

        getDataSource = function () {
             DataService
       .getDiputacionDetalleDistrito()
       .then(function (data) {
           result = data;

           for (var i = 0; i < vm.Distritos.length; i++) {
               var muni = data.votosCandidaturaIndependiente.listaDetalleDistrito.filter(function (item) { return item.id == vm.DistritosPartidos[i].id; });
               
               vm.Distritos[i].tablaActasDistrito = [];
               vm.Distritos[i].tablaVotosPartido = [];
               if (muni.length > 0) {
                   if (muni[0].tablaActasDistrito.length > 0)
                       vm.Distritos[i].tablaActasDistrito = muni[0].tablaActasDistrito;
                   if (muni[0].tablaVotosPartido.length > 0)
                   {
                       vm.Distritos[i].tablaVotosPartido = muni[0].tablaVotosPartido;
                   }
               }
           }

           for (var i = 0; i < vm.DistritosPartidos.length; i++) {
               var muni = data.votosCandidatura.listaDetalleDistrito.filter(function (item) { return item.id == vm.Distritos[i].id; });
               vm.DistritosPartidos[i].tablaActasDistrito = [];
               vm.DistritosPartidos[i].tablaVotosPartido = [];
               
               if (muni.length > 0)
               {
                   if (muni[0].tablaActasDistrito.length > 0)
                       vm.DistritosPartidos[i].tablaActasDistrito = muni[0].tablaActasDistrito;
                   if (muni[0].tablaVotosPartido.length > 0)
                   {
                       vm.DistritosPartidos[i].tablaVotosPartido = muni[0].tablaVotosPartido;                      
                   }
               }
           }
           var parametro = angular.isUndefined(vm.distritoSeleccionado) ? $rootScope.$stateParams.pIdDistrito : vm.distritoSeleccionado.id;
          
           //var parametro = $rootScope.$stateParams.pIdDistrito;
           
           
           if (vm.radioSelected == 1) {
               aplicaInformacionGeneral(data.informacionGral.informacionGralDistrito);
               var seleccionado = vm.DistritosPartidos.filter(function (item) { return item.id == parametro; });
               if (seleccionado.length > 0)
               {
                   //verificar si actualiza
                   vm.distritoSeleccionado = null;
                   vm.distritoSeleccionado = seleccionado[0];
               }
               else {
                   if (angular.isUndefined(vm.distritoSeleccionado))
                       vm.distritoSeleccionado = vm.DistritosPartidos[0];
                   else {
                       var seleccionado = vm.DistritosPartidos.filter(function (item) { return item.id == vm.distritoSeleccionado.id; });
                       if (seleccionado.length > 0) {
                           generarDatosCandidatura(seleccionado[0]);
                       }
                   }
               }

           }
           else {
               aplicaInformacionGeneral(data.informacionGral.informacionGralPartidos);
               var seleccionado = vm.Distritos.filter(function (item) { return item.id == parametro; });
               if (seleccionado.length > 0) {
                   vm.distritoSeleccionado = null;
                   vm.distritoSeleccionado = seleccionado[0];
               }
               else {
                   if (angular.isUndefined(vm.distritoSeleccionado))
                       vm.distritoSeleccionado = vm.Distritos[0];
                   else {
                       var seleccionado = vm.Distritos.filter(function (item) { return item.id == vm.distritoSeleccionado.id; });
                       if (seleccionado.length > 0) {
                           generarDatoPorPartidos(seleccionado[0]);
                       }
                   }
               }
           }

           App.stopPageLoading();

           if (vm.ultimo_corte)
           {
               $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
           }
           else
             iniciaInterval();
          
       })
       .catch(function (err) {
           App.stopPageLoading();
           iniciaInterval();
       })
        }
       
        aplicaInformacionGeneral = function (data) {

            miscelanea = data.timer_update;
            vm.actasCapturadas = data.actasCapturadas;
            vm.actasEsperadas = data.actasEsperadas;
            vm.actasRecibidas = data.actasRecibidas;
            vm.contabilizadas = data.contabilizadas;
            vm.actasCasillaUrbana = data.actasCasillaUrbana;
            vm.actasCasillaNoUrbana = data.actasCasillaNoUrbana;
            vm.actasListaNominal = data.actasListaNominal;

            vm.TotalActasListaNominal = data.TotalActasListaNominal;
            vm.porcentajecapturadas = data.porcentajecapturadas;
            vm.porcentajecontabilizadas = data.porcentajecontabilizadas;

            vm.barraporcentaje = $rootScope.settings.TruncateDecimal(((vm.actasListaNominal / vm.TotalActasListaNominal) * 100), 4);
            vm.participacion = data.Participacion;
            vm.horaCorte = data.horaCorte;
            vm.fechaCorte = data.fechaCorte;
            vm.ultimo_corte = data.ultimo_corte;

            vm.porcentajeParticipacionCiudadana = data.porcentajeParticipacionCiudadana;
            vm.casillaBasica = data.casillaBasica;
            vm.casillaContigua = data.casillaContigua;
            vm.casillaExtraordinaria = data.casillaExtraordinaria;
            vm.casillaEspecial = data.casillaEspecial;
        }


        vm.Suma = function (data, key) {
            if (angular.isUndefined(data) || angular.isUndefined(key))
                return 0;
            var sum = 0;
            angular.forEach(data, function (value) {
                sum = sum + parseInt(value[key], 10);
            });
            return sum;
        }
        

        $scope.$watch("ctrl.radioSelected", function (val) {
            if (!angular.isUndefined(val)) {
                if (val == 1) {
                    
                    if (!angular.isUndefined(result.informacionGral)) 
                        aplicaInformacionGeneral(result.informacionGral.informacionGralDistrito);
                    var seleccionado = vm.DistritosPartidos.filter(function (item) { return item.id == vm.distritoSeleccionado.id; });
                    if (seleccionado.length > 0) {
                        generarDatosCandidatura(seleccionado[0]);
                    }
                }
                else {
                    aplicaInformacionGeneral(result.informacionGral.informacionGralPartidos);
                    var seleccionado = vm.Distritos.filter(function (item) { return item.id == vm.distritoSeleccionado.id; });
                    
                    if (seleccionado.length > 0) {
                        generarDatoPorPartidos(seleccionado[0]);
                    }
                }
            }
          
        })

        generarDatosCandidatura = function (val) {

            vm.tabla_grafica = [];
            vm.objVotosCandidatura = [];
            vm.objVotosCandidatura = angular.copy(val.tablaVotosPartido);
            
            if (vm.objVotosCandidatura.length > 0) {
                var max = Math.max(... vm.objVotosCandidatura.map(function(item) {return item.total}));

                var mxmo = vm.objVotosCandidatura.filter(function (item) { return item.total == max; });
                var tamano = mxmo.length;

                vm.votacionMaxima = max;
                var sum = 0;
                
                for (var i = 0; i < vm.objVotosCandidatura.length; i++) {
                    var dato = {};
                    dato.nombreCandidato = vm.objVotosCandidatura[i].candidato;
                    dato.logoPartido = vm.objVotosCandidatura[i].logoPartido;
                    dato.porcentaje = vm.objVotosCandidatura[i].porcentaje;
                    dato.totalVotos = vm.objVotosCandidatura[i].total;
                    sum = sum + dato.totalVotos;
                    if (tamano == 1) {
                        dato.max = dato.totalVotos == max ? true : false;
                    }
                    else {
                        dato.max = false;
                    }
                    vm.tabla_grafica.push(dato);
                }
                vm.TotalVotos = vm.totalVotos = sum;
                vm.Porcentaje ="100.0000%";
            }
            else {
                vm.tabla_grafica = [];
                vm.objVotosCandidatura = [];
                vm.TotalVotos = null;
                vm.Porcentaje = "";
                var header = encabezado.DistritosPartidos.filter(function (item) { return item.id == val.id; });
                if (header.length > 0) {
                    if (header[0].Candidatura.length > 0) {
                        vm.tabla_grafica = header[0].Candidatura;
                        for (var i = 0; i < header[0].Candidatura.length; i++) {
                            if (header[0].Candidatura[i].Clave !== 'VN' && header[0].Candidatura[i].Clave !== 'CNR') {
                                var obj = {}
                                obj.logoPartido = header[0].Candidatura[i].logoPartido;
                                obj.candidato = angular.isUndefined(header[0].Candidatura[i].NombreCandidato) ? header[0].Candidatura[i].nombreCandidato : header[0].Candidatura[i].NombreCandidato;
                                
                                if (!angular.isUndefined(header[0].Candidatura[i]))
                                    obj.partido = header[0].Candidatura[i].partido;
                                else
                                    obj.partido = [];
                                vm.objVotosCandidatura.push(obj);
                            }
                        }
                    }
                }

            }
            vm.ActasDistritos = angular.copy(val.tablaActasDistrito);
        }

        generarDatoPorPartidos = function (val) {
            vm.tabla_grafica = [];
            vm.objVotosPartido = [];
            vm.objVotosPartido = angular.copy(val.tablaVotosPartido);
            if (vm.objVotosPartido.length > 0) {
                var total = 0;
                vm.objVotosPartido.forEach(function (value, key) {
                    total = total + value.Total;
                })
                var max = Math.max(... vm.objVotosPartido.map(function(item) {return item.Total}));
                var mxmo = vm.objVotosCandidatura.filter(function (item) { return item.total == max; });
                var tamano = mxmo.length;

                for (var i = 0; i < vm.objVotosPartido.length; i++) {
                    var dato = {};
                    dato.nombreCandidato = vm.objVotosPartido[i].Logo == '' ? vm.objVotosPartido[i].Nombre : '';
                    dato.logoPartido = vm.objVotosPartido[i].Logo;
                    dato.totalVotos = vm.objVotosPartido[i].Partido + vm.objVotosPartido[i].Coalicion;
                    dato.porcentaje = $rootScope.settings.TruncateDecimal(((dato.totalVotos / total) * 100),4);
                    if (tamano == 1) {
                        dato.max = dato.totalVotos == max ? true : false;
                    }
                    else {
                        dato.max = false;
                    }
                    vm.tabla_grafica.push(dato);
                }
                vm.TotalVotos = vm.totalVotos = total;
                vm.Porcentaje = "100.0000%";
            }
            else {
                vm.tabla_grafica = [];
                vm.objVotosPartido = [];
                vm.TotalVotos = null;
                vm.Porcentaje = "";
                
                var header = encabezado.DistritosPartidos.filter(function (item) { return item.id == val.id; });

                if (header.length > 0) {
                    if (header[0].Partidos.length > 0) {
                        vm.tabla_grafica = header[0].Partidos;
                        for (var i = 0; i < header[0].Partidos.length; i++) {
                            if (header[0].Partidos[i].Clave !== 'VN' && header[0].Partidos[i].Clave !== 'CNR') {
                                var obj = {}
                                obj.Logo = header[0].Partidos[i].logoPartido;
                                obj.nombreCandidato = header[0].Partidos[i].NombreCandidato;
                                if (!angular.isUndefined(header[0].Candidatura[i]))
                                    obj.partido = header[0].Candidatura[i].partido;
                                else
                                    obj.partido = [];
                               
                                vm.objVotosPartido.push(obj);
                            }
                        }
                    }
                }
            }
            vm.ActasDistritos = angular.copy(val.tablaActasDistrito);
        }

        $scope.$watch("ctrl.distritoSeleccionado", function (val)
        {
            if (angular.isUndefined(val) || angular.isUndefined(val.id))
                return;
            if (val) {
                if (vm.radioSelected == 1)
                {
                    generarDatosCandidatura(val);
                }
                else {
                    generarDatoPorPartidos(val);
                }
            }
        })
        /*********************************************************/
        vm.encabezadoTabla1 = 'Resumen de la votación';
        vm.titulo_actas = "Actas de la Entidad";

       
        
           

        vm.reloadRoute = function () {
            BusyLoading();
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            getDataSource();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }
    });