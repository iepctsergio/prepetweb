﻿angular.module('PrepetApp', [])

    .controller('seccionDistritoController', function ($rootScope, $scope, $timeout, $filter, $state, DataService,HeaderService, $interval) {
        var vm = this;
        var result = {};
        vm.modulo = "1S";   //gobernatura seccion  > seccion casilla

        var miscelanea = 60000;
        var tiempo;
        var encabezado = {};
        var update = false;

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_gob_seccion'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;           
        });

        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }
       
        vm.loadPage = function () {
            BusyLoading();
            vm.titulo = "Gobernatura ";
            vm.ambitoEstadistica = "- Sección por Distrito";
            vm.descripcion = "Cantidad de votos en cada una de las casillas de esta Sección, conforme a la información de las Actas PREP."
            vm.VotosCandidatura = " Secciones por Distrito";

            vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
            vm.radioSelected = 1;
            vm.GrupoSelector = true;
            vm.ventana = "Distrito";
            vm.titulo_detalle_votos = "Detalle de votos por sección";
            vm.titulo_actas = "Actas por sección";
            vm.detallevotos_primera_columna = "Detalle de votos por Sección";
            vm.visible = true;
            vm.tabla_grafica=[];

            HeaderService.
            getGobEntidad()
                  .then(function (data) {
                      BusyLoading();
                      encabezado = data;
                      vm.Distritos = data.Distritos;
                      vm.Partidos = data.SeccionPartidos;                      
                      getDataSource();
                  })
                  .catch(function (err) {
                      App.stopPageLoading();
                  })
        }
              
        getDataSource = function () {
            DataService
           .getGubernaturaDesgloseSeccion()
           .then(function (data) {        
               result = data;
               vm.actasCapturadas = data.informacionGral.actasCapturadas;
               vm.actasEsperadas = data.informacionGral.actasEsperadas;
               vm.actasRecibidas = data.informacionGral.actasRecibidas;
               vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
               vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
               vm.horaCorte = data.informacionGral.horaCorte;
               vm.fechaCorte = data.informacionGral.fechaCorte;
               vm.ultimo_corte = data.informacionGral.ultimo_corte;
                            
               if (update) {
                   vm.ActasDistritos = [];
                   vm.ltSecciones = [];
                   vm.tabla_grafica = [];
                   vm.visible = false;
                   vm.idSeccion = vm.pIdDistrito;
                   vm.visible = false;
                   var ltSecciones = result.ltSecciones.filter(function (item) { return item.distrito == vm.pIdDistrito; });
                   var secciones = result.Dist_Sec_Casilla.filter(function (item) { return item.AMBITO_ID == vm.pIdDistrito && item.PrincipioEleccion_ID == 1; });
                  
                   var actas = result.Actas.filter(function (item) { return item.Municipio == vm.pIdDistrito; });
                  
                   if (ltSecciones.length == 0 && secciones == 0 && actas == 0)
                   {
                       for (var i = 0; i < vm.Partidos.length; i++) {
                           var object = {};

                           vm.tabla_grafica.push(object);
                       }
                   }
                   else {
                       agrupaDistritoSeccion(ltSecciones, secciones, actas);
                   }                   
                  App.stopPageLoading();
                  update = false;
               }
               else {
                   vm.pIdDistrito = $rootScope.$stateParams.pIdDistrito;
                   if (vm.pIdDistrito == '' || vm.pIdDistrito == 0)
                       vm.pIdDistrito = "1";
               }
               App.stopPageLoading();

               if (vm.ultimo_corte) {
                   $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
               }
               else
                iniciaInterval();

           })
           .catch(function (err) {
               App.stopPageLoading();
           })
        }

        DetalleVotosSeccion = function (idDistrito, SeccionDistrito) {
            for (var i = 0; i < SeccionDistrito.length; i++) {
                if (idDistrito == SeccionDistrito[i].idDistrito) {
                    vm.DetalleVotos = SeccionDistrito[i].Secciones;
                    break;
                }
            }
        }

        ActasPorSeccion = function (idDistrito, ActasDistrito) {
            for (var i = 0; i < ActasDistrito.length; i++) {
                if (idDistrito == ActasDistrito[i].idDistrito) {
                    vm.ActasDistritos = ActasDistrito[i].Secciones;
                    break;
                }
            }
        }

      

        vm.number_format = function (amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        }

       
        $scope.$watch("ctrl.pIdDistrito", function (val) {
            
            if (val) {
             
                    if (!result.informacionGral)
                        return;
                    BusyLoading();
                    $timeout(function () {
                        vm.seccionSeleccion =undefined;
                        vm.visible = false;
                        vm.idSeccion = val.toString();
                        var ltSecciones = result.ltSecciones.filter(function (item) { return item.distrito == vm.pIdDistrito; });                        
                        var secciones = result.Dist_Sec_Casilla.filter(function (item) { return item.AMBITO_ID == vm.pIdDistrito && item.PrincipioEleccion_ID == 1; });
                       
                        var actas = result.Actas.filter(function (item) { return item.Municipio == vm.pIdDistrito; });
                        if (ltSecciones.length == 0 && secciones == 0 && actas == 0)
                        {
                            vm.tabla_grafica = [];
                            for (var i = 0; i < vm.Partidos.length; i++) {
                                var object = {};
                                vm.tabla_grafica.push(object);
                            }

                            App.stopPageLoading();
                        }
                        else {
                            agrupaDistritoSeccion(ltSecciones, secciones, actas);
                            App.stopPageLoading();
                        }
                    }, 50);
                    //si realiza filtro
                    
            }
        })

        agrupaDistritoSeccion = function (secciones, contexto, actasCasilla) {

            vm.ltSecciones = [];
            vm.tabla_grafica = [];
            
            angular.forEach(secciones, function (value) {
                angular.forEach(vm.Partidos, function (item) {
                    Object.defineProperty(value, item.columna, {
                        value: null,
                        writable: true,
                        enumerable: true,
                        configurable: true
                    });
                })
                Object.defineProperty(value, "TotalVotos", {
                    value: null,
                    writable: true,
                    enumerable: true,
                    configurable: true
                });
                var actas = contexto.filter(function (item) {
                    return item.SECCION == value.id;
                });                
                actas.forEach(function (v_actas) {
                   
                    angular.forEach(vm.Partidos, function (item) {
                        var valor = v_actas[item.columna];
                        //Si el valor no es nulo y es numerico 
                        if (valor != null && !isNaN(valor))
                        {
                            value[item.columna] = value[item.columna] + parseInt(valor);
                            value.TotalVotos = value.TotalVotos + parseInt(valor);
                        }
                        else {
                            //Si el valor no es nulo y es un texto
                            if (valor != null && valor.length > 0) {                                
                                if (value[item.columna] == 0 || value[item.columna]==null)
                                    value[item.columna] = 0;
                                //if (value.TotalVotos == 0 || value[item.columna] == null)
                                   // value.TotalVotos = 0;
                                if (value.TotalVotos == null)
                                    value.TotalVotos = 0;
                            }
                        }

                    })                    
                })
               })

            vm.ltSecciones = secciones;
            
            vm.TotalVotos = vm.ltSecciones.map(function (elemento) { return elemento.TotalVotos; }).reduce(function (suma, valor) { return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0) });
            vm.Porcentaje = null;
            var sump = 0;
            angular.forEach(vm.Partidos, function (item) {
                var votos = { partidos: '', totalVotos: 0, porcentaje: 0 };
                votos.partidos = item.columna;
                votos.totalVotos = vm.ltSecciones.map(function (elemento) { return elemento[item.columna] != null ? elemento[item.columna] : undefined; }).reduce(function (suma, valor) {
                    return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)
                });
                votos.porcentaje = votos.totalVotos / vm.TotalVotos;
                votos.porcentaje = (!isNaN(votos.porcentaje)) ? $rootScope.settings.TruncateDecimal((votos.porcentaje *100),4) : undefined;
                sump += (!isNaN(votos.porcentaje)) ? votos.porcentaje : 0;
                if (angular.isUndefined(votos.porcentaje))
                    votos.totalVotos = undefined;
                vm.tabla_grafica.push(votos);
            })
            if (sump == 0)
                vm.Porcentaje = null;
            else
                vm.Porcentaje = "100.0000%";

            if (vm.TotalVotos == 0)
                vm.TotalVotos = null;
            //Inicia el calculo de los porcentajes de las secciones
            vm.ActasDistritos = [];
            var temp = [];
            var resultado = {};
            resultado.id = -1;
            resultado.Nombre = "Total";
            resultado.Esperadas = 0;
            resultado.Capturadas = 0;
            resultado.Contabilizadas = 0;
            resultado.TotalListaNominal = 0;
            var sumPc = 0;
            var tvGeneral = 0;
            angular.forEach(secciones, function (value) {
                var actas = actasCasilla.filter(function (item) {
                    return item.Seccion == value.id;
                });
                var item = {};
                item.id = value.id;
                item.Nombre = value.Nombre;
                item.Esperadas = actas.length;
                resultado.Esperadas = resultado.Esperadas + item.Esperadas;
                var cpt = actas.filter(function (item) { return item.Capturadas == 1; });
                item.Capturadas = cpt.length;
                resultado.Capturadas = resultado.Capturadas + item.Capturadas;
                var ctb = actas.filter(function (item) { return item.Contabilizada == 1; });
                item.Contabilizadas = ctb.length
                resultado.Contabilizadas = resultado.Contabilizadas + item.Contabilizadas;
                item.TotalListaNominal = actas.map(function (item) { return item.ListaNominal; }).reduce(function (suma, valor) { return suma + valor });

                resultado.TotalListaNominal = resultado.TotalListaNominal + item.TotalListaNominal;
                if (value.TotalVotos > 0 && item.TotalListaNominal > 0) {
                    if (vm.ultimo_corte) {
                        item.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((value.TotalVotos / item.TotalListaNominal * 100), 4);
                    }
                    else {
                        var tc = contexto.filter(function (elemento) {
                            return elemento.SECCION == value.id && (elemento.TIPO_CASILLA_ID == 1 || elemento.TIPO_CASILLA_ID == 2 || elemento.TIPO_CASILLA_ID == 3);
                        });
                        var tvc = tc.map(function (item) { return !isNaN(item.TOTAL_VOTOS_CALCULADO) ? parseInt(item.TOTAL_VOTOS_CALCULADO) : null; }).reduce(function (suma, valor) { return suma + valor });
                       
                        if (!isNaN(tvc))
                        {
                            tvGeneral += tvc;
                            item.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((tvc / item.TotalListaNominal * 100), 4);
                            if (!angular.isUndefined(item.TotalListaNominal) && item.TotalListaNominal == 0) {
                                item.Participacion_Ciudadana = 0;
                            }
                        }
                    }
                    sumPc += item.Participacion_Ciudadana;

                }
                temp.push(item);
            })
            resultado.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((vm.TotalVotos / resultado.TotalListaNominal) * 100, 4);



            temp.push(resultado);
            var porcentaje = {};
            porcentaje.id = 0;
            porcentaje.Nombre = "Porcentaje";
            porcentaje.Esperadas = 100;
            if (resultado.Capturadas > 0 && resultado.Esperadas > 0)
                porcentaje.Capturadas =$rootScope.settings.TruncateDecimal((resultado.Capturadas / resultado.Esperadas) * 100, 4);
            else
                porcentaje.Capturadas = 0;
            if (resultado.Contabilizadas > 0 && resultado.Esperadas > 0)
                porcentaje.Contabilizadas =$rootScope.settings.TruncateDecimal((resultado.Contabilizadas / resultado.Esperadas) * 100, 4);
            else
                porcentaje.Contabilizadas = 0;
            porcentaje.Participacion_Ciudadana = 0;
            porcentaje.TotalListaNominal = 0;
            temp.push(porcentaje);
            vm.ActasDistritos = $filter('orderBy')(temp, "id");
            if (vm.ltSecciones.length > 0) {
                if(angular.isUndefined(vm.seccionSeleccion))
                    vm.seccionSeleccion = vm.ltSecciones[0].id;
                
            }
        }

        $scope.$watch("ctrl.idSeccion", function (val) {

         
        })

        vm.reloadRoute = function () {
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            BusyLoading();
            update = true;
            getDataSource();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }

        //var c = 0;
        //$scope.message = "This DIV is refreshed " + c + " time.";
        //var timer = $interval(function () {
        //    $scope.message = "This DIV is refreshed " + c + " time.";
        //    c++;
        //    if (c === 20) {
        //        c = 0;
        //        BusyLoading();
        //        getDataSource();

        //    }
        //}, 1000);

    });