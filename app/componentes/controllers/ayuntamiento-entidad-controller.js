﻿angular.module('PrepetApp', [])
    .controller('ayuntamientoEntController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService, HeaderService, $interval) {
        var vm = this;
        vm.idSelected = 0;
        var result = {};
        var miscelanea = 60000;
        var tiempo;

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_ayuntamiento_entidad'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;          
        });

        //Borra cache de la ventana
        //$templateCache.remove('/ayuntamiento_entidad');
        vm.radioSelected = 1;
        //texto para los radio botones
        vm.VotosCandidatura = "Votos por Candidatura";
        vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
        //modulo
        vm.titulo = 'Presidencias Municipales y Regidurías ';
        vm.ambitoEstadistica = '- Entidad';
        vm.descripcion = "Es posible que se presente un empate entre dos o más partidos políticos, coaliciones o candidaturas independientes y se resolvería hasta el cómputo final.\nPor presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos suman 100%.";
        vm.descripcion2 = "Por presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos, suman 100%."
        //Informacion del mapa
        vm.tituloMapa = 'MUNICIPIOS OBTENIDOS POR:';
        vm.tituloPrincipalMapa = "";        
        vm.NombreColumnaMapa1 = "Municipios obtenidos";
        
        vm.nombreColumna = 'Municipios';
        vm.objVotosDistrito=[];
        /*Carga de incio de mapa*/
        vm.distritosPartidos = [];
        vm.logosMapa = [];
        vm.totalVotos;

        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }


        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }

        vm.loadPage = function () {
            BusyLoading();

            HeaderService.
       getMunicipios()
           .then(function (data) {
               resultHeader = data;             
               vm.tabla_grafica = data.tablaVotosCandidato;
               getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })

            buscarLogosMapa = function (id){                
                for(var i=0; i< vm.logosMapa.length;i++){
                    if (vm.logosMapa[i].ganado == id) {                        
                        return (vm.logosMapa[i].logos.concat("<br/>No. de Votos obtenidos: <br/>").concat(( vm.logosMapa[i].votosTotal.toString().number_format() )));
                        break;
                    }
                }                
                return ("");
            }
            $scope.cargarDatos = function (item) {
                //vm.idSelected = id;     
                vm.distritosPartidos = [];
                if (vm.objVotosDistrito[item].Obtenidos.length > 0) {
                    var s, f, l;
                    for (var j = 0; j < vm.objVotosDistrito[item].Obtenidos.length; j++) {
                        s = vm.objVotosDistrito[item].Obtenidos[j].Nombre;
                        f = vm.objVotosDistrito[item].Color;
                        l = vm.objVotosDistrito[item].Obtenidos[j].votos;
                        var entidad = {
                            sliceValue: s,
                            attrs: {
                                fill: f
                            },
                            label: f
                        };
                        vm.distritosPartidos.push(entidad);
                    }
                }                
                cargarMapa();                
            }
            $scope.cargarMapaCompleto = function () {
                vm.distritosPartidos = [];
                vm.idSelected = 0;              
                for (var i = 0; i < vm.objVotosDistrito.length; i++) {
                    var s, f, l;
                    for (var j = 0; j < vm.objVotosDistrito[i].Obtenidos.length; j++) {
                        s = vm.objVotosDistrito[i].Obtenidos[j].Nombre;
                        f = vm.objVotosDistrito[i].Color;
                        l = vm.objVotosDistrito[i].Obtenidos[j].votos;
                        var entidad = {
                            sliceValue: s,
                            attrs: {
                                fill: f
                            },
                            label: f
                        };
                        vm.distritosPartidos.push(entidad);
                    }
                }              
                cargarMapa();              
            }
          
            /*************************************Fin dibujar mapa***********************************/

        }//fin del load page
        /************************************************/


        getDataSource = function () {
            DataService
              .getAyuntamientoEntidad()
                  .then(function (data)
                  {
                      result = data;
                      miscelanea = data.informacionGral.timer_update;
                      vm.actasCapturadas = data.informacionGral.actasCapturadas;
                      vm.actasEsperadas = data.informacionGral.actasEsperadas;
                      vm.actasRecibidas = data.informacionGral.actasRecibidas;
                      vm.contabilizadas = data.informacionGral.contabilizadas;
                      vm.actasCasillaUrbana = data.informacionGral.actasCasillaUrbana;
                      vm.actasCasillaNoUrbana = data.informacionGral.actasCasillaNoUrbana;
                      vm.actasListaNominal = data.informacionGral.actasListaNominal;
                      vm.casillaBasica = data.informacionGral.casillaBasica;
                      vm.casillaContigua = data.informacionGral.casillaContigua;
                      vm.casillaExtraordinaria = data.informacionGral.casillaExtraordinaria;
                      vm.casillaEspecial = data.informacionGral.casillaEspecial;
                      vm.TotalActasListaNominal = data.informacionGral.TotalActasListaNominal;
                      vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
                      vm.porcentajecontabilizadas = data.informacionGral.porcentajecontabilizadas;
                      vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
                      vm.barraporcentaje = $rootScope.settings.TruncateDecimal(((vm.actasListaNominal / vm.TotalActasListaNominal) * 100), 4);
                      vm.participacion = data.informacionGral.Participacion;
                      vm.horaCorte = data.informacionGral.horaCorte;
                      vm.fechaCorte = data.informacionGral.fechaCorte;
                      vm.ultimo_corte = data.informacionGral.ultimo_corte;

                      vm.tituloMapa = "Municipios de Tabasco";
                      vm.subTituloMapa = "Municipios obtenidos por:"

                      if (vm.radioSelected == 1) {
                          //vm.tabla_grafica = data.votosCandidatura.tablaVotosCandidato;
                          //var sum = 0;
                          var totalObtenidos = 0;
                          var p = 0;
                          for (var i = 0; i < vm.tabla_grafica.length; i++) {
                              if (data.votosCandidatura.tablaVotosCandidato.length == 0)
                                  break;
                              var rs = data.votosCandidatura.tablaVotosCandidato.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                              if (rs.length > 0) {
                                  vm.tabla_grafica[i].Color = rs[0].Color;
                                  vm.tabla_grafica[i].total_obtenidos = rs[0].total_obtenidos;
                                  vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                                  vm.tabla_grafica[i].Obtenidos = rs[0].Obtenidos;
                                  vm.tabla_grafica[i].max = rs[0].max;
                                  vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;
                                  // sum += vm.tabla_grafica[i].totalVotos;
                                  p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;
                                  totalObtenidos += !isNaN(vm.tabla_grafica[i].total_obtenidos)? vm.tabla_grafica[i].total_obtenidos:0;

                              }
                          }
                          vm.TotalVotos = vm.totalVotos = vm.TotalVotos = result.votosCandidatura.totalVotos;// vm.Suma(data.votosCandidatura.tablaVotosCandidato, 'totalVotos');
                          if (vm.TotalVotos == 0)
                              vm.TotalVotos = vm.totalVotos = null;
                          vm.totalMunicipio = totalObtenidos == 0 ? null : totalObtenidos;// vm.Suma(vm.tabla_grafica, 'total_obtenidos');
                          
                          
                          vm.PorcentajeTotal = p==0?null:"100.0000%";
                          vm.objVotosDistrito = vm.tabla_grafica.filter(function (item) { return item.total_obtenidos > 0; });
                          vm.copiaObjetoEnpate = vm.tabla_grafica.filter(function (item) { return item.total_obtenidos > 0; });

                          BuscarMapaEnpate();


                          
                          
                          
                          
                          $timeout(function () {
                              var $jq = jQuery.noConflict();
                              var chart = $("#graficaEstadistica").highcharts();
                              chart.series[1].data[0].update(parseFloat(vm.porcentajecapturadas));
                              chart.series[2].data[0].update(parseFloat(vm.porcentajecontabilizadas));
                          }, 300);

                          agregar();
                          var $jq = jQuery.noConflict();
                          cargarMapa = function () {        0
                              
                              $jq(function () {
                                  $jq(".mapcontainer").mapael({
                                      map: {
                                          name: "municipio_tabasco",
                                          zoom: {
                                              enabled: true,
                                              maxLevel: 10,
                                              mousewheel: false,
                                              init: {
                                                  latitude: 40.717079,
                                                  longitude: -74.00116,
                                                  level: 0
                                              },
                                              attrs: {
                                                  stroke: "#fff",
                                                  "stroke-width": 1
                                              },
                                              attrsHover: {
                                                  "stroke-width": 2
                                              }

                                          }
                                      },
                                      legend: {
                                          area: {
                                              title: "MUNICIPIOS DE TABASCO",
                                              slices: Object.assign(vm.distritosPartidos)
                                          }
                                      },
                                      areas: {
                                          "JNT": {
                                              value: "Jonuta",
                                              tooltip: {
                                                  content: "<span style=\"font-weight:bold;\">Jonuta</span><br />".concat(buscarLogosMapa("Jonuta"))
                                              }
                                          },
                                          "TNQ": {
                                              value: "Tenosique",
                                              tooltip: {
                                                  content: "<span style=\"font-weight:bold;\">Tenosique</span><br/>".concat(buscarLogosMapa("Tenosique"))
                                              }
                                          },
                                          "BLC": {
                                              value: "Balancán",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Balancán</span><br />".concat(buscarLogosMapa("Balancán")) }
                                          },
                                          "EMZ": {
                                              value: "Emiliano Zapata",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Emiliano Zapata</span><br />".concat(buscarLogosMapa("Emiliano Zapata")) }
                                          },
                                          "MCN": {
                                              value: "Macuspana",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Macuspana</span><br />".concat(buscarLogosMapa("Macuspana")) }
                                          },
                                          "CTL": {
                                              value: "Centla",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Centla</span><br />".concat(buscarLogosMapa("Centla")) }
                                          },
                                          "NCJ": {
                                              value: "Nacajuca",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Nacajuca</span><br />".concat(buscarLogosMapa("Nacajuca")) }
                                          },
                                          "JPM": {
                                              value: "Jalpa de Méndez",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Jalpa de Méndez</span><br />".concat(buscarLogosMapa("Jalpa de Méndez")) }
                                          },
                                          "CDC": {
                                              value: "Cunduacán",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Cunduacán</span><br />".concat(buscarLogosMapa("Cunduacán")) }
                                          },
                                          "PRS": {
                                              value: "Paraíso",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Paraíso</span> <br />".concat(buscarLogosMapa("Paraíso")) }
                                          },
                                          "CMC": {
                                              value: "Comalcalco",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Comalcalco</span><br />".concat(buscarLogosMapa("Comalcalco")) }
                                          },
                                          "TCP": {
                                              value: "Tacotalpa",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Tacotalpa</span><br />".concat(buscarLogosMapa("Tacotalpa")) }
                                          },
                                          "TEP": {
                                              value: "Teapa",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Teapa</span><br />".concat(buscarLogosMapa("Teapa")) }
                                          },
                                          "JLP": {
                                              value: "Jalapa",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Jalapa</span><br />".concat(buscarLogosMapa("Jalapa")) }
                                          },
                                          "HMG": {
                                              value: "Huimanguillo",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Huimanguillo</span><br />".concat(buscarLogosMapa("Huimanguillo")) }
                                          },
                                          "CNT": {
                                              value: "Centro",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Centro</span><br />".concat(buscarLogosMapa("Centro")) }
                                          },
                                          "CDN": {
                                              value: "Cárdenas",
                                              tooltip: { content: "<span style=\"font-weight:bold;\">Cárdenas</span><br />".concat(buscarLogosMapa("Cárdenas")) }
                                          }
                                      }
                                  });
                              });
                          }
                          cargarMapa();
                      }
                      else {
                          getVotosPorPartidos(data.votosCandidaturaIndependiente.tablaVotosCandidato);
                         
                         /* vm.tabla_grafica = result.votosCandidaturaIndependiente.tablaVotosCandidato;
                          vm.TotalVotos = result.votosCandidaturaIndependiente.totalVotos;
                          if (vm.tabla_grafica.length > 0) {
                              vm.calcularMaximos();
                          } else {
                              vm.calcularMaximos();
                          }
                          */
                      }

                      App.stopPageLoading();
                      if (vm.ultimo_corte) {
                          $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
                      }
                      else
                      iniciaInterval();
                  })
             .catch(function (err) {
                 App.stopPageLoading();
                 iniciaInterval();
             })
            //Fin del  servicio      

        }
        
        getVotosPorPartidos = function (data) {
            var p = 0;
            for (var i = 0; i < vm.tabla_grafica.length; i++) {
                if (data.length == 0)
                    break;
                var rs = data.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                if (rs.length > 0) {
                   // vm.tabla_grafica[i].Color = rs[0].Color;                    
                    vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                    vm.tabla_grafica[i].max = rs[0].max;
                    vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;
                    //sum += !isNaN(vm.tabla_grafica[i].totalVotos)?vm.tabla_grafica[i].totalVotos:0;
                    //totalObtenidos +=!isNaN(vm.tabla_grafica[i].total_obtenidos)? vm.tabla_grafica[i].total_obtenidos:0;
                    p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;
                }
            }
            vm.TotalVotos = result.votosCandidaturaIndependiente.totalVotos;
            if (vm.TotalVotos == 0)
                vm.TotalVotos = null;

            vm.PorcentajeTotal = p == 0 ? null : "100.0000%";
            
            if (data.length > 0)
            {
                vm.calcularMaximos();
            }/* else {
                vm.calcularMaximos();
            }*/
        }

        BuscarMapaEnpate = function () {
            vm.distritosPartidos = [];
            for (var i = 0; i < vm.objVotosDistrito.length; i++) {                
                for (var t = 0; t < vm.objVotosDistrito[i].Obtenidos.length; t++) {                    
                    for (var j = 0; j < vm.copiaObjetoEnpate.length; j++) {                        
                        if (vm.copiaObjetoEnpate[j].logoPartido != vm.objVotosDistrito[i].logoPartido) {  /*Se usa el logo como id de la coalicion para evitar comparacion del mismo objeto*/
                            if (vm.copiaObjetoEnpate[j].total_obtenidos > 0)
                                for (var k = 0; k < vm.copiaObjetoEnpate[j].Obtenidos.length; k++) {
                                    if ((vm.objVotosDistrito[i].Obtenidos[t].id == vm.copiaObjetoEnpate[j].Obtenidos[k].id) && (vm.objVotosDistrito[j].Obtenidos[k].Nombre !="")) {
                                            //console.log(i + "-" + vm.objVotosDistrito[i].Obtenidos[t].Nombre + "++" + vm.copiaObjetoEnpate[j].Obtenidos[k].Nombre);
                                            vm.objVotosDistrito[j].Obtenidos[k].Color = "";
                                            vm.objVotosDistrito[j].Obtenidos[k].Nombre = "";
                                            vm.objVotosDistrito[j].total_obtenidos = vm.objVotosDistrito[j].total_obtenidos - 1;
                                            vm.totalMunicipio = vm.totalMunicipio - 1;
                                    }
                                }
                        }
                    }                   
               }
            }       
            agregar();
        }
        /************************************************/
        agregar = function () {
            vm.distritosPartidos = [];            
            for (var i = 0; i < vm.objVotosDistrito.length; i++) {
                var s, f, l;
                var imagen = "";
                var logosMapaGanado;
                for (var j = 0; j < vm.objVotosDistrito[i].Obtenidos.length; j++) {
               
                        s = vm.objVotosDistrito[i].Obtenidos[j].Nombre;
                        f = vm.objVotosDistrito[i].Color;
                        l = vm.objVotosDistrito[i].Obtenidos[j].votos;                        
                        var entidad = {
                            sliceValue: s,
                            attrs: {
                                fill: f
                            },
                            label: f
                        };
                        if (typeof (vm.objVotosDistrito[i].logoPartido) != "") {
                            imagen = imagen.concat("<img src='assets/img/partidos/240/" + vm.objVotosDistrito[i].logoPartido + "'  height='30' />");
                        } else
                            imagen = imagen.concat(vm.objVotosDistrito[i].nombreCandidatonombreColumna);

                        logosMapaGanados = {
                            ganado: s, logos: imagen, votosTotal: l
                        };
                        imagen = "";

                        vm.logosMapa.push(logosMapaGanados);          
                        vm.distritosPartidos.push(entidad);                                         
                }
            };
        }

        $scope.$watch("ctrl.radioSelected", function (val) {
            if (val) {
                if (val == 1) {
                    if (result.votosCandidatura) {
                        vm.tabla_grafica = [];
                        vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                        //vm.tabla_grafica = result.votosCandidatura.tablaVotosCandidato;
                        vm.TotalVotos = result.votosCandidatura.totalVotos;
                        var p = 0;
                        for (var i = 0; i < vm.tabla_grafica.length; i++) {
                            if (result.votosCandidatura.tablaVotosCandidato.length == 0)
                                break;
                            var rs = result.votosCandidatura.tablaVotosCandidato.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                            if (rs.length > 0) {
                                vm.tabla_grafica[i].Color = rs[0].Color;
                                vm.tabla_grafica[i].total_obtenidos = rs[0].total_obtenidos;
                                vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                                vm.tabla_grafica[i].Obtenidos = rs[0].Obtenidos;
                                vm.tabla_grafica[i].max = rs[0].max;
                                vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;
                                p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;
                                vm.totalObtenidos += vm.tabla_grafica[i].total_obtenidos;

                            }
                        }
                        vm.PorcentajeTotal = p == 0 ? null : $filter("number")(p, 2) + "%";
                        if (vm.TotalVotos == 0)
                            vm.TotalVotos = null;
                       cargarMapa();
                    }
                } else {
                    vm.tabla_grafica = [];
                    vm.tabla_grafica = resultHeader.Partidos;
                    
                    getVotosPorPartidos(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                  /*  vm.tabla_grafica = result.votosCandidaturaIndependiente.tablaVotosCandidato;
                    vm.TotalVotos = result.votosCandidaturaIndependiente.totalVotos;
                    if (vm.tabla_grafica.length > 0) {
                        vm.calcularMaximos();
                    } else {
                        vm.calcularMaximos();
                    }*/
                }
            }            
        });

        

        vm.calcularMaximos = function () {
            vm.Chart = {};
            vm.Chart.p1 = 0;
            vm.Chart.p2;
            vm.Chart.p3;
            vm.Chart.p4;
            vm.Chart.p5;
            vm.maxValueChart = 0;

            var max = Math.max.apply(Math, vm.tabla_grafica.map(function (item) {
                
                return angular.isUndefined(item.totalVotos) ? 0 : item.totalVotos;
            }));
            vm.valorMaximo = max;
            if (max > 0) {
                vm.Chart.p2 = vm.porcentajeGrafica(max, 25);
                vm.Chart.p3 = vm.porcentajeGrafica(max, 50);
                vm.Chart.p4 = vm.porcentajeGrafica(max, 75);
                vm.Chart.p5 = vm.porcentajeGrafica(max, 100);
            } else {
                vm.Chart.p2 = 25;
                vm.Chart.p3 = 50;
                vm.Chart.p4 = 75;
                vm.Chart.p5 = 100;
            }
            
        };
       

        //Evento de la tala grafica
        vm.Suma = function (data, key) {
            if (angular.isUndefined(data) || angular.isUndefined(key))
                return 0;
            var sum = 0;
            angular.forEach(data, function (value) {
                sum = sum + parseInt(value[key], 10);
            });
            return sum;
        }

        vm.alturaBar = function (valormaximo, total) {
            if (angular.isUndefined(valormaximo))
                return 200;
            if (angular.isUndefined(total))
                return 200;
            valormaximo = valormaximo.toString();
            total = total.toString();
            var totalInteger = parseInt(total.replace(',', ''));

            var tot = parseInt(valormaximo.replace(',', ''));
            if (tot <= 0) {
                return 200;
            } else {

                return 200 - ((totalInteger * 200) / vm._porcentajeGrafica(valormaximo, 100));
            }

        }

        vm.porcentajeGrafica = function (total, por) {
            var result = vm._porcentajeGrafica(total, por);
            return vm.number_format(result, 0);
        };


        vm._porcentajeGrafica = function (total, por) {

            var tot = parseInt(total.toString().replace(/,/g, ""));
            var next = parseInt(tot.toString()[0]) + 1;

            var nextInt = next * Math.pow(10, tot.toString().length - 1);

            if ((nextInt - tot) <= nextInt * 0.1) {
                nextInt = nextInt + (nextInt * 0.1);
            }

            return nextInt * (por / 100);
        }
        
        vm.number_format = function (amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        }
        /**************************************************************************/
       

        vm.reloadRoute = function () {
            // $state.reload();
            BusyLoading();
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            getDataSource();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }

        String.prototype.number_format = function (d) {
            var n = this;
            var c = isNaN(d = Math.abs(d)) ? 0 : d; //isNaN(d = Math.abs(d)) ? 2 : d; para los numero decimales
            var s = n < 0 ? "-" : "";
            var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + ',' : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + ',') + (c ? '.' + Math.abs(n - i).toFixed(c).slice(2) : "");
        }
        
    });