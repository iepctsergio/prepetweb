﻿angular.module('PrepetApp', [])
    .controller('bdController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService) {
        var vm = this;
        vm.ventana = 10;
        var url = "";
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_ayuda'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;
        });
        vm.loadPage = function () {
            DataService.
           getBaseDatos()
                 .then(function (data) {
                     vm.horaCorte = data.horaCorte;
                     vm.fechaCorte = data.fechaCorte;
                     url = data.basedatos;
                 })
                 .catch(function (err) {
                     App.stopPageLoading();
                 })
        }
        vm.exportarDatos = function () {
            window.location.href = 'http://localhost:35200/api/consultar?archivo=' + url;
        }
    });