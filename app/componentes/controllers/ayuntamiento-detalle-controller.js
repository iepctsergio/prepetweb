﻿angular.module('PrepetApp', [])
    .controller('ayuntamientoDetalleController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService, HeaderService, $interval) {
        var vm = this;
        var result = {};
        var encabezado = {};
        $scope.MunicipioSeleccionado = {};
        vm.selccionadoMucipio = $rootScope.$stateParams.pIDMunicipio;

        var resultHeader = {};
        var miscelanea = 60000;
        var tiempo;

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_ayuntamiento_detalle'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;

        });


        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }

        vm.loadPage = function () {
          
            vm.radioSelected = 1;
            //texto para los radio botones
            vm.VotosCandidatura = "Votos por Candidatura";
            vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
            vm.mostrarNombreCandidato = true;

            vm.tipoSectorEleccion = 'el Ayuntamiento'; //para saber si entidad o municpio
            vm.tituloTabla = 'Distribución de votos por candidatura a nivel Municipio';

            //modulo  Presidencias Municipales y Regidur&iacute;as
            vm.titulo = 'Detalle por Presidencias Municipales y Regidurías';
            vm.ambitoEstadistica = null;
            vm.descripcion = null;
            /*Participacion ciudadana*/           
            vm.nombreTabla = 'Estadística de Ayuntamiento';

            vm.titulo_actas = "Actas del Municipio";
            vm.titulo_tabla = "Actas por Ayuntamiento";

            vm.Municipios = [];
            vm.MunicipiosPartidos = [];
            vm.tabla_grafica = [];
            vm.objVotosCandidatura = [];
            vm.objVotosPartido = [];
            vm.ActasDistritos = [];
            vm.votacionMaxima = -1;
            
            BusyLoading();
           
            HeaderService.
       getMunicipios()
           .then(function (data)
           {
               BusyLoading();
               encabezado = data;
               vm.Municipios = angular.copy(data.Municipios);
               vm.MunicipiosPartidos = angular.copy(data.Municipios);
               
              getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })
            
          

                           
        }//fin del load page

        getDataSource = function ()
        {
            DataService
       .getAyuntamientoDetalleMunicipio()
       .then(function (data) {
           result = data;
           miscelanea = data.informacionGral.timer_update;
           vm.actasCapturadas = data.informacionGral.actasCapturadas;
           vm.actasEsperadas = data.informacionGral.actasEsperadas;
           vm.actasRecibidas = data.informacionGral.actasRecibidas;
           vm.contabilizadas = data.informacionGral.contabilizadas;
           vm.actasCasillaUrbana = data.informacionGral.actasCasillaUrbana;
           vm.actasCasillaNoUrbana = data.informacionGral.actasCasillaNoUrbana;
           vm.actasListaNominal = data.informacionGral.actasListaNominal;
           vm.casillaBasica = data.informacionGral.casillaBasica;
           vm.casillaContigua = data.informacionGral.casillaContigua;
           vm.casillaExtraordinaria = data.informacionGral.casillaExtraordinaria;
           vm.casillaEspecial = data.informacionGral.casillaEspecial;
           vm.TotalActasListaNominal = data.informacionGral.TotalActasListaNominal;
           vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
           vm.porcentajecontabilizadas = data.informacionGral.porcentajecontabilizadas;
           vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
           vm.barraporcentaje = $rootScope.settings.TruncateDecimal(((vm.actasListaNominal / vm.TotalActasListaNominal) * 100), 4);
           vm.participacion = data.informacionGral.participacion;
           vm.horaCorte = data.informacionGral.horaCorte;
           vm.fechaCorte = data.informacionGral.fechaCorte;
           vm.ultimo_corte = data.informacionGral.ultimo_corte;
           

           //Antes
           /*vm.Municipios = data.votosCandidatura.listaDetalleMunicipio;
           
           vm.MunicipiosPartidos = data.votosCandidaturaIndependiente.listaDetalleMunicipio;

           vm.DistritosPartidos = data.votosCandidaturaIndependiente.listaDetalleDistrito;*/

           for (var i = 0; i < vm.Municipios.length; i++)
           {
               var muni = data.votosCandidaturaIndependiente.listaDetalleMunicipio.filter(function (item) { return item.id == vm.Municipios[i].id; });
               vm.Municipios[i].tablaActasDistrito = [];
               vm.Municipios[i].tablaVotosPartido = [];
               if (muni.length > 0)
               {
                   if (muni[0].tablaActasDistrito.length > 0)
                       vm.Municipios[i].tablaActasDistrito = muni[0].tablaActasDistrito;
                   if (muni[0].tablaVotosPartido.length > 0) {
                       vm.Municipios[i].tablaVotosPartido = muni[0].tablaVotosPartido;
                   }
               }
           }
           
           for (var i = 0; i < vm.MunicipiosPartidos.length; i++) {
               var muni = data.votosCandidatura.listaDetalleMunicipio.filter(function (item) { return item.id == vm.MunicipiosPartidos[i].id; });
               vm.MunicipiosPartidos[i].tablaActasDistrito = [];
               vm.MunicipiosPartidos[i].tablaVotosPartido = [];
               if (muni.length > 0) {
                
                   if (muni[0].tablaActasDistrito.length > 0)
                       vm.MunicipiosPartidos[i].tablaActasDistrito = muni[0].tablaActasDistrito;
                   if (muni[0].tablaVotosPartido.length > 0) {
                       vm.MunicipiosPartidos[i].tablaVotosPartido = muni[0].tablaVotosPartido;
                   }
               }
           }

           var parametro = angular.isUndefined(vm.MunicipioSeleccionado) ? $rootScope.$stateParams.pIDMunicipio : vm.MunicipioSeleccionado.id;
           
           if (vm.radioSelected == 1) {
             
               var seleccionado = vm.MunicipiosPartidos.filter(function (item) { return item.id == parametro; });
               if (seleccionado.length > 0) {
                   vm.MunicipioSeleccionado = null;
                   vm.MunicipioSeleccionado = seleccionado[0];
               }
               else {
                   if (angular.isUndefined(vm.MunicipioSeleccionado))
                       vm.MunicipioSeleccionado = vm.MunicipiosPartidos[0];
                   else {
                       var seleccionado = vm.MunicipiosPartidos.filter(function (item) { return item.id == vm.MunicipioSeleccionado.id; });
                       if (seleccionado.length > 0) {
                           generarDatosCandidatura(seleccionado[0]);
                       }
                   }
               }

           }
           else {
              
               var seleccionado = vm.Municipios.filter(function (item) { return item.id == parametro; });
               if (seleccionado.length > 0) {
                   vm.MunicipioSeleccionado = null;
                   vm.MunicipioSeleccionado = seleccionado[0];
               }
               else {
                   if (angular.isUndefined(vm.MunicipioSeleccionado))
                       vm.MunicipioSeleccionado = vm.Municipios[0];
                   else {
                       var seleccionado = vm.Municipios.filter(function (item) { return item.id == vm.MunicipioSeleccionado.id; });
                       if (seleccionado.length > 0) {
                           generarDatoPorPartidos(seleccionado[0]);
                       }
                   }
               }
           }
           App.stopPageLoading();
           if (vm.ultimo_corte)
           {
               $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
           }
           else
             iniciaInterval();
       })
       .catch(function (err) {
           App.stopPageLoading();
           iniciaInterval();
       })
        }

        $scope.$watch("ctrl.MunicipioSeleccionado", function (val) {
           if (angular.isUndefined(val) || angular.isUndefined(val.id))
                return;
           if (val)
           {               
                if (vm.radioSelected == 1)
                {
                    generarDatosCandidatura(val);
                    
                }
                else {
                    generarDatoPorPartidos(val);
                }
            }
        })
        generarDatosCandidatura = function (val) {
            
            vm.tabla_grafica = [];
            vm.objVotosCandidatura = [];
            vm.objVotosCandidatura = angular.copy(val.tablaVotosPartido);
            if (vm.objVotosCandidatura.length > 0) {
                
                var max = Math.max(... vm.objVotosCandidatura.map(function(item) {return item.total}));
                //Para los repetidos
                var mxmo = vm.objVotosCandidatura.filter(function (item) { return item.total == max; });
                var tamano = mxmo.length;

                vm.votacionMaxima = max;
                var sum = 0;

                for (var i = 0; i < vm.objVotosCandidatura.length; i++) {
                    var dato = {};
                    dato.nombreCandidato = vm.objVotosCandidatura[i].candidato;
                    dato.logoPartido = vm.objVotosCandidatura[i].logoPartido;
                    dato.porcentaje = vm.objVotosCandidatura[i].porcentaje;
                    dato.totalVotos = vm.objVotosCandidatura[i].total;
                    sum = sum + dato.totalVotos;
                    if (tamano == 1) {
                        dato.max = dato.totalVotos == max ? true : false;
                    }
                    else {
                        dato.max = false;
                    }
                    vm.tabla_grafica.push(dato);
                }

                vm.TotalVotos = vm.totalVotos = sum;
                vm.Porcentaje ="100.0000%";
            }
            else
            {
                vm.tabla_grafica = [];
                vm.objVotosCandidatura = [];
                vm.TotalVotos = null;
                vm.Porcentaje = "";
                var header = encabezado.MunicipiosPartidos.filter(function (item) { return item.id == val.id; });
               if (header.length > 0)
               {
                   if (header[0].Candidatura.length > 0) {
                       vm.tabla_grafica = header[0].Candidatura;
                       for (var i = 0; i < header[0].Candidatura.length; i++)
                       {
                           if (header[0].Candidatura[i].Clave !== 'VN' && header[0].Candidatura[i].Clave !== 'CNR') {
                               var obj = {}
                               obj.logoPartido = header[0].Candidatura[i].logoPartido;
                               obj.candidato = angular.isUndefined(header[0].Candidatura[i].NombreCandidato) ? header[0].Candidatura[i].nombreCandidato : header[0].Candidatura[i].NombreCandidato;
                               if (!angular.isUndefined(header[0].Candidatura[i]))
                                   obj.partido = header[0].Candidatura[i].partido;
                               else
                                   obj.partido = [];
                               vm.objVotosCandidatura.push(obj);
                           }
                       }
                   }
               }

            }
            vm.ActasDistritos = angular.copy(val.tablaActasDistrito);
        }
        generarDatoPorPartidos = function (val)
        {
            vm.tabla_grafica = [];
            vm.objVotosPartido = [];
            vm.objVotosPartido = angular.copy(val.tablaVotosPartido);
            if (vm.objVotosPartido.length > 0) {
                var total = 0;
                vm.objVotosPartido.forEach(function (value, key) {
                    total = total + value.Total;
                })
                var max = Math.max(... vm.objVotosPartido.map(function(item) {return item.Total}));
                var mxmo = vm.objVotosCandidatura.filter(function (item) { return item.total == max; });
                var tamano = mxmo.length;

                for (var i = 0; i < vm.objVotosPartido.length; i++) {
                    var dato = {};
                    dato.nombreCandidato = vm.objVotosPartido[i].Logo == '' ? vm.objVotosPartido[i].Nombre : '';
                    dato.logoPartido = vm.objVotosPartido[i].Logo;
                    dato.totalVotos = vm.objVotosPartido[i].Partido + vm.objVotosPartido[i].Coalicion;
                    dato.porcentaje = $rootScope.settings.TruncateDecimal(((dato.totalVotos / total) * 100),4);

                    if (tamano == 1) {
                        dato.max = dato.totalVotos == max ? true : false;
                    }
                    else {
                        dato.max = false;
                    }
                    vm.tabla_grafica.push(dato);
                }
                vm.TotalVotos = vm.totalVotos = total;
                vm.Porcentaje = "100.0000%";
            }
            else {
                vm.tabla_grafica = [];
                vm.objVotosPartido = [];
                vm.TotalVotos = null;
                vm.Porcentaje = "";
                var header = encabezado.MunicipiosPartidos.filter(function (item) { return item.id == val.id; });

                if (header.length > 0) {
                    if (header[0].Partidos.length > 0) {
                        vm.tabla_grafica = header[0].Partidos;
                        for (var i = 0; i < header[0].Partidos.length; i++) {
                            if (header[0].Partidos[i].Clave !== 'VN' && header[0].Partidos[i].Clave !== 'CNR') {
                                var obj = {}
                                obj.Logo = header[0].Partidos[i].logoPartido;
                                obj.nombreCandidato = header[0].Partidos[i].NombreCandidato;
                                if (!angular.isUndefined(header[0].Candidatura[i]))
                                    obj.partido = header[0].Candidatura[i].partido;
                                else
                                    obj.partido = [];
                                vm.objVotosPartido.push(obj);
                            }
                        }
                    }
                }
            }
            vm.ActasDistritos = angular.copy(val.tablaActasDistrito);
        }

        $scope.$watch("ctrl.radioSelected", function (val) {
            if (!angular.isUndefined(val)) {
                if (val == 1)
                {
                    var seleccionado = vm.MunicipiosPartidos.filter(function (item) { return item.id == vm.MunicipioSeleccionado.id; });
                    
                    if (seleccionado.length > 0) {
                        generarDatosCandidatura(seleccionado[0]);
                    }
                }
                else
                {
                    var seleccionado = vm.Municipios.filter(function (item) { return item.id == vm.MunicipioSeleccionado.id; });
                    if (seleccionado.length > 0)
                    {
                        generarDatoPorPartidos(seleccionado[0]);
                    }
                }
            }
        })

        vm.reloadRoute = function () {
            BusyLoading();
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            getDataSource();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }
        
        vm.click_seccion_casilla = function () {
            $rootScope.Nombre = vm.MunicipioSeleccionado.Nombre;
            $rootScope.pIDMunicipio = vm.MunicipioSeleccionado.id;
            $state.go("seccion_casilla", { pIdSeccion: 0, modulo: '3S', pIdMunicipio: vm.MunicipioSeleccionado.id, pIdDistrito: vm.MunicipioSeleccionado.id });
            
        }
    });