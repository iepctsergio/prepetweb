﻿angular.module('PrepetApp', [])
    .controller('dipDetalleEntidadController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService, DataService, HeaderService, $interval, NgTableParams) {
        var vm = this;
        vm.modulo = "2DE"
        var result;

        var resultHeader = {};
        var miscelanea = 60000;
        var tiempo;
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_diputacion_detalle'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;
            //loadPage();
        });
        //****variable del encabzado**************    
        vm.radioSelected = 1;
        vm.radioSelectedCasilla = true;
        vm.detalleCasilla = "Detalle Casilla";
        vm.VotosCandidatura = "Distritos";
        vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
       
        //****variable del encabzado**************
        vm.titulo = 'Diputaciones ';
        vm.ambitoEstadistica = '- Detalle de la Entidad';
        vm.descripcion = "El total de votos calculado y porcentaje que se muestran, se refieren a los votos asentados en las Actas PREP hasta el momento. Por presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%.";
        vm.titulo_detalle_votos = "Detalle de votos por Distritos";
        vm.detallevotos_primera_columna = "Distritos";
        vm.titulo_actas = "Actas por distritos";
        
        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }


        vm.loadPage = function () {

            BusyLoading();

            HeaderService.
       getDiputacion()
           .then(function (data) {
               resultHeader = data;
               vm.objVotosDistrito = vm.tabla_grafica = data.tablaVotosCandidato;
               vm.DetalleVotos = data.Distritos;
               DetallePorDistrito(data.tablaVotosCandidato);
               DetallePorActas(data.Distritos);
               getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })           
        }//Fin de load page
        /****************************/
       
        getDataSource = function () {
            DataService
           .getDiputacioneDetalleEntidad()
           .then(function (data) {
               result = data;               
             
               if (vm.radioSelected == 1) {
                   aplicaInformacionGeneral(data.informacionGral.informacionGralDistrito);
                   LlenaTablaVotacion(data.votosCandidatura.tablaVotosCandidato);
                   LlenaDetalleMunicipios(data.votosCandidatura.tablaDistrito);
                   vm.votacionMaxima = -1;
                   $scope.votacionAlta();

                   if (result.votosCandidatura.tablaActasDistrito.length > 0) {

                       for (var i = 0; i < vm.ActasDistritos.length; i++) {

                           var rs = result.votosCandidatura.tablaActasDistrito.filter(function (item) {
                               return item.id == vm.ActasDistritos[i].id;
                           });

                           if (rs.length > 0) {
                               vm.ActasDistritos[i].Esperadas = rs[0].Esperadas;
                               vm.ActasDistritos[i].Capturadas = rs[0].Capturadas;
                               vm.ActasDistritos[i].Contabilizadas = rs[0].Contabilizadas;
                               vm.ActasDistritos[i].Participacion_Ciudadana = rs[0].Participacion_Ciudadana;
                               vm.ActasDistritos[i].TotalListaNominal = rs[0].TotalListaNominal;
                           }
                       }

                   }

               }
               else {
                   aplicaInformacionGeneral(data.informacionGral.informacionGralPartidos);
                   LlenaTablaVotacion(data.votosCandidaturaIndependiente.tablaVotosCandidato);
                   LlenaDetalleMunicipios(data.votosCandidaturaIndependiente.tablaDistrito)

                   if (result.votosCandidaturaIndependiente.tablaActasPartido.length > 0) {

                       for (var i = 0; i < vm.ActasDistritos.length; i++) {

                           var rs = result.votosCandidaturaIndependiente.tablaActasPartido.filter(function (item) {
                               return item.id == vm.ActasDistritos[i].id;
                           });

                           if (rs.length > 0) {
                               vm.ActasDistritos[i].Esperadas = rs[0].Esperadas;
                               vm.ActasDistritos[i].Capturadas = rs[0].Capturadas;
                               vm.ActasDistritos[i].Contabilizadas = rs[0].Contabilizadas;
                               vm.ActasDistritos[i].Participacion_Ciudadana = rs[0].Participacion_Ciudadana;
                               vm.ActasDistritos[i].TotalListaNominal = rs[0].TotalListaNominal;
                           }
                       }

                   }
               }

              
               App.stopPageLoading();
               if (vm.ultimo_corte) {
                   $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
               }
               else
                 iniciaInterval();
           })
           .catch(function (err) {
               App.stopPageLoading();
               iniciaInterval();
           })
        }//Fin del data source

        aplicaInformacionGeneral = function (data) {

            miscelanea = data.timer_update;
            vm.actasCapturadas = data.actasCapturadas;
            vm.actasEsperadas = data.actasEsperadas;
            vm.actasRecibidas = data.actasRecibidas;
            vm.contabilizadas = data.contabilizadas;
            vm.actasCasillaUrbana = data.actasCasillaUrbana;
            vm.actasCasillaNoUrbana = data.actasCasillaNoUrbana;
            vm.actasListaNominal = data.actasListaNominal;

            vm.TotalActasListaNominal = data.TotalActasListaNominal;
            vm.porcentajecapturadas = data.porcentajecapturadas;
            vm.porcentajecontabilizadas = data.porcentajecontabilizadas;

            vm.barraporcentaje = $rootScope.settings.TruncateDecimal(((vm.actasListaNominal / vm.TotalActasListaNominal) * 100), 4);
            vm.participacion = data.Participacion;
            vm.horaCorte = data.horaCorte;
            vm.fechaCorte = data.fechaCorte;
            vm.ultimo_corte = data.ultimo_corte;

            vm.porcentajeParticipacionCiudadana = data.porcentajeParticipacionCiudadana;
            vm.casillaBasica = data.casillaBasica;
            vm.casillaContigua = data.casillaContigua;
            vm.casillaExtraordinaria = data.casillaExtraordinaria;
            vm.casillaEspecial = data.casillaEspecial;
        }


        $scope.votacionAlta = function () {
            for (var i = 0; i < vm.objVotosDistrito.length; i++) {
                if (parseFloat(vm.votacionMaxima) < parseFloat(vm.objVotosDistrito[i].total)) {
                    vm.votacionMaxima = vm.objVotosDistrito[i].total;
                }
            }
        }

        DetallePorActas = function (data) {
            
            vm.ActasDistritos = [];
            var t = {};
            t.id = -1;
            t.Nombre = "Total";
            t.Participacion_Ciudadana = 0;
            vm.ActasDistritos.push(t);
            var t2 = {};
            t2.id = 0;
            t2.Nombre = "Porcentaje";
            t2.Participacion_Ciudadana = 0;
            vm.ActasDistritos.push(t2);
            for (var i = 0; i < data.length; i++) {
                var item = {};
                item.id = data[i].id;
                item.Nombre = data[i].Nombre;
                item.Participacion_Ciudadana = 0;
                vm.ActasDistritos.push(item);
            }
        }

        DetallePorDistrito = function (data) {
            if (angular.isUndefined(vm.DetalleVotos))
                return;
            for (var i = 0; i < vm.DetalleVotos.length; i++) {
                vm.DetalleVotos[i].Partidos = [];
                for (var j = 0; j < data.length; j++) {
                    var item = {};
                    item.Clave = data[j].Clave;
                    vm.DetalleVotos[i].Partidos.push(item);
                }
            }
        }

        LlenaTablaDetalleXDistrito = function (data) {
            for (var i = 0; i < vm.DetalleVotos.length; i++) {

                var rs = data.filter(function (item) {
                    return item.id == vm.DetalleVotos[i].id;
                });
                if (rs.length > 0) {
                    vm.DetalleVotos[i].Partidos = [];
                    vm.DetalleVotos[i].Partidos = rs[0].Partidos;
                    vm.DetalleVotos[i].TotalVotos = rs[0].TotalVotos;
                }
            }
        }

        LlenaDetalleMunicipios = function (data) {
            for (var i = 0; i < vm.DetalleVotos.length; i++) {

                var rs = data.filter(function (item) {
                    return item.id == vm.DetalleVotos[i].id;
                });

                if (rs.length > 0) {
                    if (rs[0].Partidos.length > 0) {
                        angular.forEach(vm.DetalleVotos[i].Partidos, function (value) {
                            var rsp = rs[0].Partidos.filter(function (item) { return item.Otros === value.Clave });
                            if (rsp.length > 0)
                            {
                                if (rsp[0].votos != null) {
                                    value.votos = rsp[0].votos;
                                }
                                else {
                                    value.votos = "P";
                                }
                            }
                        })

                        vm.DetalleVotos[i].TotalVotos = rs[0].TotalVotos;
                    }

                }
            }

        }

        LlenaTablaVotacion = function (data) {
            var sum = 0; var p = 0;
            var totalObtenidos = 0;

            for (var i = 0; i < vm.tabla_grafica.length; i++) {
                if (data.length == 0)
                    break;
                var rs = data.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                if (rs.length > 0) {
                    vm.tabla_grafica[i].Color = rs[0].Color;
                    vm.tabla_grafica[i].total_obtenidos = rs[0].total_obtenidos;
                    vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                    vm.tabla_grafica[i].Obtenidos = rs[0].Obtenidos;
                    vm.tabla_grafica[i].max = rs[0].max;
                    vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;

                    sum += !isNaN(vm.tabla_grafica[i].totalVotos) ? vm.tabla_grafica[i].totalVotos : 0;
                    totalObtenidos += angular.isUndefined(vm.tabla_grafica[i].total_obtenidos) ? 0 : vm.tabla_grafica[i].total_obtenidos;
                    p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;

                }
            }
            if (data.length > 0) {
                vm.objVotosDistrito = [];
                vm.objVotosDistrito = vm.tabla_grafica;
                vm.TotalVotos = sum==0?null:sum;
                vm.PorcentajeTotal = p==0?null: "100.0000%";
                vm.totalDistritos = totalObtenidos==0?null: totalObtenidos;

            }
        }


        /****************************/
      

        vm.porcentajeGrafica = function (total, por) {
            var resultado = vm._porcentajeGrafica(total, por);
            return vm.number_format(resultado, 0);
        };


        vm._porcentajeGrafica = function (total, por) {

            var tot = parseInt(total.toString().replace(/,/g, ""));
            var next = parseInt(tot.toString()[0]) + 1;

            var nextInt = next * Math.pow(10, tot.toString().length - 1);

            if ((nextInt - tot) <= nextInt * 0.1) {
                nextInt = nextInt + (nextInt * 0.1);
            }

            return nextInt * (por / 100);
        }

        vm.Suma = function (data, key) {
            if (angular.isUndefined(data) || angular.isUndefined(key))
                return 0;
            var sum = 0;
            angular.forEach(data, function (value) {
                sum = sum + parseInt(value[key], 10);
                /*obtener votacion maxima*/
                if (vm.votacionMaxima < parseInt(value[key], 10)) {
                    vm.votacionMaxima = parseInt(value[key], 10);
                }
            });
            return sum;
        }
        vm.number_format = function (amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        }

        $scope.$watch("ctrl.radioSelected", function (val) {
            if (val) {

            }
        })
  
        vm.Secciones = [];
        $scope.$watch("ctrl.idMunicipio", function (val) {
            if (val) {
                vm.Secciones = [];
                for (var i = 1; i <= val; i++) {
                    var seccion = { id: i, nombre: "Distrito_loc_ " + i };
                    vm.Secciones.push(seccion);
                }
                vm.idSeccion = "1";
            }
        });

        $scope.$watch("ctrl.idSeccion", function (val) {
            if (val) {

            }
        })


        $scope.$watch("ctrl.radioSelected", function (val) {
            if (val) {
                if (result) {
                    if (val == 1) {
                        vm.tabla_grafica = [];
                        vm.objVotosDistrito = vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                        DetallePorDistrito(resultHeader.tablaVotosCandidato);

                        if (result.votosCandidatura) {
                            aplicaInformacionGeneral(result.informacionGral.informacionGralDistrito);
                            LlenaTablaVotacion(result.votosCandidatura.tablaVotosCandidato);
                            LlenaDetalleMunicipios(result.votosCandidatura.tablaDistrito);

                            if (result.votosCandidatura.tablaActasDistrito.length > 0) {

                                for (var i = 0; i < vm.ActasDistritos.length; i++) {

                                    var rs = result.votosCandidatura.tablaActasDistrito.filter(function (item) {
                                        return item.id == vm.ActasDistritos[i].id;
                                    });

                                    if (rs.length > 0) {
                                        vm.ActasDistritos[i].Esperadas = rs[0].Esperadas;
                                        vm.ActasDistritos[i].Capturadas = rs[0].Capturadas;
                                        vm.ActasDistritos[i].Contabilizadas = rs[0].Contabilizadas;
                                        vm.ActasDistritos[i].Participacion_Ciudadana = rs[0].Participacion_Ciudadana;
                                        vm.ActasDistritos[i].TotalListaNominal = rs[0].TotalListaNominal;
                                    }
                                }

                            }
                        }
                    }
                    else if (val == 2) {
                        //vm.tabla_grafica = [];
                        vm.tabla_grafica = resultHeader.Partidos;
                        aplicaInformacionGeneral(result.informacionGral.informacionGralPartidos);
                        LlenaTablaVotacion(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                        DetallePorDistrito(resultHeader.Partidos);
                        LlenaDetalleMunicipios(result.votosCandidaturaIndependiente.tablaDistrito);

                        if (result.votosCandidaturaIndependiente.tablaActasPartido.length > 0) {

                            for (var i = 0; i < vm.ActasDistritos.length; i++) {

                                var rs = result.votosCandidaturaIndependiente.tablaActasPartido.filter(function (item) {
                                    return item.id == vm.ActasDistritos[i].id;
                                });

                                if (rs.length > 0) {
                                    vm.ActasDistritos[i].Esperadas = rs[0].Esperadas;
                                    vm.ActasDistritos[i].Capturadas = rs[0].Capturadas;
                                    vm.ActasDistritos[i].Contabilizadas = rs[0].Contabilizadas;
                                    vm.ActasDistritos[i].Participacion_Ciudadana = rs[0].Participacion_Ciudadana;
                                    vm.ActasDistritos[i].TotalListaNominal = rs[0].TotalListaNominal;
                                }
                            }

                        }
                    }
                    else {
                        BusyLoading();
                        vm.Partidos = resultHeader.PartidosSeccionDistritos;
                        vm.TotalVotos = undefined;
                        getDataSourceCasilla();

                    }
                }
            }          
        })

        getDataSourceCasilla = function () {
            DataService
           .getDiputacionDesgloseSeccion()
           .then(function (data) {
               vm.tabla_grafica = [];
               vm.VT;
               var rs = data.Dist_Sec_Casilla.filter(function (item) { return item.PrincipioEleccion_ID == 1; });
               var VotoTotal = rs.map(function (elemento) { return elemento["TOTAL_VOTOS_CALCULADO"] != null ? parseInt(elemento["TOTAL_VOTOS_CALCULADO"]) : undefined; }).reduce(function (suma, valor) {
                   return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)
               });

               vm.VT = VotoTotal;
               vm.PT="100.0000%"
               angular.forEach(vm.Partidos, function (item) {
                   var votos = { partidos: '', totalVotos: 0, porcentaje: 0 };
                   votos.totalVotos = rs.map(function (elemento) { return elemento[item.columna] != null ? parseInt(elemento[item.columna]) : undefined; }).reduce(function (suma, valor) {
                       return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)
                   });
                   votos.porcentaje = votos.totalVotos / vm.VT;
                   votos.porcentaje = (!isNaN(votos.porcentaje)) ? $rootScope.settings.TruncateDecimal((votos.porcentaje *100),4) : undefined;
                   if (angular.isUndefined(votos.porcentaje))
                     votos.totalVotos = undefined;
                   vm.tabla_grafica.push(votos);
               })

               $scope.Actas = new NgTableParams({ count: 20 },
                   {
                       counts: [],
                       //dataset: data.Dist_Sec_Casilla,
                       total: rs.length,
                       getData: function ($defer, params) {
                           var result = rs.slice((params.page() - 1) * params.count(), params.page() * params.count());
                           calculaParticipacionCiudadana(result);
                           $defer.resolve(result);

                       }
                   });
               if (!vm.ultimo_corte)
                   iniciaInterval();
               App.stopPageLoading();

           })
           .catch(function (err) {
               App.stopPageLoading();
           })
        }


        calculaParticipacionCiudadana = function (Actas) {
           // vm.ActasDistritos = [];
            var temp = [];

            angular.forEach(Actas, function (value) {
                var item = {};
                item.SECCION = value.SECCION;
                item.id = value.id;
                item.Nombre = value.Casilla_Descripcion;
                if (value.Casilla_Descripcion.indexOf('ESPECIAL') !== -1) {
                    item.especial = true;
                    item.Participacion_Ciudadana = null;
                }
                else {

                    item.TotalListaNominal = value.LISTA_NOMINAL;
                    if (value.CONTABILIZADA == 1) {
                        var tvln = parseInt(value.TOTAL_VOTOS_CALCULADO);
                        if (!isNaN(tvln))
                            item.Participacion_Ciudadana =$rootScope.settings.TruncateDecimal((tvln / item.TotalListaNominal * 100), 4);
                        else {
                            if (value.TOTAL_VOTOS_CALCULADO != null && value.TOTAL_VOTOS_CALCULADO.indexOf('EXCEDE') !== -1)
                                item.Participacion_Ciudadana = 0;
                            else
                                item.Participacion_Ciudadana = null;
                        }
                    }
                    else {
                        if (value.TOTAL_VOTOS_CALCULADO != null && value.TOTAL_VOTOS_CALCULADO.indexOf('EXCEDE') !== -1)
                            item.Participacion_Ciudadana = 0;
                        else
                            item.Participacion_Ciudadana = null;
                    }

                }
                temp.push(item);
            })
            vm.LtParticipacionCiudadana = $filter('orderBy')(temp, "id");
        }


        $scope.isNumber = function (n) {

            return !isNaN(parseFloat(n)) || isFinite(n);
        }

        vm.CasillasRP = function ()
        {
            $state.go("seccion_casilla", { pIdSeccion: -1, modulo:'DIP'});
        }


        vm.reloadRoute = function () {
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            BusyLoading();
            getDataSource();
            //$state.reload();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }
});