﻿angular.module('PrepetApp', [])
    .controller('ayuntamientoSeccionController', function ($rootScope, $scope, $http, $timeout, $state, $filter, DataService, HeaderService, $interval) {

        var miscelanea = 60000;
        var tiempo;
        var encabezado = {};
        var update = false;
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
        $('html, body').animate({ scrollTop: 0 }, 500);
        Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_ayuntamiento_seccion'), $state);
        $rootScope.settings.layout.pageBodySolid = true;
        $rootScope.settings.layout.pageSidebarClosed = true;
        });
        
    var vm = this;
    
    BusyLoading = function () {
        var option = { animate: true };
        App.startPageLoading(option);
    }


    iniciaInterval = function () {
        if (miscelanea > 0) {
            tiempo = $interval(function () {
                getDataSource();
            }, miscelanea, true);
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
        }
    }


    vm.loadPage = function () {
        BusyLoading();
        vm.modulo = "3S";
        vm.radioSelected = 1;
        //Quitar el selector
        vm.GrupoSelector = true;
        //texto para los radio botones
        vm.VotosCandidatura = "Secciones por Municipio";
        vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";       
        //modulo
        vm.titulo = 'Sección por Municipio';
        vm.ambitoEstadistica = null;
        vm.descripcion = null;       
        vm.Secciones = [];

        vm.titulo_detalle_votos = "Detalle de votos por Sección";
        vm.titulo_tabla = "Actas por Seccion";
        vm.titulo_actas = "Actas por Sección";
        vm.item_acta = {};
        vm.inicio = true;
        vm.detallevotos_primera_columna = "Secciones";


        HeaderService.
   getMunicipios()
       .then(function (data) {
           BusyLoading();
           encabezado = data;
           vm.Municipios = angular.copy(data.Municipios);
          // vm.Municipios = angular.copy(data.Municipios);         

           getDataSource();
       })
       .catch(function (err) {
           App.stopPageLoading();
       })

       
    }
    getDataSource = function () {
        DataService
                  .getAyuntamientoDesgloseSeccion()
                  .then(function (data) {
                      result = data;
                      miscelanea = data.informacionGral.timer_update;
                      vm.actasCapturadas = data.informacionGral.actasCapturadas;
                      vm.actasEsperadas = data.informacionGral.actasEsperadas;
                      vm.actasRecibidas = data.informacionGral.actasRecibidas;
                      vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
                      vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
                      vm.horaCorte = data.informacionGral.horaCorte;
                      vm.fechaCorte = data.informacionGral.fechaCorte;
                      vm.ultimo_corte = data.informacionGral.ultimo_corte;
                      //vm.Municipios = data.ltDistritos;
                      //vm.Partidos = data.Partidos;
                      $timeout(function ()
                      {
                          if (update)
                          {
                                  vm.ActasDistritos = [];
                                  vm.ltSecciones = [];
                                  vm.tabla_grafica = [];
                                  vm.visible = false;
                                  vm.idSeccion = vm.idMunicipio;
                                  var ltSecciones = result.ltSecciones.filter(function (item) { return item.distrito == vm.idMunicipio; });
                                  var secciones = result.Dist_Sec_Casilla.filter(function (item) { return item.AMBITO_ID == vm.idMunicipio && item.PrincipioEleccion_ID == 1; });

                                  var actas = result.Actas.filter(function (item) { return item.Municipio == vm.idMunicipio; });
                                  agrupaDistritoSeccion(ltSecciones, secciones, actas);
                                  App.stopPageLoading();
                                  update = false;
                              
                          }
                          else
                          {
                              if (!angular.isUndefined($rootScope.$stateParams.pIdDistrito) && $rootScope.$stateParams.pIdDistrito != '')
                                  vm.idMunicipio = $rootScope.$stateParams.pIdDistrito;
                              else if (!angular.isUndefined($rootScope.$stateParams.pIdMunicipio) && $rootScope.$stateParams.pIdMunicipio != '')
                                  vm.idMunicipio = $rootScope.$stateParams.pIdMunicipio;
                              else if (vm.idMunicipio == '' || vm.idMunicipio == 0 || angular.isUndefined(vm.idMunicipio)) {
                                  if (vm.Municipios.length > 0)
                                      vm.idMunicipio = "" + vm.Municipios[0].id;
                              }
                          }

                          if (vm.ultimo_corte) {
                              $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
                          }
                          else
                             iniciaInterval();
                      }, 100);

                      App.stopPageLoading();
                  })
                  .catch(function (err) {
                      App.stopPageLoading();
                  })
    }
   
    vm.nombreTabla = "Estadística de secciones por Municipios";

    $scope.$watch("ctrl.idMunicipio", function (val) {
        if (val) {            
            if (!result.informacionGral)
                return;
            BusyLoading();
           
                //si realiza filtro
            $timeout(function () {
                vm.seccionSeleccion = undefined;
                var p = encabezado.Secciones.filter(function (item) { return item.id == vm.idMunicipio; });            
                vm.Partidos = p[0].Partidos;
                vm.ActasDistritos = [];
                vm.ltSecciones = [];
                vm.tabla_grafica = [];
                vm.TotalVotos = null;
                vm.Porcentaja = null;
                if (vm.Partidos.length > 0) {
                    vm.visible = false;
                    vm.idSeccion = val.toString();
                    var ltSecciones = result.ltSecciones.filter(function (item) { return item.distrito == vm.idMunicipio; });
                    var secciones = result.Dist_Sec_Casilla.filter(function (item) { return item.AMBITO_ID == vm.idMunicipio && item.PrincipioEleccion_ID == 1;; });
                    var actas = result.Actas.filter(function (item) { return item.Municipio == vm.idMunicipio; });
                    agrupaDistritoSeccion(ltSecciones, secciones, actas);
                    App.stopPageLoading();
                } else {
                    App.stopPageLoading();
                }
                }, 50);
           
           
            //PorcentajeTotalDeActas(actas);
        }
    });

    agrupaDistritoSeccion = function (secciones, contexto, actasCasilla) {

        vm.ltSecciones = [];
        vm.tabla_grafica = [];

        angular.forEach(secciones, function (value) {
            angular.forEach(vm.Partidos, function (item) {
                Object.defineProperty(value, item.columna, {
                    value: null,
                    writable: true,
                    enumerable: true,
                    configurable: true
                });
            })
            Object.defineProperty(value, "TotalVotos", {
                value: null,
                writable: true,
                enumerable: true,
                configurable: true
            });
            var actas = contexto.filter(function (item) {
                return item.SECCION == value.id;
            });
            //inserta los datos en cada celda por sección
            actas.forEach(function (v_actas) {
                angular.forEach(vm.Partidos, function (item) {
                    var valor = v_actas[item.columna];
                    //Si el valor no es nulo y es numerico 
                    if (valor != null && !isNaN(valor)) {
                        value[item.columna] = value[item.columna] + parseInt(valor);
                        value.TotalVotos = value.TotalVotos + parseInt(valor);

                    }
                    else {
                        //Si el valor no es nulo y es un texto
                        if (valor != null && valor.length > 0) {
                            if (value[item.columna] == 0 || value[item.columna] == null)
                                value[item.columna] = 0;
                            //if (value.TotalVotos == 0 || value[item.columna] == null)
                            // value.TotalVotos = 0;
                            if(value.TotalVotos == null)
                            value.TotalVotos = 0;
                        }
                    }
                    
                })
            })
        })
        
        vm.ltSecciones = secciones;
        vm.TotalVotos = vm.ltSecciones.map(function (elemento) { return elemento.TotalVotos; }).reduce(function (suma, valor){ return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)});
        vm.Porcentaje = null;
        var sumporcentaje = 0;
        angular.forEach(vm.Partidos, function (item) {
            var votos = { partidos: '', totalVotos: 0, porcentaje: 0 };
            votos.partidos = item.columna;
            votos.totalVotos = vm.ltSecciones.map(function (elemento) { return elemento[item.columna]!=null?elemento[item.columna]:undefined; }).reduce(function (suma, valor)
            {
                return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)
            });
            votos.porcentaje = (votos.totalVotos / vm.TotalVotos)*100;
            votos.porcentaje = (!isNaN(votos.porcentaje)) ? votos.porcentaje : undefined;
            sumporcentaje += sumporcentaje + (!isNaN(votos.porcentaje)) ? votos.porcentaje : 0;
            if (angular.isUndefined(votos.porcentaje))
                votos.totalVotos = undefined;
            vm.tabla_grafica.push(votos);
        })
        if (sumporcentaje == 0)
            vm.Porcentaje = null;
        else
            vm.Porcentaje ="100.0000%";
        if (vm.TotalVotos == 0)
            vm.TotalVotos = null;

        //Inicia el calculo de los porcentajes de las secciones
        vm.ActasDistritos = [];
        var temp = [];
        var resultado = {};
        resultado.id = -1;
        resultado.Nombre = "Total";
        resultado.Esperadas = 0;
        resultado.Capturadas = 0;
        resultado.Contabilizadas = 0;
        resultado.TotalListaNominal = 0;
        var sumPc = 0;
        var tvGeneral = 0;
        angular.forEach(secciones, function (value)
        {
            var actas = actasCasilla.filter(function (item) {
                return item.Seccion == value.id;
            });
            var item = {};
            item.id = value.id;
            item.Nombre = value.Nombre;
            item.Esperadas = actas.length;
            resultado.Esperadas = resultado.Esperadas + item.Esperadas;
            var cpt = actas.filter(function (item) { return item.Capturadas == 1; });
            item.Capturadas = cpt.length;
            resultado.Capturadas = resultado.Capturadas+item.Capturadas;
            var ctb = actas.filter(function (item) { return item.Contabilizada == 1; });           
            item.Contabilizadas = ctb.length
            resultado.Contabilizadas = resultado.Contabilizadas + item.Contabilizadas;
            item.TotalListaNominal = actas.map(function (item) { return item.ListaNominal; }).reduce(function (suma, valor) { return suma + valor });
            
            resultado.TotalListaNominal = resultado.TotalListaNominal + item.TotalListaNominal;
            if (value.TotalVotos > 0 && item.TotalListaNominal > 0)
            {
                if (vm.ultimo_corte) {
               
                    item.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((value.TotalVotos / item.TotalListaNominal * 100), 4);
                }
                else {
                    
                    var tc = contexto.filter(function (elemento) {
                        return elemento.SECCION == value.id && (elemento.TIPO_CASILLA_ID == 1 || elemento.TIPO_CASILLA_ID == 2 || elemento.TIPO_CASILLA_ID == 3);
                    });
                    var tvc = tc.map(function (item) { return !isNaN(item.TOTAL_VOTOS_CALCULADO) ? parseInt(item.TOTAL_VOTOS_CALCULADO) : 0; }).reduce(function (suma, valor) { return suma + valor });
                    if (!isNaN(tvc))
                    {
                        tvGeneral += tvc;
                        item.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((tvc / item.TotalListaNominal * 100), 4);
                    }

                }

                
               sumPc += item.Participacion_Ciudadana;
               
            }
            temp.push(item);
        })

        //Verificar
        resultado.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal(( (vm.ultimo_corte ? vm.TotalVotos : tvGeneral) / resultado.TotalListaNominal) * 100, 4);



        temp.push(resultado);
        var porcentaje = {};
        porcentaje.id = 0;
        porcentaje.Nombre = "Porcentaje";
        porcentaje.Esperadas = 100;
        if (resultado.Capturadas > 0 && resultado.Esperadas > 0)
            porcentaje.Capturadas = $rootScope.settings.TruncateDecimal((resultado.Capturadas / resultado.Esperadas) * 100, 4);
        else
            porcentaje.Capturadas = 0;
        if (resultado.Contabilizadas > 0 && resultado.Esperadas > 0)
            porcentaje.Contabilizadas = $rootScope.settings.TruncateDecimal((resultado.Contabilizadas / resultado.Esperadas) * 100, 4);
        else
            porcentaje.Contabilizadas = 0;
        porcentaje.Participacion_Ciudadana = 0;
        porcentaje.TotalListaNominal = 0;
        temp.push(porcentaje);
        vm.ActasDistritos = $filter('orderBy')(temp, "id");
        if (vm.ltSecciones.length > 0) {
            if (angular.isUndefined(vm.seccionSeleccion))
                vm.seccionSeleccion = vm.ltSecciones[0].id;

        }
    }

    PorcentajeTotalDeActas = function (actas) {
        var total = {};
        total.id = -1;
        total.Nombre = "Total";
        total.Esperadas = actas.map(function (item) { return item.Esperadas; }).reduce(function (suma, valor) { return suma + valor });
        total.Capturadas = actas.map(function (item) { return item.Capturadas; }).reduce(function (suma, valor) { return suma + valor });
        total.Contabilizadas = actas.map(function (item) { return item.Contabilizadas; }).reduce(function (suma, valor) { return suma + valor });
        total.TotalListaNominal = actas.map(function (item) { return item.TotalListaNominal; }).reduce(function (suma, valor) { return suma + valor });
        total.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((vm.TotalVotos / total.TotalListaNominal * 100), 4);

        actas.push(total);
        var porcentaje = {};
        porcentaje.id = 0;
        porcentaje.Nombre = "Porcentaje";
        porcentaje.Esperadas = 100;
        if (total.Capturadas > 0 && total.Esperadas > 0)
            porcentaje.Capturadas = $rootScope.settings.TruncateDecimal((total.Capturadas / total.Esperadas) * 100, 4);
        else
            porcentaje.Capturadas = 0;
        if (total.Contabilizadas > 0 && total.Esperadas > 0)
            porcentaje.Contabilizadas = $rootScope.settings.TruncateDecimal((total.Contabilizadas / total.Esperadas) * 100, 4);
        else
            porcentaje.Contabilizadas = 0;
        porcentaje.Participacion_Ciudadana = 0;
        porcentaje.TotalListaNominal = 0;
        actas.push(porcentaje);
        var actasOrdenadas = $filter('orderBy')(actas, "id");
        vm.ActasDistritos = actasOrdenadas;
    }

    $scope.$watch("ctrl.idSeccion", function (val) {
        if (val)
        {

        }
    })

    vm.reloadRoute = function () {
        // $state.reload();
        update = true;
        BusyLoading();
        $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
        getDataSource();
    }
    vm.exportarDatos = function () {
        window.location.href = $rootScope.settings.basedatos;
    }
});