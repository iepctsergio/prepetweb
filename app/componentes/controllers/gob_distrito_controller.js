﻿angular.module('PrepetApp', [])
    .controller('gobDistritoController', function ($rootScope, $scope, $timeout, $filter, $state, DataService, HeaderService, $interval, NgTableParams) {
        var vm = this;        
        var result = {};
        var resultHeader = {};
        var DistritoCandidatura = [];
        var DistritoPartidos = [];
        var miscelanea = 60000;
        var tiempo;
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_gob_distritos'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;
            //loadPage();
        });

        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                    if (vm.radioSelected==3)
                       getDataSourceCasilla();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }

      
        vm.loadPage = function () {
            BusyLoading();
           
            vm.modulo = "1D";//gobernatura distrito > seccion distrito
            vm.titulo = "Gubernatura";
            vm.ambitoEstadistica = "- Distritos";
            vm.descripcion = " El total de votos calculado y porcentaje que se muestran, se refieren a los votos asentados en las Actas PREP hasta el momento. \nPor presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%."
            vm.VotosCandidatura = "Votos por Candidatura";
            vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
            vm.detalleCasilla = "Detalle Casilla";
            vm.tipoAmbito = "1S";
            vm.radioSelectedCasilla = true;
            vm.radioSelected = 1;            
            vm.titulo_detalle_votos = "Detalle de votos por Distritos";
            vm.titulo_actas = "Actas por Distritos";
            vm.votacionMaxima = -1;
            vm.mostrarNombreCandidato = true;

            vm.item_acta = {};
            vm.inicio = true;
            vm.item_distrito = {};

            HeaderService.
       getGobEntidad()
           .then(function (data) {
               resultHeader = data;
               vm.candidatos = vm.tabla_grafica = data.tablaVotosCandidato;
             
               vm.detallevotos_primera_columna = "Detalle de votos por Distritos";             
               vm.DetalleVotos = data.Distritos;
               DetallePorDistrito(data.tablaVotosCandidato);
               DetallePorActas(data.Distritos);
               getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })

        }//Fin del page load

        DetallePorActas = function (data)
        {
            vm.ActasDistritos = [];
            var t = {};
            t.id = -1;
            t.Nombre = "Total";
            t.Participacion_Ciudadana = 0;
            vm.ActasDistritos.push(t);
            var t2 = {};
            t2.id = 0;
            t2.Nombre = "Porcentaje";
            t2.Participacion_Ciudadana = 0;
            vm.ActasDistritos.push(t2);
            for (var i = 0; i < data.length; i++)
            {
                var item = {};
                item.id = data[i].id;
                item.Nombre = data[i].Nombre;
                item.Participacion_Ciudadana = 0;
                vm.ActasDistritos.push(item);
            }
        }

        DetallePorDistrito = function (data)
        {
            if (angular.isUndefined(vm.DetalleVotos))
                return;
            for (var i = 0; i < vm.DetalleVotos.length; i++)
            {
                vm.DetalleVotos[i].Partidos = [];
                vm.DetalleVotos[i].Partidos = data;
            }
        }

        getDataSource = function () {
            DataService
               .getGubernaturaDistrito()
               .then(function (data) {
                   result = data;
                   miscelanea = data.informacionGral.timer_update;
                   vm.actasCapturadas = data.informacionGral.actasCapturadas;
                   vm.actasEsperadas = data.informacionGral.actasEsperadas;
                   vm.actasRecibidas = data.informacionGral.actasRecibidas;
                   vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
                   vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
                   vm.participacion = data.informacionGral.participacion;
                   vm.horaCorte = data.informacionGral.horaCorte;
                   vm.fechaCorte = data.informacionGral.fechaCorte;
                   vm.ultimo_corte = data.informacionGral.ultimo_corte;
                   //vm.tabla_grafica = data.votosCandidatura.tablaVotosCandidato;
                   //vm.candidatos = data.votosCandidatura.tablaVotosCandidato;
                   //vm.TotalVotos = Suma(vm.tabla_grafica, 'totalVotos');
                  
                   if (vm.radioSelected == 1)
                   {
                       if (data.votosCandidatura.tablaVotosCandidato.length > 0)
                           LlenaTablaGrafica(data.votosCandidatura.tablaVotosCandidato);

                       LlenaTablaDetalleXDistrito(result.votosCandidatura.tablaDistrito);

                       if (vm.DetalleVotos.length > 0) {
                           if (angular.isUndefined(vm.idDistrito)) {
                               vm.item_distrito = vm.DetalleVotos[0];
                               vm.idDistrito = "" + vm.item_distrito.id;
                           } else {
                               var x = vm.idDistrito;
                               vm.idDistrito = "0";
                               $timeout(function () {
                                   vm.idDistrito = x;
                               }, 70);
                               
                             
                           }
                           
                       }
                      // vm.ActasDistritos = result.votosCandidatura.tablaActasDistrito;

                       vm.mostrarNombreCandidato = true;
                   }
                   else if (result.votosCandidaturaIndependiente.tablaVotosCandidato.length > 0)
                   {
                       LlenaTablaGrafica(result.votosCandidaturaIndependiente.tablaVotosCandidato);

                       //vm.ActasDistritos = result.votosCandidaturaIndependiente.tablaActasDistrito;
                       LlenaTablaDetalleXDistrito(result.votosCandidaturaIndependiente.tablaDistrito);
                       if (vm.DetalleVotos.length > 0) {
                           if (angular.isUndefined(vm.idDistrito)) {
                               vm.idDistrito = "" + vm.DetalleVotos[0].id;
                           }
                           else
                           {
                               var x = vm.idDistrito;
                               vm.idDistrito = "0";
                               $timeout(function () {
                                   vm.idDistrito = x;
                               }, 70);
                           }
                       }
                       vm.mostrarNombreCandidato = false;
                   }

                   LlenarActas();

                   //vm.ActasDistritos = data.votosCandidatura.tablaActasDistrito;
                   if (vm.ultimo_corte) {
                       $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
                   }
                   else
                   iniciaInterval();
                   App.stopPageLoading();
               })
               .catch(function (err) {
                   App.stopPageLoading();
               })
        }

        LlenarActas = function () {

            if (result.votosCandidatura.tablaActasDistrito.length > 0) {

                for (var i = 0; i < vm.ActasDistritos.length; i++) {

                    var rs = result.votosCandidatura.tablaActasDistrito.filter(function (item) {
                        return item.id == vm.ActasDistritos[i].id;
                    });

                    if (rs.length > 0) {
                        vm.ActasDistritos[i].Esperadas = rs[0].Esperadas;
                        vm.ActasDistritos[i].Capturadas = rs[0].Capturadas;
                        vm.ActasDistritos[i].Contabilizadas = rs[0].Contabilizadas;
                        vm.ActasDistritos[i].Participacion_Ciudadana = rs[0].Participacion_Ciudadana;
                        vm.ActasDistritos[i].TotalListaNominal = rs[0].TotalListaNominal;
                    }
                }

            }
        }

        LlenaTablaDetalleXDistrito = function (data) {
            for (var i = 0; i < vm.DetalleVotos.length; i++) {

                var rs = data.filter(function (item) {
                    return item.id == vm.DetalleVotos[i].id;
                });
                if (rs.length > 0)
                {
                    vm.DetalleVotos[i].Partidos = [];
                    vm.DetalleVotos[i].Partidos = rs[0].Partidos;
                    vm.DetalleVotos[i].TotalVotos = rs[0].TotalVotos;
                }
            }
        }

        LlenaTablaGrafica = function (data) {
            var sum = 0; var p = 0;
            angular.forEach(vm.tabla_grafica, function (value) {
                angular.forEach(data, function (item) {
                    if (value.Clave === item.logoPartido) {
                        value.porcentaje = item.porcentaje;
                        value.totalVotos = item.totalVotos;
                        sum += value.totalVotos;
                        value.max = item.max;
                        p += value.porcentaje;
                    }
                })
            })
            vm.TotalVotos = sum;
            vm.PorcentajeTotal = "100.0000";
        }

     
        Suma = function (data, key) {
            if (angular.isUndefined(data) || angular.isUndefined(key))
                return 0;
            var sum = 0;
            angular.forEach(data, function (value) {
                sum = sum + parseInt(value[key], 10);
                /*obtener votacion maxima*/
                if (vm.votacionMaxima < parseInt(value[key], 10)) {
                    vm.votacionMaxima = parseInt(value[key], 10);
                }
            });
            return sum;
        }
      
        vm.number_format = function (amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        }

        $scope.$watch("ctrl.radioSelected", function (val) {
            if (val) {
                
                if (val == 1)
                {
                    if (angular.isUndefined(vm.DetalleVotos))
                        return;                  
                    vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                    LlenaTablaGrafica(result.votosCandidatura.tablaVotosCandidato);
                    DetallePorDistrito(resultHeader.tablaVotosCandidato);
                    LlenaTablaDetalleXDistrito(result.votosCandidatura.tablaDistrito);
                    if (vm.DetalleVotos.length > 0) {
                        vm.item_distrito = vm.DetalleVotos[1];
                        vm.idDistrito = "" + vm.item_distrito.id;
                    }

                    vm.mostrarNombreCandidato = true;
                }
                else if (val == 2) {
                    vm.tabla_grafica = resultHeader.Partidos;
                    LlenaTablaGrafica(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                    DetallePorDistrito(resultHeader.Partidos);
                    LlenaTablaDetalleXDistrito(result.votosCandidaturaIndependiente.tablaDistrito);
                    if (vm.DetalleVotos.length > 0) {
                        vm.idDistrito = "" + vm.DetalleVotos[1].id;
                    }
                    vm.mostrarNombreCandidato = false;

                }
                else {
                    BusyLoading();
                    vm.Partidos = resultHeader.SeccionPartidos;
                    //vm.tabla_grafica = [];
                    vm.TotalVotos = undefined;
                    getDataSourceCasilla();
                    
                }
             
            }
            
        })

       

        getDataSourceCasilla = function () {
            DataService
           .getGubernaturaDesgloseSeccion()
           .then(function (data) {
               vm.tabla_grafica = [];
               vm.VT;
               var rs = data.Dist_Sec_Casilla.filter(function (item) { return item.PrincipioEleccion_ID == 1; });
               var VotoTotal = rs.map(function (elemento) { return elemento["TOTAL_VOTOS_CALCULADO"] != null ? parseInt(elemento["TOTAL_VOTOS_CALCULADO"]) : undefined; }).reduce(function (suma, valor) {
                   return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)
               });

               vm.VT = VotoTotal;
               vm.PT = "100.0000%"
               angular.forEach(vm.Partidos, function (item) {
                   var votos = { partidos: '', totalVotos: 0, porcentaje: 0 };
                   votos.totalVotos = rs.map(function (elemento) { return elemento[item.columna] != null ? parseInt(elemento[item.columna]) : undefined; }).reduce(function (suma, valor) {
                       return (!isNaN(suma) ? suma : 0) + (!isNaN(valor) ? valor : 0)
                   });
                   votos.porcentaje = votos.totalVotos / vm.VT;
                   votos.porcentaje = (!isNaN(votos.porcentaje)) ? $rootScope.settings.TruncateDecimal((votos.porcentaje * 100),4) : undefined;
                   if (angular.isUndefined(votos.porcentaje))
                       votos.totalVotos = undefined;
                   vm.tabla_grafica.push(votos);
               })

               $scope.Actas = new NgTableParams({ count: 20 },
                   {
                       counts: [],
                       //dataset: data.Dist_Sec_Casilla,
                       total: rs.length,
                       getData: function ($defer, params) {                          
                           var result = rs.slice((params.page() - 1) * params.count(), params.page() * params.count());
                           calculaParticipacionCiudadana(result);
                           $defer.resolve(result);

                       }
                   });
               if(!vm.ultimo_corte)
                 iniciaInterval();
               App.stopPageLoading();            

           })
           .catch(function (err) {
               App.stopPageLoading();
           })
        }

      
        calculaParticipacionCiudadana = function (Actas) {
            //vm.ActasDistritos = [];
            var temp = [];

            angular.forEach(Actas, function (value) {
                var item = {};
                item.SECCION = value.SECCION;
                item.id = value.id;
                item.Nombre = value.Casilla_Descripcion;
                if (value.Casilla_Descripcion.indexOf('ESPECIAL') !== -1) {
                    item.especial = true;
                    item.Participacion_Ciudadana = null;
                }
                else {
                    
                    item.TotalListaNominal = value.LISTA_NOMINAL;
                    if (value.CONTABILIZADA == 1) {
                        var tvln = parseInt(value.TOTAL_VOTOS_CALCULADO);
                        if (!isNaN(tvln))
                            item.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((tvln / item.TotalListaNominal * 100), 4);
                        else {
                            if (value.TOTAL_VOTOS_CALCULADO != null && value.TOTAL_VOTOS_CALCULADO.indexOf('EXCEDE') !== -1)
                                item.Participacion_Ciudadana = 0;
                            else
                                item.Participacion_Ciudadana = null;
                        }
                    }
                    else {
                        if (value.TOTAL_VOTOS_CALCULADO != null && value.TOTAL_VOTOS_CALCULADO.indexOf('EXCEDE') !== -1)
                            item.Participacion_Ciudadana = 0;
                        else
                            item.Participacion_Ciudadana = null;
                    }

                }
                temp.push(item);
            })
            vm.LtParticipacionCiudadana = $filter('orderBy')(temp, "id");
        }


        $scope.isNumber = function (n) {

            return !isNaN(parseFloat(n)) || isFinite(n);
        }
      
       
        $scope.$watch("ctrl.idDistrito", function (val)
        {
            vm.item_acta = {};
            if (val) {
                if (vm.inicio) {
                    $timeout(function () {
                        for (var i = 0; i < vm.ActasDistritos.length; i++) {
                            if (vm.ActasDistritos[i].id == val) {
                                vm.item_acta = vm.ActasDistritos[i];
                                break;
                            }
                        }
                    }, 1000);
                    vm.inicio = false;
                }
                else {
                    for (var i = 0; i < vm.DetalleVotos.length; i++) {
                        if (val == vm.DetalleVotos[i].id) {
                            vm.item_distrito = vm.DetalleVotos[i];
                            break;
                        }
                    }


                    for (var i = 0; i < vm.ActasDistritos.length; i++) {
                        if (vm.ActasDistritos[i].id == val) {
                            vm.item_acta = vm.ActasDistritos[i];
                            break;
                        }
                    }
                }
            }
        })
        
        vm.getSelectedCalendar = function(val) {
            
        }

        vm.reloadRoute = function () {
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            BusyLoading();
            getDataSource();

            if (vm.radioSelected == 3)
                getDataSourceCasilla();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }    
    })
  /*******************************************************************/
  /**********************inicio prueba doble scroll*******************/
  /*******************************************************************/

.directive('scroll', [function () {
    return {
        link: function (scope, element, attrs) {
            // ng-repeat delays the actual width of the element.
            // this listens for the change and updates the scroll bar
            function widthListener() {
                if (anchor.width() != lastWidth)
                    updateScroll();
            }

            function updateScroll() {
                // for whatever reason this gradually takes away 1 pixel when it sets the width.
                $div2.width(anchor.width() + 1);

                // make the scroll bars the same width
                $div1.width($div2.width());

                // sync the real scrollbar with the virtual one.
                $wrapper1.scroll(function () {
                    $wrapper2.scrollLeft($wrapper1.scrollLeft());
                });

                // sync the virtual scrollbar with the real one.
                $wrapper2.scroll(function () {
                    $wrapper1.scrollLeft($wrapper2.scrollLeft());
                });
            }

            var anchor = element.find('[data-anchor]'),
                lastWidth = anchor.width(),
                listener;

            // so that when you go to a new link it stops listening
            element.on('remove', function () {
                clearInterval(listener);
            });

            // creates the top virtual scrollbar
            element.wrapInner("<div class='div2' />");
            element.wrapInner("<div class='wrapper2' />");

            // contains the element with a real scrollbar
            element.prepend("<div class='wrapper1'><div class='div1'></div></div>");

            var $wrapper1 = element.find('.wrapper1'),
                $div1 = element.find('.div1'),
                $wrapper2 = element.find('.wrapper2'),
                $div2 = element.find('.div2')

            // force our virtual scrollbar to work the way we want.
            $wrapper1.css({
                width: "100%",
                border: "none 0px rgba(0, 0, 0, 0)",
                overflowX: "scroll",
                overflowY: "hidden",
                height: "20px",
            });

            $div1.css({
                height: "20px",
            });

            $wrapper2.css({
                width: "100%",
                overflowX: "scroll",
            });

            listener = setInterval(function () {
                widthListener();
            }, 650);

            updateScroll();
        }
    }
}]);