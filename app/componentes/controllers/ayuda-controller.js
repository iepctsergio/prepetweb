﻿angular.module('PrepetApp', [])
    .controller('ayudaController', function ($rootScope, $scope, $http, $timeout, $filter, $state, $stateParams) {
        var vm = this;
        vm.ventana = 0;
        vm.acercaVoto = $rootScope.$stateParams.av;
        vm.acta = $rootScope.$stateParams.acta;
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_ayuda'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;            
        });

        vm.loadPage = function () {

            vm.solicitud = $rootScope.$stateParams.id;            
            if (($rootScope.$stateParams.id != 1) && ($rootScope.$stateParams.id > 0)) {
                vm.ventana = vm.solicitud;
            }
            else {
                vm.ventana = 1;
            }

            if (($rootScope.$stateParams.av != 1) && ($rootScope.$stateParams.av > 0)) {
                vm.acercaVoto = $rootScope.$stateParams.av;
            }
            else {
                vm.acercaVoto = 1;
            }
            
            if (($rootScope.$stateParams.acta != 1) && ($rootScope.$stateParams.acta > 0)) {
                vm.acta = $rootScope.$stateParams.acta;
            }
            else {
                vm.acta = 1;
            }
            
            vm.glosario = 1;
            
        }       
    });