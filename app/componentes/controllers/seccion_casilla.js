﻿angular.module('PrepetApp', [])

    .controller('seccionCasillaController', function ($rootScope, $scope, $http, $timeout, $filter, DataService, HeaderService, $interval) {
        var vm = this;        
        var miscelanea = 60000;
        var tiempo;

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            //loadPage();
            $('html, body').animate({ scrollTop: 0 }, 500);
        });

        vm.pIdSeccion = ("0000" + $rootScope.$stateParams.pIdSeccion).slice(-4);
        vm.pIdMunicipio = $rootScope.pIDMunicipio;
        vm.nomMunicipio = $rootScope.Nombre;
        
        vm.modulo = $rootScope.$stateParams.modulo;
        vm.resultado;
        if (vm.modulo == 'DIP')
        {
            vm.titulo = $rootScope.$state.current.data.pageTitle = "Diputación"
            
        }
        else {
            switch (vm.modulo) {
                case '1D':
                case '1S':
                    vm.titulo = $rootScope.$state.current.data.pageTitle = "Gobernador";
                    break;
                case '2D':
                case '2S':
                    vm.titulo = $rootScope.$state.current.data.pageTitle = "Diputación";
                    break;
                case '3D':
                case '3S':
                    vm.titulo = $rootScope.$state.current.data.pageTitle = "Ayuntamiento";
                    break;

            }
        }
       
        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }

        vm.loadPage = function () {
            BusyLoading();
            vm.casilla_especial = false;
            vm.ambitoEstadistica = " -Sección - Casilla";
            vm.descripcion = "Cantidad de votos en cada una de las casillas de esta Sección, conforme a la información de las Actas PREP hasta el momento. Por presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%."
            vm.VotosCandidatura = "Sección / Desglose por casilla";
            vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
            vm.radioSelected = 1;
            vm.GrupoSelector = true;
            vm.ventana = "Casilla";
            vm.item_acta = {};
            vm.inicio = true;
            //if (vm.modulo == '1D' || vm.modulo == '1S'||vm.modulo=='DIP')
            if (vm.modulo == 'DIP')
            {
                HeaderService.
                getCasilla("DIP", 0)
               .then(function (data)
               {
                   vm.Partidos = [];
                   for (var i = 0; i < data.length; i++) {
                       if (data[i].Clave !== 'CIND') {
                           var partido = {};
                           partido.columna = data[i].Clave;
                           partido.logo = data[i].Clave === "CNR" ? "CANDIDATURA NO REGISTRADA" : data[i].Clave === 'VN' ? "VOTOS NULOS" : data[i].logoPartido;
                           vm.Partidos.push(partido);
                       }

                   }
                   
                   getDataSource();
               })
               .catch(function (err) {
                   App.stopPageLoading();
               })
                //getDataSource();
            }
            else {
                HeaderService.
                getCasilla(vm.modulo, $rootScope.$stateParams.pIdDistrito)
               .then(function (data) {                   
                   vm.Partidos = data;
                   getDataSource();
               })
               .catch(function (err) {
                   App.stopPageLoading();
               })
            }
            
           // getDataSource();
            
        }//Fin del load page

        getDataSource = function ()
        {
            if (vm.modulo == 'DIP')//Casillas especiales
            {
                vm.casilla_especial = true;
                vm.ambitoEstadistica = "- Casillas Especiales";
                vm.descripcion = "Cantidad de votos por Representación Proporcional en cada casilla especial que conforma la entidad; de acuerdo a la información asentada en las actas de escrutinio y cómputo.";
                vm.VotosCandidatura = "Casillas Especiales";
                DataService
             .getCasilla("2S")
             .then(function (data) {
                 vm.resultado = data;
                 miscelanea = data.informacionGral.timer_update;
                 vm.actasCapturadas = data.informacionGral.actasCapturadas;
                 vm.actasEsperadas = data.informacionGral.actasEsperadas;
                 vm.actasRecibidas = data.informacionGral.actasRecibidas;
                 vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
                 vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
                 vm.ultimo_corte = data.informacionGral.ultimo_corte;

                 vm.horaCorte = data.informacionGral.horaCorte;
                 vm.fechaCorte = data.informacionGral.fechaCorte;                     

                 var rs = data.Dist_Sec_Casilla.filter(function (item) { return item.PrincipioEleccion_ID == 2; });
                 var temp = $filter('orderBy')(rs, "SECCION");
                 angular.forEach(temp, function (value) {
                     value.Casilla_Descripcion = value.SECCION + " " + value.Casilla_Descripcion;
                 })
                 vm.Actas = temp;
                 App.stopPageLoading();

                 if (vm.ultimo_corte) {
                     $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
                 }
                 else
                     iniciaInterval();
             })
                .catch(function (err) {
                    App.stopPageLoading();
                    iniciaInterval();
                })
                
            }
            else {

                DataService
               .getCasilla(vm.modulo)
               .then(function (data) {
                   vm.resultado = data;
                   miscelanea = data.informacionGral.timer_update;
                   vm.actasCapturadas = data.informacionGral.actasCapturadas;                   
                   vm.actasEsperadas = data.informacionGral.actasEsperadas;
                   vm.actasRecibidas = data.informacionGral.actasRecibidas;
                   vm.porcentajecapturadas =data.informacionGral.porcentajecapturadas;
                   vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;

                   vm.horaCorte = data.informacionGral.horaCorte;
                   vm.fechaCorte = data.informacionGral.fechaCorte;
                   vm.ultimo_corte = data.informacionGral.ultimo_corte;
                   /*if (vm.modulo == '1D' || vm.modulo == '1S') {
                       vm.Partidos = data.Partidos;
                   }*/
                   if (vm.pIdMunicipio > 0)
                   {
                       
                       vm.Secciones = data.ltSecciones.filter(function (item) { return item.distrito == vm.pIdMunicipio; });
                   }
                   else {
                       vm.Secciones = data.ltSecciones.filter(function (item) { return item.distrito == $rootScope.$stateParams.pIdDistrito });
                   }
                   vm.Actas = data.Dist_Sec_Casilla.filter(function (item) { return item.SECCION == vm.pIdSeccion && item.PrincipioEleccion_ID == 1; });
                   var casillas = vm.resultado.Actas.filter(function (item) { return item.Seccion == vm.pIdSeccion; });
                   calculaParticipacionCiudadana(vm.Actas, casillas);
                   App.stopPageLoading();

                   if (vm.ultimo_corte) {
                       $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
                   }
                   else
                    iniciaInterval();
               })
                .catch(function (err) {
                    App.stopPageLoading();
                    iniciaInterval();
                })

            }
        }

        calculaParticipacionCiudadana = function (Actas, Casillas) {
            vm.ActasDistritos = [];
            var temp = [];

            angular.forEach(Actas, function (value) {
                var item = {};
                item.id = value.id;
                item.Nombre = value.Casilla_Descripcion;
                if (value.Casilla_Descripcion.indexOf('ESPECIAL') !== -1)
                {
                    item.especial = true;
                    item.Participacion_Ciudadana = null;
                }
                else {
                    var encontrado = Casillas.filter(function (item) { return item.Casilla_Descripcion == value.Casilla_Descripcion; });
                    item.TotalListaNominal = encontrado[0].ListaNominal==0?null:encontrado[0].ListaNominal;
                    if (encontrado[0].Contabilizada == 1) {
                        var tvln = parseInt(value.TOTAL_VOTOS_CALCULADO);
                        if (!isNaN(tvln))
                            item.Participacion_Ciudadana = $rootScope.settings.TruncateDecimal((tvln / item.TotalListaNominal * 100), 4);
                        else {
                            if (value.TOTAL_VOTOS_CALCULADO != null && value.TOTAL_VOTOS_CALCULADO.indexOf('EXCEDE') !== -1)
                                item.Participacion_Ciudadana = 0;
                            else
                                item.Participacion_Ciudadana = null;
                        }
                    }
                    else {
                        if (value.TOTAL_VOTOS_CALCULADO != null && value.TOTAL_VOTOS_CALCULADO.indexOf('EXCEDE') !== -1)
                            item.Participacion_Ciudadana = 0;
                        else
                            item.Participacion_Ciudadana = null;
                    }

                }
                temp.push(item);
            })
            vm.LtParticipacionCiudadana = $filter('orderBy')(temp, "id");
        }

        $scope.isNumber = function (n) {
           
            return !isNaN(parseFloat(n)) || isFinite(n);
        }

    

        $scope.$watch("ctrl.pIdSeccion", function (val) {
            if (val) {
                if (val == 0)
                    return;
                else
                {
                    if (angular.isUndefined(vm.resultado))
                        return;
                    BusyLoading();
                    $timeout(function () {
                        vm.Actas = vm.resultado.Dist_Sec_Casilla.filter(function (item) { return item.SECCION == val && item.PrincipioEleccion_ID == 1; });
                        var casillas = vm.resultado.Actas.filter(function (item) { return item.Seccion == vm.pIdSeccion; });
                        calculaParticipacionCiudadana(vm.Actas, casillas);
                        App.stopPageLoading();
                    });
                }

            }
        })

        vm.SeleccionCasilla = function ()
        {
           /* for (var i = 0; i < vm.Actas.length; i++) {
                if (vm.idCasilla == vm.Actas[i].id) {
                    vm.SeccionSeleccionado = vm.Actas[i];
                    break;
                }
            }
            
            //El acta 
            for (var i = 0; i < vm.LtParticipacionCiudadana.length; i++) {
                if (vm.idCasilla == vm.LtParticipacionCiudadana[i].id) {
                    vm.item_acta = vm.LtParticipacionCiudadana[i];
                    break;
                }
            }*/
            
        }
       
        vm.reloadRoute = function () {
            BusyLoading();
            getDataSource();
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
           // $state.reload();
        }

       
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }       
    });