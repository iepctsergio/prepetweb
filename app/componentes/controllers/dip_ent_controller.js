﻿angular.module('PrepetApp', [])
    .controller('dipEntController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService, HeaderService, $interval) {
        var vm = this;
        var result;
        var miscelanea = 60000;
        var tiempo;

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            $('html, body').animate({ scrollTop: 0 }, 500);
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_diputados_entidad'), $state);
            $rootScope.settings.layout.pageBodySolid = true;
            $rootScope.settings.layout.pageSidebarClosed = true;            
        });
     
        vm.radioSelected = 1;
        vm.MostarrDistritoOptenidos = true; /*para mostrar la linea en la tabla resumen-votacion-grafica la fila de istritos obtenido*/
        vm.VotosCandidatura = "Distritos";
        vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
        vm.titulo = 'Diputaciones ';
        vm.ambitoEstadistica = '- Entidad';
        vm.descripcion = "El total de votos calculado y porcentaje que se muestran, se refieren a los votos asentados en las Actas PREP hasta el momento. Por presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%.";
        vm.tituloMapa = 'DIPUTACIONES OBTENIDAS POR:';
        vm.tituloPrincipalMapa = 'Mapa Distritos Electorales ';
        vm.textoMapa = "El Mapa resalta los distritos electorales donde aventaja el partido político, coalición o candidatura independiente hasta el momento.";
        vm.NombreColumnaMapa1 = "Distritos obtenidos";
        
        vm.objVotosDistrito;
        vm.distritosPartidos = [];
        vm.logosMapa = [];

        
        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }


        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }

        vm.loadPage = function () {
            BusyLoading();

            HeaderService.
       getDiputacion()
           .then(function (data) {
               resultHeader = data;               
               vm.tabla_grafica = data.tablaVotosCandidato;
               getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })


            buscarLogosMapa = function (id) {
                for (var i = 0; i < vm.logosMapa.length; i++) {
                    if (vm.logosMapa[i].ganado == id) {
                        return (vm.logosMapa[i].logos.concat("<br/>No. de Votos obtenidos: <br/>").concat((vm.logosMapa[i].votosTotal.toString().number_format())));
                        break;
                    }
                }
                return ("");
            }

            $scope.cargarDatos = function (item) {
                //vm.idSelected = id;                
                vm.distritosPartidos = [];
                if (vm.objVotosDistrito[item].Obtenidos.length > 0) {
                    var s, f, l;
                    for (var j = 0; j < vm.objVotosDistrito[item].Obtenidos.length; j++) {
                        s = vm.objVotosDistrito[item].Obtenidos[j].Nombre;
                        f = vm.objVotosDistrito[item].Color;
                        l = vm.objVotosDistrito[item].Obtenidos[j].votos;
                        var entidad = {
                            sliceValue: s,
                            attrs: {
                                fill: f
                            },
                            label: f
                        };
                        vm.distritosPartidos.push(entidad);
                    }
                }
                cargarMapa();
            }

            $scope.cargarMapaCompleto = function () {
                vm.distritosPartidos = [];
                vm.idSelected = 0;
                for (var i = 0; i < vm.objVotosDistrito.length; i++) {
                    var s, f, l;
                    for (var j = 0; j < vm.objVotosDistrito[i].Obtenidos.length; j++) {
                        s = vm.objVotosDistrito[i].Obtenidos[j].Nombre;
                        f = vm.objVotosDistrito[i].Color;
                        l = vm.objVotosDistrito[i].Obtenidos[j].votos;
                        var entidad = {
                            sliceValue: s,
                            attrs: {
                                fill: f
                            },
                            label: f
                        };
                        vm.distritosPartidos.push(entidad);
                    }
                }
                cargarMapa();
            }
        }

       getDataSource = function () {
          DataService
            .getDiputacionEntidad()
                .then(function (data) {
                    result = data;
                                      

                    vm.tituloMapa = "Mapa Distritos Electorales:";
                    vm.subTituloMapa = "Diputaciones obtenidas por:";

                    if (vm.radioSelected == 1) {

                        aplicaInformacionGeneral(data.informacionGral.informacionGralDistrito);
                        /*vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
                        vm.casillaBasica = data.informacionGral.casillaBasica;
                        vm.casillaContigua = data.informacionGral.casillaContigua;
                        vm.casillaExtraordinaria = data.informacionGral.casillaExtraordinaria;
                        vm.casillaEspecial = data.informacionGral.casillaEspecial;*/

                        //vm.tabla_grafica = data.votosCandidatura.tablaVotosCandidato;
                        //var sum = 0;
                        var totalObtenidos = 0;
                        var p = 0;
                        for (var i = 0; i < vm.tabla_grafica.length; i++) {
                            if (data.votosCandidatura.tablaVotosCandidato.length == 0)
                                break;
                            var rs = data.votosCandidatura.tablaVotosCandidato.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                            if (rs.length > 0) {
                                vm.tabla_grafica[i].Color = rs[0].Color;
                                vm.tabla_grafica[i].total_obtenidos = rs[0].total_obtenidos;
                                vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                                vm.tabla_grafica[i].Obtenidos = rs[0].Obtenidos;
                                vm.tabla_grafica[i].max = rs[0].max;
                                vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;
                                // sum += vm.tabla_grafica[i].totalVotos;
                                p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;
                                totalObtenidos += !isNaN(vm.tabla_grafica[i].total_obtenidos) ? vm.tabla_grafica[i].total_obtenidos : 0;

                            }
                        }
                        vm.TotalVotos = vm.totalVotos = vm.TotalVotos = result.votosCandidatura.totalVotos;// vm.Suma(data.votosCandidatura.tablaVotosCandidato, 'totalVotos');
                        if (vm.TotalVotos == 0)
                            vm.TotalVotos = null;
                        vm.totalDistritos = totalObtenidos == 0 ? null : totalObtenidos;


                        vm.PorcentajeTotal = p==0?null: "100.0000%";
                        vm.objVotosDistrito = vm.tabla_grafica.filter(function (item) { return item.total_obtenidos > 0; });
                        vm.copiaObjetoEnpate = vm.tabla_grafica.filter(function (item) { return item.total_obtenidos > 0; });
                        vm.sumaDeDistritos = vm.Suma(data.votosCandidatura.tablaVotosCandidato, 'total_obtenidos');


                        BuscarMapaEnpate();

                        
                        
                        $timeout(function () {
                            var $jq = jQuery.noConflict();
                            var chart = $("#graficaEstadistica").highcharts();
                            
                            chart.series[1].data[0].update(parseFloat(vm.porcentajecapturadas));
                            chart.series[2].data[0].update(parseFloat(vm.porcentajecontabilizadas));
                        }, 300);
                        
                        
                     
                        vm.tituloMapa = "Mapa Distritos Electorales:";
                        vm.subTituloMapa = "Diputaciones obtenidas por:";

                        agregar();
                        var $jq = jQuery.noConflict();
                        cargarMapa = function () {
                            
                            $jq(function () {
                                $jq(".mapcontainer").mapael({
                                    map: {
                                        name: "distritos_tabasco",
                                        zoom: {
                                            enabled: true,
                                            maxLevel: 10,
                                            mousewheel: false,
                                            init: {
                                                latitude: 40.717079,
                                                longitude: -74.00116,
                                                level: 0
                                            },
                                            attrs: {
                                                stroke: "#fff",
                                                "stroke-width": 1
                                            },
                                            attrsHover: {
                                                "stroke-width": 2
                                            }
                                        }
                                    },
                                    legend: {
                                        area: {
                                            title: "DISTRITOS DE TABASCO",
                                            slices: Object.assign(vm.distritosPartidos)
                                        }
                                    },
                                    areas: {
                                        "I": {
                                            value: "01 DISTRITO ELECTORAL TENOSIQUE",
                                            tooltip: {
                                                content: "<span style=\"font-weight:bold;\">01 DISTRITO ELECTORAL TENOSIQUE</span><br />".concat(buscarLogosMapa("01 DISTRITO ELECTORAL TENOSIQUE")),}
                                        },
                                        "II": {
                                            value: "02 DISTRITO ELECTORAL CÁRDENAS",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">02 DISTRITO ELECTORAL CÁRDENAS</span><br />".concat(buscarLogosMapa("02 DISTRITO ELECTORAL CÁRDENAS")) }
                                        },
                                        "III": {
                                            value: "03 DISTRITO ELECTORAL CÁRDENAS",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">03 DISTRITO ELECTORAL CÁRDENAS</span><br />".concat(buscarLogosMapa("03 DISTRITO ELECTORAL CÁRDENAS")) }
                                        },
                                        "IV": {
                                            value: "04 DISTRITO ELECTORAL HUIMANGUILLO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">04 DISTRITO ELECTORAL HUIMANGUILLO</span><br />".concat(buscarLogosMapa("04 DISTRITO ELECTORAL HUIMANGUILLO")) }
                                        },
                                        "V": {
                                            value: "05 DISTRITO ELECTORAL CENTLA",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">05 DISTRITO ELECTORAL CENTLA</span><br />".concat(buscarLogosMapa("05 DISTRITO ELECTORAL CENTLA")) }
                                        },
                                        "VI": {
                                            value: "06 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">06 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("06 DISTRITO ELECTORAL CENTRO")) }
                                        },
                                        "VII": {
                                            value: "07 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">07 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("07 DISTRITO ELECTORAL CENTRO")) }
                                        },
                                        "VIII": {
                                            value: "08 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">08 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("08 DISTRITO ELECTORAL CENTRO")) }
                                        },
                                        "IX": {
                                            value: "09 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">09 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("09 DISTRITO ELECTORAL CENTRO")) }
                                        },
                                        "X": {
                                            value: "10 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">10 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("10 DISTRITO ELECTORAL CENTRO")) }
                                        },
                                        "XI": {
                                            value: "11 DISTRITO ELECTORAL TACOTALPA",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">11 DISTRITO ELECTORAL TACOTALPA</span><br />".concat(buscarLogosMapa("11 DISTRITO ELECTORAL TACOTALPA")) }
                                        },
                                        "XII": {
                                            value: "12 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">12 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("12 DISTRITO ELECTORAL CENTRO")) }
                                        },
                                        "XIII": {
                                            value: "13 DISTRITO ELECTORAL COMALCALCO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">13 DISTRITO ELECTORAL COMALCALCO</span><br />".concat(buscarLogosMapa("13 DISTRITO ELECTORAL COMALCALCO")) }
                                        },
                                        "XIV": {
                                            value: "14 DISTRITO ELECTORAL CUNDUACÁN",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">14 DISTRITO ELECTORAL CUNDUACÁN</span><br />".concat(buscarLogosMapa("14 DISTRITO ELECTORAL CUNDUACÁN")) }
                                        },
                                        "XV": {
                                            value: "15 DISTRITO ELECTORAL EMILIANO ZAPATA",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">15 DISTRITO ELECTORAL EMILIANO ZAPATA</span><br />".concat(buscarLogosMapa("15 DISTRITO ELECTORAL EMILIANO ZAPATA")) }
                                        },
                                        "XVI": {
                                            value: "16 DISTRITO ELECTORAL HUIMANGUILLO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">16 DISTRITO ELECTORAL HUIMANGUILLO</span><br />".concat(buscarLogosMapa("16 DISTRITO ELECTORAL HUIMANGUILLO")) }
                                        },
                                        "XVII": {
                                            value: "17 DISTRITO ELECTORAL JALPA DE MÉNDEZ",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">17 DISTRITO ELECTORAL JALPA DE MÉNDEZ</span><br />".concat(buscarLogosMapa("17 DISTRITO ELECTORAL JALPA DE MÉNDEZ")) }
                                        },
                                        "XVIII": {
                                            value: "18 DISTRITO ELECTORAL MACUSPANA",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">18 DISTRITO ELECTORAL MACUSPANA</span><br />".concat(buscarLogosMapa("18 DISTRITO ELECTORAL MACUSPANA")) }
                                        },
                                        "XIX": {
                                            value: "19 DISTRITO ELECTORAL NACAJUCA",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">19 DISTRITO ELECTORAL NACAJUCA</span><br />".concat(buscarLogosMapa("19 DISTRITO ELECTORAL NACAJUCA")) }
                                        },
                                        "XX": {
                                            value: "20 DISTRITO ELECTORAL PARAÍSO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">20 DISTRITO ELECTORAL PARAÍSO</span><br />".concat(buscarLogosMapa("20 DISTRITO ELECTORAL PARAÍSO")) }
                                        },
                                        "XXI": {
                                            value: "21 DISTRITO ELECTORAL CENTRO",
                                            tooltip: { content: "<span style=\"font-weight:bold;\">21 DISTRITO ELECTORAL CENTRO</span><br />".concat(buscarLogosMapa("21 DISTRITO ELECTORAL CENTRO")) }
                                        }
                                    }
                                });
                            });                            
                        };
                        cargarMapa();
                    }
                    else {
                        aplicaInformacionGeneral(data.informacionGral.informacionGralPartidos);
                        /*vm.porcentajeParticipacionCiudadana = data.informacionGral.ppcxpartidos;
                        vm.casillaBasica = data.informacionGral.pcasillaBasica;
                        vm.casillaContigua = data.informacionGral.pcasillaContigua;
                        vm.casillaExtraordinaria = data.informacionGral.pcasillaExtraordinaria;
                        vm.casillaEspecial = data.informacionGral.pcasillaEspecial;
                        */
                        getVotosPorPartidos(data.votosCandidaturaIndependiente.tablaVotosCandidato);
                    }

                    App.stopPageLoading();
                    
                    if (vm.ultimo_corte) {
                        $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
                    }
                    else
                    iniciaInterval();
                })
           .catch(function (err) {
               App.stopPageLoading();
               iniciaInterval();
           })
          //Fin del  servicio      

      }


       aplicaInformacionGeneral = function (data) {

           miscelanea = data.timer_update;
           vm.actasCapturadas = data.actasCapturadas;
           vm.actasEsperadas = data.actasEsperadas;
           vm.actasRecibidas = data.actasRecibidas;
           vm.contabilizadas = data.contabilizadas;
           vm.actasCasillaUrbana = data.actasCasillaUrbana;
           vm.actasCasillaNoUrbana = data.actasCasillaNoUrbana;
           vm.actasListaNominal = data.actasListaNominal;

           vm.TotalActasListaNominal = data.TotalActasListaNominal;
           vm.porcentajecapturadas = data.porcentajecapturadas;
      
           vm.porcentajecontabilizadas = data.porcentajecontabilizadas;

           vm.barraporcentaje = $rootScope.settings.TruncateDecimal(((vm.actasListaNominal / vm.TotalActasListaNominal) * 100), 4);
           vm.participacion = data.Participacion;
           vm.horaCorte = data.horaCorte;
           vm.fechaCorte = data.fechaCorte;
           vm.ultimo_corte = data.ultimo_corte;

           vm.porcentajeParticipacionCiudadana = data.porcentajeParticipacionCiudadana;
           vm.casillaBasica = data.casillaBasica;
           vm.casillaContigua = data.casillaContigua;
           vm.casillaExtraordinaria = data.casillaExtraordinaria;
           vm.casillaEspecial = data.casillaEspecial;
       }

      getVotosPorPartidos = function (data) {
          //var totalObtenidos = 0;
          var p = 0;
          for (var i = 0; i < vm.tabla_grafica.length; i++) {
              if (data.length == 0)
                  break;
              var rs = data.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
              if (rs.length > 0) {
                  // vm.tabla_grafica[i].Color = rs[0].Color;                    
                  vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                  vm.tabla_grafica[i].max = rs[0].max;
                  vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;
                  p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;
                  //sum += !isNaN(vm.tabla_grafica[i].totalVotos)?vm.tabla_grafica[i].totalVotos:0;
                  //totalObtenidos +=!isNaN(vm.tabla_grafica[i].total_obtenidos)? vm.tabla_grafica[i].total_obtenidos:0;

              }
          }

          vm.PorcentajeTotal = p==0?null: "100.0000%";
          vm.TotalVotos = result.votosCandidaturaIndependiente.totalVotos;
          if (vm.TotalVotos == 0)
              vm.TotalVotos = null;

          if (data.length > 0) {
              vm.calcularMaximos();
          }
      }

      
          BuscarMapaEnpate = function () {
              vm.distritosPartidos = [];
              for (var i = 0; i < vm.objVotosDistrito.length; i++) {
                  for (var t = 0; t < vm.objVotosDistrito[i].Obtenidos.length; t++) {
                      for (var j = 0; j < vm.copiaObjetoEnpate.length; j++) {
                          if (vm.copiaObjetoEnpate[j].logoPartido != vm.objVotosDistrito[i].logoPartido) {  /*Se usa el logo como id de la coalicion para evitar comparacion del mismo objeto*/
                              if (vm.copiaObjetoEnpate[j].total_obtenidos > 0)
                                  for (var k = 0; k < vm.copiaObjetoEnpate[j].Obtenidos.length; k++) {
                                      if ((vm.objVotosDistrito[i].Obtenidos[t].id == vm.copiaObjetoEnpate[j].Obtenidos[k].id) && (vm.objVotosDistrito[j].Obtenidos[k].Nombre != "")) {
                                          //console.log(i + "-" + vm.objVotosDistrito[i].Obtenidos[t].Nombre + "++" + vm.copiaObjetoEnpate[j].Obtenidos[k].Nombre);
                                          vm.objVotosDistrito[j].Obtenidos[k].Color = "";
                                          vm.objVotosDistrito[j].Obtenidos[k].Nombre = "";
                                          vm.objVotosDistrito[j].total_obtenidos = vm.objVotosDistrito[j].total_obtenidos - 1;
                                          vm.totalDistritos = vm.totalDistritos - 1;
                                      }
                                  }
                          }
                      }
                  }
              }
              agregar();
          }
      
        /************************************************/
      agregar = function () {
          vm.distritosPartidos = [];
          for (var i = 0; i < vm.objVotosDistrito.length; i++) {
              var s, f, l;
              var imagen = "";
              var logosMapaGanado;
              for (var j = 0; j < vm.objVotosDistrito[i].Obtenidos.length; j++) {

                  s = vm.objVotosDistrito[i].Obtenidos[j].Nombre;
                  f = vm.objVotosDistrito[i].Color;
                  l = vm.objVotosDistrito[i].Obtenidos[j].votos;
                  var entidad = {
                      sliceValue: s,
                      attrs: {
                          fill: f
                      },
                      label: f
                  };
                  if (typeof (vm.objVotosDistrito[i].logoPartido) != "") {
                      imagen = imagen.concat("<img src='assets/img/partidos/240/" + vm.objVotosDistrito[i].logoPartido + "'  height='30' />");
                  } else
                      imagen = imagen.concat(vm.objVotosDistrito[i].nombreCandidatonombreColumna);

                  logosMapaGanados = {
                      ganado: s, logos: imagen, votosTotal: l
                  };
                  imagen = "";

                  vm.logosMapa.push(logosMapaGanados);
                  vm.distritosPartidos.push(entidad);                  
              }
          };
      }

        $scope.$watch("ctrl.radioSelected", function (val) {
            if (val) {
                if (val == 1) {
                    if (!angular.isUndefined(result)) {
                        aplicaInformacionGeneral(result.informacionGral.informacionGralDistrito);
                       /* vm.porcentajeParticipacionCiudadana = result.informacionGral.porcentajeParticipacionCiudadana;
                        vm.casillaBasica = result.informacionGral.casillaBasica;
                        vm.casillaContigua = result.informacionGral.casillaContigua;
                        vm.casillaExtraordinaria = result.informacionGral.casillaExtraordinaria;
                        vm.casillaEspecial = result.informacionGral.casillaEspecial;*/
                        vm.tabla_grafica = [];
                        vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                        //vm.tabla_grafica = result.votosCandidatura.tablaVotosCandidato;
                        vm.TotalVotos = vm.totalVotos = result.votosCandidatura.totalVotos;
                        var p = 0;
                        for (var i = 0; i < vm.tabla_grafica.length; i++) {
                            if (result.votosCandidatura.tablaVotosCandidato.length == 0)
                                break;
                            var rs = result.votosCandidatura.tablaVotosCandidato.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                            if (rs.length > 0) {
                                vm.tabla_grafica[i].Color = rs[0].Color;
                                vm.tabla_grafica[i].total_obtenidos = rs[0].total_obtenidos;
                                vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                                vm.tabla_grafica[i].Obtenidos = rs[0].Obtenidos;
                                vm.tabla_grafica[i].max = rs[0].max;
                                vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;
                                vm.totalDistritos += vm.tabla_grafica[i].total_obtenidos;
                                p += !isNaN(vm.tabla_grafica[i].porcentaje) ? vm.tabla_grafica[i].porcentaje : 0;
                            }
                        }
                        vm.PorcentajeTotal = p==0?null:$rootScope.settings.TruncateDecimal(p, 4) + "%";
                        cargarMapa();
                    }
                } else {

                    /*vm.porcentajeParticipacionCiudadana = result.informacionGral.ppcxpartidos;
                    vm.casillaBasica = result.informacionGral.pcasillaBasica;
                    vm.casillaContigua = result.informacionGral.pcasillaContigua;
                    vm.casillaExtraordinaria = result.informacionGral.pcasillaExtraordinaria;
                    vm.casillaEspecial = result.informacionGral.pcasillaEspecial;
                    */
                    aplicaInformacionGeneral(result.informacionGral.informacionGralPartidos);
                    vm.tabla_grafica = [];
                    vm.tabla_grafica = resultHeader.Partidos;
                    getVotosPorPartidos(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                    vm.TotalVotos = vm.totalVotos = result.votosCandidaturaIndependiente.totalVotos;
                }
            }

           /* if (val) {
                if (val == 1) {
                    if (!angular.isUndefined(result)) {
                        vm.tabla_grafica = result.votosCandidatura.tablaVotosCandidato;
                        vm.objVotosDistrito = result.votosCandidatura.tablaVotosCandidato
                        vm.TotalVotos = result.votosCandidatura.totalVotos;
                        cargarMapa();
                    }
                } else {
                    vm.tabla_grafica = result.votosCandidaturaIndependiente.tablaVotosCandidato;
                    vm.objVotosDistrito = result.votosCandidaturaIndependiente.tablaVotosCandidato;
                    vm.TotalVotos = result.votosCandidaturaIndependiente.totalVotos;
                    if (vm.tabla_grafica.length > 0) {
                        vm.calcularMaximos();
                    } else {
                        vm.calcularMaximos();
                    }
                }
            }*/
        });

        vm.calcularMaximos = function () {
            vm.Chart = {};
            vm.Chart.p1 = 0;
            vm.Chart.p2;
            vm.Chart.p3;
            vm.Chart.p4;
            vm.Chart.p5;
            vm.maxValueChart = 0;

            var max = Math.max.apply(Math, vm.tabla_grafica.map(function (item) {

                return angular.isUndefined(item.totalVotos) ? 0 : item.totalVotos;
            }));
            vm.valorMaximo = max;
            if (max > 0) {
                vm.Chart.p2 = vm.porcentajeGrafica(max, 25);
                vm.Chart.p3 = vm.porcentajeGrafica(max, 50);
                vm.Chart.p4 = vm.porcentajeGrafica(max, 75);
                vm.Chart.p5 = vm.porcentajeGrafica(max, 100);
            } else {
                vm.Chart.p2 = 25;
                vm.Chart.p3 = 50;
                vm.Chart.p4 = 75;
                vm.Chart.p5 = 100;
            }

        };

        vm.Suma = function (data, key) {
            if (angular.isUndefined(data) || angular.isUndefined(key))
                return 0;
            var sum = 0;
            angular.forEach(data, function (value) {
                sum = sum + parseInt(value[key], 10);
                /*obtener votacion maxima*/
                if (vm.votacionMaxima < parseInt(value[key], 10)) {
                    vm.votacionMaxima = parseInt(value[key], 10);
                }
            });
            return sum;
        };


        vm.alturaBar = function (valormaximo, total) {
            if (angular.isUndefined(valormaximo))
                return 200;
            if (angular.isUndefined(total))
                return 200;

            valormaximo = valormaximo.toString();
            total = total.toString();
            var totalInteger = parseInt(total.replace(',', ''));

            var tot = parseInt(valormaximo.replace(',', ''));
            if (tot <= 0) {
                return 200;
            } else {

                return 200 - ((totalInteger * 200) / vm._porcentajeGrafica(valormaximo, 100));
            }

        };

        vm.porcentajeGrafica = function (total, por)
        {
            var result=vm._porcentajeGrafica(total, por);
            return vm.number_format(result,0);
        };


        vm._porcentajeGrafica = function (total, por) {

            var tot = parseInt(total.toString().replace(/,/g, ""));
            var next = parseInt(tot.toString()[0]) + 1;

            var nextInt = next * Math.pow(10, tot.toString().length - 1);

            if ((nextInt - tot) <= nextInt * 0.1) {
                nextInt = nextInt + (nextInt * 0.1);
            }

            return nextInt * (por / 100);
        };

        vm.number_format = function (amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        };

        

        
        /**********************************************************************************/
        vm.reloadRoute = function () {
            BusyLoading();
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            getDataSource();
        };
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        };

        String.prototype.number_format = function (d) {
            var n = this;
            var c = isNaN(d = Math.abs(d)) ? 0 : d; //isNaN(d = Math.abs(d)) ? 2 : d; para los numero decimales
            var s = n < 0 ? "-" : "";
            var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + ',' : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + ',') + (c ? '.' + Math.abs(n - i).toFixed(c).slice(2) : "");
        }
});