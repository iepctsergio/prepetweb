﻿
angular.module('PrepetApp', [])
    .controller('ayuntamientoMunicipioController', function ($rootScope, $scope, $http, $timeout, $filter, $state, DataService, HeaderService, $interval) {
        var vm = this;
        vm.modulo = "3HM";
        var result = {};
        $scope.selccionadoMucipio = {};
        var resultHeader = {};
        var miscelanea = 60000;
        var tiempo;

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();
            Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_ayuntamiento_municipios'), $state);
            $('html, body').animate({ scrollTop: 0 }, 500);
            //loadPage();
        });
        $rootScope.settings.layout.pageBodySolid = true;
        $rootScope.settings.layout.pageSidebarClosed = true;
    

        BusyLoading = function () {
            var option = { animate: true };
            App.startPageLoading(option);
        }

        iniciaInterval = function () {
            if (miscelanea > 0) {
                tiempo = $interval(function () {
                    getDataSource();
                }, miscelanea, true);
                $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            }
        }

        vm.loadPage = function () {
            
            vm.radioSelected = 1;
            //texto para los radio botones
            vm.VotosCandidatura = "Votos por Candidatura";
            vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";            
            //modulo
            vm.titulo = 'Presidencias Municipales y Regidurías -';
            vm.ambitoEstadistica = ' Municipios';
            vm.descripcion = "Es posible que se presente un empate entre dos o más partidos políticos, coaliciones o candidaturas independientes y se resolvería hasta el cómputo final.\nPor presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos suman 100%.";
            vm.titulo_detalle_votos = "Detalle de votos por Municipios";
            vm.titulo_actas = "Actas por Municipio";

            vm.requiereMunicipio = true;
            vm.item_acta = {};
            vm.inicio = true;
            vm.detallevotos_primera_columna = "Detalle de Votos por Municipios";      
            vm.nombreColumna = "Municipios";

            BusyLoading();

            HeaderService.
       getMunicipios()
           .then(function (data) {
               resultHeader = data;
               vm.objVotosDistrito = vm.tabla_grafica = data.tablaVotosCandidato;
               vm.DetalleVotos = data.Municipios;
               DetallePorDistrito(data.tablaVotosCandidato);
               DetallePorActas(data.Municipios);               
               getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })
          
        }//fin del load page

        DetallePorActas = function (data) {
            vm.ActasDistritos = [];
            var t = {};
            t.id = -1;
            t.Nombre = "Total";
            t.Participacion_Ciudadana = 0;
            vm.ActasDistritos.push(t);
            var t2 = {};
            t2.id = 0;
            t2.Nombre = "Porcentaje";
            t2.Participacion_Ciudadana = 0;
            vm.ActasDistritos.push(t2);
            for (var i = 0; i < data.length; i++) {
                var item = {};
                item.id = data[i].id;
                item.Nombre = data[i].Nombre;
                item.Participacion_Ciudadana = 0;
                vm.ActasDistritos.push(item);
            }
        }

        DetallePorDistrito = function (data) {
            if (angular.isUndefined(vm.DetalleVotos))
                return;
            for (var i = 0; i < vm.DetalleVotos.length; i++) {
                vm.DetalleVotos[i].Partidos = [];
                for (var j = 0; j < data.length; j++) {
                    var item = {};
                    item.Clave = data[j].Clave;
                    vm.DetalleVotos[i].Partidos.push(item);
                }
            }
        }

        getDataSource = function () {
            DataService
   .getAyuntamientoMunicipio()
   .then(function (data) {
       result = data;
       miscelanea = data.informacionGral.timer_update;
       vm.actasCapturadas = data.informacionGral.actasCapturadas;
       vm.actasEsperadas = data.informacionGral.actasEsperadas;
       vm.actasRecibidas = data.informacionGral.actasRecibidas;
       vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
       vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
       vm.participacion = data.informacionGral.participacion;
       vm.horaCorte = data.informacionGral.horaCorte;
       vm.fechaCorte = data.informacionGral.fechaCorte;
       vm.ultimo_corte = data.informacionGral.ultimo_corte;
       if (vm.radioSelected == 1) {
           LlenaTablaVotacion(data.votosCandidatura.tablaVotosCandidato);
           LlenaDetalleMunicipios(data.votosCandidatura.tablaDistrito);
           vm.votacionMaxima = -1;
           $scope.votacionAlta();
       }
       else {
           LlenaTablaVotacion(data.votosCandidaturaIndependiente.tablaVotosCandidato);
           LlenaDetalleMunicipios(data.votosCandidaturaIndependiente.tablaDistrito)
       }

       if (result.votosCandidatura.tablaActasDistrito.length > 0) {

           for (var i = 0; i < vm.ActasDistritos.length; i++) {

               var rs = result.votosCandidatura.tablaActasDistrito.filter(function (item) {
                   return item.id == vm.ActasDistritos[i].id;
               });

               if (rs.length > 0) {
                   vm.ActasDistritos[i].Esperadas = rs[0].Esperadas;
                   vm.ActasDistritos[i].Capturadas = rs[0].Capturadas;
                   vm.ActasDistritos[i].Contabilizadas = rs[0].Contabilizadas;
                   vm.ActasDistritos[i].Participacion_Ciudadana = rs[0].Participacion_Ciudadana;
                   vm.ActasDistritos[i].TotalListaNominal = rs[0].TotalListaNominal;
               }
           }

       }
       App.stopPageLoading();
       if (vm.ultimo_corte)
       {
           $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
       }
       else
         iniciaInterval();
   })
   .catch(function (err) {
       App.stopPageLoading();
       iniciaInterval();
   })
        }//Fin del data source

        LlenaTablaDetalleXDistrito = function (data) {
            for (var i = 0; i < vm.DetalleVotos.length; i++) {

                var rs = data.filter(function (item) {
                    return item.id == vm.DetalleVotos[i].id;
                });
                if (rs.length > 0) {
                    vm.DetalleVotos[i].Partidos = [];
                    vm.DetalleVotos[i].Partidos = rs[0].Partidos;
                    vm.DetalleVotos[i].TotalVotos = rs[0].TotalVotos;
                }
            }
        }

        LlenaDetalleMunicipios = function (data)
        {
            for (var i = 0; i < vm.DetalleVotos.length; i++) {

                var rs = data.filter(function (item) {
                    return item.id == vm.DetalleVotos[i].id;
                });
                
                if (rs.length > 0)
                {
                    if (rs[0].Partidos.length > 0)
                    {
                        angular.forEach(vm.DetalleVotos[i].Partidos, function (value) {
                            var rsp = rs[0].Partidos.filter(function (item) { return item.Otros === value.Clave });
                            if (rsp.length > 0)
                            {                                
                                if (rsp[0].votos != null)
                                {
                                    value.votos = rsp[0].votos;                                    
                                }
                                else
                                {
                                    value.votos = "P";                                   
                                }
                            }
                        })

                        vm.DetalleVotos[i].TotalVotos = rs[0].TotalVotos;
                    }
                   
                }
            }

        }

        LlenaTablaVotacion= function (data) {
            var sum = 0; var p = 0;
            var totalObtenidos = 0;

            for (var i = 0; i < vm.tabla_grafica.length; i++) {
                if (data.length == 0)
                    break;
                var rs = data.filter(function (item) { return item.logoPartido == vm.tabla_grafica[i].Clave });
                if (rs.length > 0) {
                    vm.tabla_grafica[i].Color = rs[0].Color;
                    vm.tabla_grafica[i].total_obtenidos = rs[0].total_obtenidos;
                    vm.tabla_grafica[i].totalVotos = rs[0].totalVotos;
                    vm.tabla_grafica[i].Obtenidos = rs[0].Obtenidos;
                    vm.tabla_grafica[i].max = rs[0].max;
                    vm.tabla_grafica[i].porcentaje = rs[0].porcentaje;

                    sum += !isNaN(vm.tabla_grafica[i].totalVotos)?vm.tabla_grafica[i].totalVotos:0;
                    totalObtenidos += angular.isUndefined(vm.tabla_grafica[i].total_obtenidos)? 0: vm.tabla_grafica[i].total_obtenidos;
                    p += !isNaN(vm.tabla_grafica[i].porcentaje)?vm.tabla_grafica[i].porcentaje:0;

                }
            }
            if (data.length > 0) {
                vm.objVotosDistrito = [];
                vm.objVotosDistrito = vm.tabla_grafica;
                vm.TotalVotos = sum;
                vm.PorcentajeTotal = $rootScope.settings.TruncateDecimal(p, 4) + "%";
                vm.totalDistritos = totalObtenidos;
            }
        }


        vm.Suma = function (data, key) {
            if (angular.isUndefined(data) || angular.isUndefined(key))
                return 0;
            var sum = 0;
            angular.forEach(data, function (value) {
                sum = sum + parseInt(value[key], 10);
                /*obtener votacion maxima*/
                if (vm.votacionMaxima < parseInt(value[key], 10)) {
                    vm.votacionMaxima = parseInt(value[key], 10);
                }
            });
            return sum;
        }

        $scope.votacionAlta = function () {            
            for (var i = 0; i < vm.objVotosDistrito.length; i++) {
                if (parseFloat(vm.votacionMaxima) < parseFloat(vm.objVotosDistrito[i].total)) {            
                    vm.votacionMaxima = vm.objVotosDistrito[i].total;                    
                }
            }                       
        }

        //Evento del radio button
        $scope.$watch("ctrl.radioSelected", function (val) {
            if (val) {
                if (result) {
                    if (val == 1) {
                        vm.tabla_grafica = [];
                        vm.objVotosDistrito = vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                        DetallePorDistrito(resultHeader.tablaVotosCandidato);

                        if (result.votosCandidatura) {
                            LlenaTablaVotacion(result.votosCandidatura.tablaVotosCandidato);
                            LlenaDetalleMunicipios(result.votosCandidatura.tablaDistrito);                           
                        }
                    } else {
                        vm.tabla_grafica = [];
                        vm.tabla_grafica = resultHeader.Partidos;
                        LlenaTablaVotacion(result.votosCandidaturaIndependiente.tablaVotosCandidato);
                        DetallePorDistrito(resultHeader.Partidos);
                        LlenaDetalleMunicipios(result.votosCandidaturaIndependiente.tablaDistrito);
                    }
                }
            }
        })

        vm.reloadRoute = function () {
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
            BusyLoading();
            getDataSource();
           // $state.reload();
        }
        vm.exportarDatos = function () {
            window.location.href = $rootScope.settings.basedatos;
        }

        $(function () {
            $('.wrapper1').on('scroll', function (e) {
                $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
            });
            $('.wrapper2').on('scroll', function (e) {
                $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
            });
        });
        $(window).on('load', function (e) {
            $('.div1').width($('table').width());
            $('.div2').width($('table').width());
        });
    });