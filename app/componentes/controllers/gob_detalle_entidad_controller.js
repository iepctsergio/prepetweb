﻿angular.module('PrepetApp', []).controller('gobDetalleEntidadController', function ($rootScope, $scope, $timeout, $filter, $state, DataService, HeaderService, $interval) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
        $('html, body').animate({ scrollTop: 0 }, 500);
        Layout.setAngularJsMainMenuActiveLink('set', $('#menu_link_gob_detalle'), $state);
        $rootScope.settings.layout.pageBodySolid = true;
        $rootScope.settings.layout.pageSidebarClosed = true;
    });
    var vm = this;
    var result = {};
    var resultHeader = {};

    var resultHeader = {};
    var miscelanea = 60000;
    var tiempo;
 
    //****variable del encabzado**************    
    vm.radioSelected = 1;
    vm.votacionMaxima = -1;
    vm.VotosCandidatura = "Votos por Candidatura";
    vm.VotosPartidoPolitico = "Votos por Partido Político y Candidatura Independiente";
    
    vm.op1 = 'Votos por Candidatura';
    vm.op2 = 'Votos por Partido Político y Candidatura Independiente';
    

    //****variable del encabzado**************
    vm.titulo = 'Gubernatura';
    vm.ambitoEstadistica = '-Detalle de Entidad';
    vm.descripcion = "El total de votos calculado y porcentaje que se muestran, se refieren a los votos asentados en las Actas PREP hasta el momento. \nPor presentación, los decimales de los porcentajes muestran sólo cuatro dígitos. No obstante, al considerar todos los decimales, suman 100%.";

    //***************************************Tablas******************
    //--------------tabla1---------
    vm.encabezadoTabla1 = 'Resumen de la votación';
    vm.titulo_actas = "Actas de la Entidad";
    vm.mostrarNombreCandidato = true;
  
    vm.objResumenVotacion=[];       
    vm.objVotosCandidatura = [];
    vm.objVotosPartido = [];

    BusyLoading = function () {
        var option = { animate: true };
        App.startPageLoading(option);
    }

    iniciaInterval = function () {
        if (miscelanea > 0) {
            tiempo = $interval(function () {
                getDataSource();
            }, miscelanea, true);
            $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
        }
    }

    /************************************************************************/
    vm.loadPage = function () {
        BusyLoading();
           App.stopPageLoading();

           HeaderService.
       getGobEntidad()
           .then(function (data) {
               resultHeader = data;             
               vm.tabla_grafica = data.tablaVotosCandidato;
               vm.objVotosCandidatura = data.DistribucionVotos;
                
               vm.objVotosPartido = [];
             
               angular.forEach(resultHeader.Partidos, function (value) {
                   var item = {};
                   item.Logo = value.logoPartido;
                   item.Clave = value.Clave;
                   vm.objVotosPartido.push(item);
               })
              
               getDataSource();
           })
           .catch(function (err) {
               App.stopPageLoading();
           })

       }//Fin de load page

       getDataSource = function () {
           DataService
      .getGubernaturaDetalleEntidad()
      .then(function (data) {
          result = data;
          miscelanea = data.informacionGral.timer_update;
          App.stopPageLoading();
          vm.actasCapturadas = data.informacionGral.actasCapturadas;
          vm.actasEsperadas = data.informacionGral.actasEsperadas;
          vm.actasRecibidas = data.informacionGral.actasRecibidas;
          vm.porcentajecapturadas = data.informacionGral.porcentajecapturadas;
          vm.porcentajeParticipacionCiudadana = data.informacionGral.porcentajeParticipacionCiudadana;
          vm.participacion = data.informacionGral.participacion;
          vm.horaCorte = data.informacionGral.horaCorte;
          vm.fechaCorte = data.informacionGral.fechaCorte;
          vm.ultimo_corte = data.informacionGral.ultimo_corte;         

          if (vm.radioSelected == 1) {
              if (data.votosCandidatura.tablaVotosCandidato.length > 0)
                  LlenaTablaGrafica(data.votosCandidatura.tablaVotosCandidato);
          }
          else if (result.votosCandidaturaIndependiente.tablaVotosCandidato.length > 0) {
              LlenaTablaGrafica(result.votosCandidaturaIndependiente.tablaVotosCandidato);
              //vm.calcularMaximos();
          }
        
          LlenaDistribucionVotos(data.votosCandidatura.tablaVotosPartido)
          vm.ActasDistritos = data.votosCandidatura.tablaActasDistrito;

          angular.forEach(vm.objVotosPartido, function (value) {
              for (var i = 0; i < data.votosCandidaturaIndependiente.tablaVotosPartido.length; i++) {
                  if (value.Clave === data.votosCandidaturaIndependiente.tablaVotosPartido[i].Logo)
                  {
                      value.Coalicion = data.votosCandidaturaIndependiente.tablaVotosPartido[i].Coalicion;
                      value.Partido = data.votosCandidaturaIndependiente.tablaVotosPartido[i].Partido;
                      value.Total = data.votosCandidaturaIndependiente.tablaVotosPartido[i].Total;
                      break;
                  }
              }
          })

          App.stopPageLoading();

          if (vm.ultimo_corte) {
              $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
          }
          else
          iniciaInterval();
         // data.votosCandidaturaIndependiente.tablaVotosPartido
         // objVotosPartido
      })
      .catch(function (err) {
          App.stopPageLoading();
      })

       }
          
       
       LlenaTablaGrafica = function (data) {
           var sum = 0; var p = 0;
           angular.forEach(vm.tabla_grafica, function (value) {
               for (var i = 0; i < data.length; i++) {
                   if (value.Clave === data[i].logoPartido) {
                       value.porcentaje = data[i].porcentaje;
                       value.totalVotos = data[i].totalVotos;
                       sum += value.totalVotos;
                       value.max = data[i].max;
                       p += value.porcentaje;
                       break
                   }
               }              
           })
           vm.TotalVotos = sum == 0 ? null : sum;           
           vm.PorcentajeTotal = p == 0 ? null : "100.0000%";
           vm.Porcentaje = vm.PorcentajeTotal;
       }

       LlenaDistribucionVotos = function (data) {
           console.log(data);
           angular.forEach(vm.objVotosCandidatura, function (value) {
               angular.forEach(data, function (item) {
                   if (value.Clave === item.candidato) {
                       value.porcentaje = item.porcentaje;
                       value.total = item.total;
                       value.partido = item.partido;
                   }
               })
           })
       }

       vm.Suma = function (data, key) {
           if (angular.isUndefined(data) || angular.isUndefined(key))
               return 0;
           var sum = 0;
           angular.forEach(data, function (value) {
               sum = sum + parseInt(value[key], 10);
               /*obtener votacion maxima*/
               if (vm.votacionMaxima < parseInt(value[key], 10)) {
                   vm.votacionMaxima = parseInt(value[key], 10);
               }
           });
           return sum;
       }
       

       $scope.$watch("ctrl.radioSelected", function (val) {
           if (val) {
               if (result) {
                   if (val == 1) {
                       if (result.votosCandidatura) {

                           vm.tabla_grafica = resultHeader.tablaVotosCandidato;
                           LlenaTablaGrafica(result.votosCandidatura.tablaVotosCandidato);                          
                       }
                   }
                   else
                   {
                       vm.tabla_grafica = resultHeader.Partidos;
                       LlenaTablaGrafica(result.votosCandidaturaIndependiente.tablaVotosCandidato);                       
                   }
               }
           }
       })
    /************************************************************************/
    
       vm.reloadRoute = function () {
           $scope.$on('$destroy', function () { $interval.cancel(tiempo); });
           BusyLoading();
           getDataSource();
           //$state.reload();
       }
       vm.exportarDatos = function () {

           window.location.href = $rootScope.settings.basedatos;
       }
       
});