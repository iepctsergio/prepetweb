﻿
/*PETICIONES CON API RES */
PrepetApp.factory('DataService', ['$http', '$q', function ($http, $q) {

    var http_request = "http://localhost:3005/api/"   
    return {
        getGubernaturaEntidad: getGubernaturaEntidad,
        getGubernaturaDetalleEntidad: getGubernaturaDetalleEntidad,
        getGubernaturaDistrito: getGubernaturaDistrito,
        getGubernaturaDesgloseSeccion: getGubernaturaDesgloseSeccion,
        getCasilla: getCasilla,

        getDiputacionEntidad: getDiputacionEntidad,
        getDiputacioneDetalleEntidad: getDiputacioneDetalleEntidad,
        getDiputacionDetalleDistrito: getDiputacionDetalleDistrito,
        getDiputacionDesgloseSeccion: getDiputacionDesgloseSeccion,

        getAyuntamientoEntidad: getAyuntamientoEntidad,
        getAyuntamientoMunicipio: getAyuntamientoMunicipio,
        getAyuntamientoDetalleMunicipio: getAyuntamientoDetalleMunicipio,
        getAyuntamientoDesgloseSeccion: getAyuntamientoDesgloseSeccion

    }

    function getGubernaturaEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get(http_request + 'GOB_ENTIDAD').
           then(function (response) {               
               var data = {}
               data = response.data.datos.entidad;
               data.informacionGral = response.data.datos.informacionGral;
               defered.resolve(data);
           });
        return promise;
    }

    function getGubernaturaDetalleEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'GOB_DET_ENTIDAD').
         then(function (response) {
             var datos = {};
             datos = response.data.datos.detalle_entidad;
             datos.informacionGral = response.data.datos.informacionGral;
             defered.resolve(datos);
         });
        return promise;
    }

    function getGubernaturaDistrito() {
        var defered = $q.defer();
        var promise = defered.promise;        
        $http.get(http_request + 'GOB_DISTRITO').
        then(function (response) {
            var datos = {};
            datos = response.data.datos.distritos;
            datos.informacionGral = response.data.datos.informacionGral;
            defered.resolve(datos);
        });       
        return promise;
    }

    function getGubernaturaDesgloseSeccion() {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get(http_request + 'GOB_SECCION').
          then(function (response) {
              defered.resolve(getSeccion(response.data.datos));
          });
        return promise;
    }

    function getCasilla(modulo) {
        var defered = $q.defer();
        var promise = defered.promise;
        switch (modulo) {
            case '1D'://gobernador distrito
                $http.get(http_request + 'GOB_SECCION').
                  then(function (response) {
                      defered.resolve(getSeccion(response.data.datos));
                  });
                return promise;
                break;
            case '1S'://Gobernador sección
                $http.get(http_request + 'GOB_SECCION').
                  then(function (response) {
                      defered.resolve(getSeccion(response.data.datos));
                  });
                return promise;
                break;
            case '2D'://Diputacion distrito
                $http.get(http_request + 'DIP_SECCION').
               then(function (response) {
                   defered.resolve(getSeccion(response.data.datos));
               });                
                return promise;
                break;
            case '2S'://Diputación seccion
                 $http.get(http_request + 'DIP_SECCION').
                  then(function (response) {
                      defered.resolve(getSeccion(response.data.datos));
                  });
                return promise;
                break;
            case '3D'://Ayuntamiento distrito
                $http.get(http_request + 'MUN_SECCION').
                  then(function (response) {
                      defered.resolve(getSeccion(response.data.datos));
                  });              
                return promise;
                break;
            case '3S'://ayuntamiento sección
                $http.get(http_request + 'MUN_SECCION').
                  then(function (response) {
                      defered.resolve(getSeccion(response.data.datos));
                  });
                return promise;
                break;
        }

    }

    function getDiputacionEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'DIP_ENTIDAD').
          then(function (response) {
              var datos = {};
              datos = response.data.datos.entidad;
              datos.informacionGral = response.data.datos.informacionGral;
              defered.resolve(datos);          
          });
        return promise;
    }

    function getDiputacioneDetalleEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'DIP_DET_ENTIDAD').
          then(function (response) {
              var datos = {};
              datos = response.data.datos.detalle_entidad;
              datos.informacionGral = response.data.datos.informacionGral;
              defered.resolve(datos);
          });        
        return promise;
    }

    function getDiputacionDetalleDistrito() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'DIP_DISTRITO').
                  then(function (response) {
                      var datos = {};
                      datos = response.data.datos.distritos;
                      datos.informacionGral = response.data.datos.informacionGral;
                      defered.resolve(datos);
                    
                  });
        return promise;
    }

    function getDiputacionDesgloseSeccion() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'DIP_SECCION').
        then(function (response) {
            defered.resolve(getSeccion(response.data.datos));          
        });
        return promise;
    }

    function getSeccion(jsonData) {
        var seccion = {};
        seccion.informacionGral = jsonData.informacionGral;
        seccion.Partidos = jsonData.seccion.Partidos;
        seccion.Dist_Sec_Casilla = jsonData.seccion.Dist_Sec_Casilla;
        seccion.ltSecciones = jsonData.seccion.ltSecciones;
        seccion.ltDistritos = jsonData.seccion.ltDistritos;
        seccion.Actas = jsonData.seccion.Actas;

        return seccion;
    }

    function getAyuntamientoEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'MUN_ENTIDAD').
         then(function (response) {
             var datos = {};
             datos = response.data.datos.entidad;
             datos.informacionGral = response.data.datos.informacionGral;
             defered.resolve(datos);
         });       
        return promise;
    }

    function getAyuntamientoMunicipio() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(http_request + 'MUN_MUNICIPIOS').
         then(function (response) {
             var datos = {};
             datos = response.data.datos.municipios;
             datos.informacionGral = response.data.datos.informacionGral;
             defered.resolve(datos);
            /* var result = JSON.parse(response.data.datos);
             var detalle = {};
             detalle.informacionGral = result.prepData.ayuntamientos.informacionGral;
             detalle.votosCandidatura = result.prepData.ayuntamientos.detalle.votosCandidatura
             detalle.votosCandidaturaIndependiente = result.prepData.ayuntamientos.detalle.votosCandidaturaIndependiente;
             defered.resolve(detalle);*/
         });
      
        return promise;
    }

    function getAyuntamientoDetalleMunicipio() {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get(http_request + 'MUN_DETALLE').
        then(function (response) {
            var datos = {};
            datos = response.data.datos.detalle_municipio;
            datos.informacionGral = response.data.datos.informacionGral;
            defered.resolve(datos);
            /*var result = JSON.parse(response.data.datos);
            var detalleDistrito = {};
            detalleDistrito.informacionGral = result.prepData.ayuntamientos.informacionGral;
            detalleDistrito.votosCandidatura = result.prepData.ayuntamientos.detalle_municipios.votosCandidatura
            detalleDistrito.votosCandidaturaIndependiente = result.prepData.ayuntamientos.detalle_municipios.votosCandidaturaIndependiente;
            defered.resolve(detalleDistrito);*/
        });
        return promise;
    }

    function getAyuntamientoDesgloseSeccion() {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get(http_request + 'MUN_SECCION').
       then(function (response) {
           defered.resolve(getSeccion(response.data.datos));
       });      
      return promise;
    }
}]);

/*CAMBIO DE SOLICITUDES*/
PrepetApp.factory('*DataService', ['$http', '$q', function ($http, $q) {
    var request_gob_1 = {
        method: 'get',
        url: 'app/json/Data/GOB_ENTIDAD.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_gob_2 = {
        method: 'get',
        url: 'app/json/Data/GOB_DET_ENTIDAD.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_gob_3 = {
        method: 'get',
        url: 'app/json/Data/GOB_DISTRITO.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_gob_4 = {
        method: 'get',
        url: 'app/json/Data/GOB_SECCION.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };



    var request_dip_1 = {
        method: 'get',
        url: 'app/json/Data/DIP_ENTIDAD.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_dip_2 = {
        method: 'get',
        url: 'app/json/Data/DIP_DET_ENTIDAD.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_dip_3 = {
        method: 'get',
        url: 'app/json/Data/DIP_DISTRITO.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_dip_4 = {
        method: 'get',
        url: 'app/json/Data/DIP_SECCION.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };



    var request_pre_mun_reg_1 = {
        method: 'get',
        url: 'app/json/Data/MUN_ENTIDAD.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_pre_mun_reg_2 = {
        method: 'get',
        url: 'app/json/Data/MUNICIPIOS.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_pre_mun_reg_3 = {
        method: 'get',
        url: 'app/json/Data/MUN_DETALLE.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

    var request_pre_mun_reg_4 = {
        method: 'get',
        url: 'app/json/Data/MUN_SECCION.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };

   

    var request_bd = {
        method: 'get',
        url: 'app/json/Data/descarga.json',
        dataType: 'json',
        headers: { 'Content-Type': undefined, 'Cache-Control': 'no-cache' }
    };
    return {
        getGubernaturaEntidad: getGubernaturaEntidad,
        getGubernaturaDetalleEntidad: getGubernaturaDetalleEntidad,
        getGubernaturaDistrito: getGubernaturaDistrito,
        getGubernaturaDesgloseSeccion: getGubernaturaDesgloseSeccion,
        getCasilla: getCasilla,

        getDiputacionEntidad: getDiputacionEntidad,
        getDiputacioneDetalleEntidad: getDiputacioneDetalleEntidad,
        getDiputacionDetalleDistrito: getDiputacionDetalleDistrito,
        getDiputacionDesgloseSeccion: getDiputacionDesgloseSeccion,

        getAyuntamientoEntidad: getAyuntamientoEntidad,
        getAyuntamientoMunicipio: getAyuntamientoMunicipio,
        getAyuntamientoDetalleMunicipio: getAyuntamientoDetalleMunicipio,
        getAyuntamientoDesgloseSeccion: getAyuntamientoDesgloseSeccion,
        getBaseDatos: getBaseDatos
    }

    function getBaseDatos() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_bd)
            .success(function (jsonData) {
                defered.resolve(jsonData);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getGubernaturaEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_gob_1)
            .success(function (jsonData)
            {
                var data = {}
                data=jsonData.entidad;
                data.informacionGral = jsonData.informacionGral;                
                defered.resolve(data);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getGubernaturaDetalleEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_gob_2)
           .success(function (jsonData)
           {
               var datos = {};
               datos = jsonData.detalle_entidad;
               datos.informacionGral = jsonData.informacionGral;               
               defered.resolve(datos);
           })
           .error(function (error) {
               defered.reject(err)
           })
        return promise;
    }

    function getGubernaturaDistrito() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_gob_3)
           .success(function (jsonData) {
               var datos = {};
               datos = jsonData.distritos;
               datos.informacionGral = jsonData.informacionGral;
               defered.resolve(datos);
           })
           .error(function (error) {
               defered.reject(err)
           })
        return promise;
    }

    function getGubernaturaDesgloseSeccion() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_gob_4)
          .success(function (jsonData) {
              defered.resolve(getSeccion(jsonData));
          })
          .error(function (error) {
              defered.reject(err)
          })
        return promise;
    }

    function getCasilla(modulo) {
        var defered = $q.defer();
        var promise = defered.promise;
        switch (modulo) {
            case '1D'://gobernador distrito
                $http(request_gob_4)
                  .success(function (jsonData) {
                      defered.resolve(getSeccion(jsonData));
                      // defered.resolve(jsonData.prepData.gobernatura.seccion);
                  })
                  .error(function (error) {
                      defered.reject(err)
                  })
                return promise;
                break;
            case '1S'://Gobernador sección
                $http(request_gob_4)
                    .success(function (jsonData) {
                        defered.resolve(getSeccion(jsonData));
                        //defered.resolve(jsonData.prepData.gobernatura.seccion);
                    })
                   .error(function (error) {
                       defered.reject(err)
                   })
                return promise;
                break;
            case '2D'://Diputacion distrito
                $http(request_dip_4)
                  .success(function (jsonData) {

                      defered.resolve(getSeccion(jsonData));
                  })
                 .error(function (error) {
                     defered.reject(err)
                 })
                return promise;
                break;
            case '2S'://Diputación seccion
                $http(request_dip_4)
                  .success(function (jsonData) {
                      defered.resolve(getSeccion(jsonData));
                  })
                 .error(function (error) {
                     defered.reject(err)
                 })
                return promise;
                break;
            case '3D'://Ayuntamiento distrito
                $http(request_pre_mun_reg_4)
                 .success(function (jsonData) {
                     defered.resolve(getSeccion(jsonData));
                     //defered.resolve(jsonData.prepData.ayuntamientos.seccion);
                 })
                .error(function (error) {
                    defered.reject(err)
                })
                return promise;
                break;
            case '3S'://ayuntamiento sección
                $http(request_pre_mun_reg_4)
                 .success(function (jsonData) {
                     defered.resolve(getSeccion(jsonData));
                     //defered.resolve(jsonData.prepData.ayuntamientos.seccion);
                 })
                .error(function (error) {
                    defered.reject(err)
                })
                return promise;
                break;
        }

    }

    function getDiputacionEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_dip_1)
            .success(function (jsonData) {
                var datos = {};
                datos = jsonData.entidad;
                datos.informacionGral = jsonData.informacionGral;
                defered.resolve(datos);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getDiputacioneDetalleEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_dip_2)
           .success(function (jsonData) {
               var datos = {};
               datos = jsonData.detalle_entidad;
               datos.informacionGral = jsonData.informacionGral;
               defered.resolve(datos);
           })
           .error(function (error) {
               defered.reject(err)
           })
        return promise;
    }

    function getDiputacionDetalleDistrito() {
        var defered = $q.defer();
        var promise = defered.promise;
        $http(request_dip_3)
            .success(function (jsonData) {
                var datos = {};
                datos = jsonData.distritos;
                datos.informacionGral = jsonData.informacionGral;
                defered.resolve(datos);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getDiputacionDesgloseSeccion() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_dip_4)
          .success(function (jsonData) {
              defered.resolve(getSeccion(jsonData));
          })
          .error(function (error) {
              defered.reject(err)
          })
        return promise;
    }


    function getSeccion(jsonData) {
        var seccion = {};
        seccion.informacionGral = jsonData.informacionGral;
        seccion.Partidos = jsonData.seccion.Partidos;
        seccion.Dist_Sec_Casilla = jsonData.seccion.Dist_Sec_Casilla;
        seccion.ltSecciones = jsonData.seccion.ltSecciones;
        seccion.ltDistritos = jsonData.seccion.ltDistritos;
        seccion.Actas = jsonData.seccion.Actas;

        return seccion;
    }

    function getAyuntamientoEntidad() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_pre_mun_reg_1)
            .success(function (jsonData) {               
                var datos = {};
                datos = jsonData.entidad;
                datos.informacionGral = jsonData.informacionGral;
                defered.resolve(datos);

            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getAyuntamientoMunicipio() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_pre_mun_reg_2)
            .success(function (jsonData) {             
                var datos = {};
                datos = jsonData.municipios;
                datos.informacionGral = jsonData.informacionGral;
                defered.resolve(datos);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getAyuntamientoDetalleMunicipio() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_pre_mun_reg_3)
            .success(function (jsonData) {                
                var datos = {};
                datos = jsonData.detalle_municipio;
                datos.informacionGral = jsonData.informacionGral;
                defered.resolve(datos);
            })
            .error(function (error) {
                defered.reject(err)
            })
        return promise;
    }

    function getAyuntamientoDesgloseSeccion() {
        var defered = $q.defer();
        var promise = defered.promise;
        //solicitud json
        $http(request_pre_mun_reg_4)
          .success(function (jsonData) {
              defered.resolve(getSeccion(jsonData));
          })
          .error(function (error) {
              defered.reject(err)
          })
        return promise;
    }
}]);
